# Onboarding legal Person for a DataProvider

__Description__

Bob is the administrator of Company B and want to identify Company B as a provider to the GXDCH

__Steps__
* Bob retrieves Participant (“Data Provider”) schema description
* Bob fulfills the provider self description according to the schema
* Bob send the Self-description to the GXDCH Trust Framework API
* GXDCH Trust Framework validates the provider Self-Description request (checks the claims, run accreditation process) then returns a VC

```mermaid
sequenceDiagram
actor Bob
participant Wallet
participant ICP
participant Registry
Participant Notarization
participant Compliance
participant Verifier
Activate Bob
    rect rgb(240,238,238)
        Bob -->>+ ICP: request a VC_LegalPerson
        ICP -->>- Bob: form with required fields for a VC_LegalPerson
        Bob -->>+ ICP: return form filled
            ICP -->>+ Notarization: request VC legal person
            Notarization -->>- ICP: return VC_LegalPerson
            ICP -->>+ Registry: add CredentialStatus 
            Registry -->>- ICP: return CredentialStatus
            ICP ->> ICP: sign VC_LegalPerson
            ICP -->>+ Registry: store VC issued
            Registry -->>- ICP: registry updated
        ICP -->>- Bob: display QRCode for VC_LegalPerson
        Bob -->>+ Wallet: scan QRcode for VC_LegalPerson
        Wallet -->>+ ICP: request VC_LegalPerson
        ICP -->>- Wallet: return VC_LegalPerson
        Wallet -->>- Bob: VC_LegalPerson added
    end

    rect rgb(240,238,238)
        Bob -->>+ ICP: request a VC_RegistrationNumber
        ICP -->>- Bob: display QRCode for VP_RegistrationNumber to request legalPerson
        
        Bob -->>+ Wallet: scan QRCode 
        
            Wallet -->>+ ICP: Request VP_RegistrationNumber

                ICP -->>+ Bob: form with required fields for a VC_RegistrationNumber
                Bob -->>- ICP: return form filled

                ICP -->>+ Notarization: request VC_RegistrationNumber
                Notarization -->>- ICP: return VC_RegistrationNumber
            
                ICP -->>+ Registry: add CredentialStatus 
                Registry -->>- ICP: return CredentialStatus
            
                ICP ->> ICP: sign VC_RegistrationNumber
            
                ICP -->>+ Registry: store VC issued
                Registry -->>- ICP: registry updated
            
            ICP -->>- Wallet: provide VC_RegistrationNumber
        Wallet -->>- Bob: display VC_RegistrationNumber added 
    end

    rect rgb(240,238,238)
        Bob -->>+ ICP: request a VC_TermsAndConditions
        ICP -->>- Bob: form with required fields for a VC_TermsAndConditions
        Bob -->>+ ICP: return form filled
        ICP -->>+ Registry: add CredentialStatus 
        Registry -->>- ICP: return CredentialStatus
        ICP ->> ICP: sign VC_RegistrationNumber
        ICP -->>+ Registry: store VC issued
        Registry -->>- ICP: registry updated
        ICP -->>- Bob: display QRCode for VC_TermsAndConditions
        Bob -->>+ Wallet: scan QRcode for VC_TermsAndConditions
        Wallet -->>+ ICP: request VC_TermsAndConditions
        ICP -->>- Wallet: return VC_TermsAndConditions
        Wallet -->>- Bob: VC_TermsAndConditions added
    end
    
    rect rgb(240,238,238)
        Bob -->>+ ICP: request legalPersonCompliance 
        ICP -->>- Bob: display QRCode VP request legalPersonCompliance
        Bob -->>+ Wallet: scan QR Code 
        Wallet -->>+ ICP: send VP legalPersonCompliance
        ICP -->>+ Compliance: send request 
        Compliance -->>+ Verifier: check documents sent by Wallet
        Verifier -->>- Compliance: checks done
        Compliance -->>- ICP: return Compliance
        ICP ->> ICP: sign legalPersonCompliance
        ICP -->>+ Registry: store VC legalPersonCompliance
        Registry -->>- ICP: VC_LegalPerson stored
        ICP -->> Wallet: return VC legalPersonCompliance
        Wallet -->>- Bob: VC legalPersonCompliance added
    end  
deactivate Bob
```

# Declaration of the DataProvider's catalogue endpoint to the federated_Catalog

```mermaid
sequenceDiagram
    actor Anna
    participant ProviderCatalogue
    participant FederatedCatalogue
    participant PubSub
     
    FederatedCatalogue->>PubSub: Subscribes for service consumption
    Anna->>ProviderCatalogue: Creation of the provider catalogue
    Anna->>+ProviderCatalogue: Catalogue API to publish its information to the pub/sub
    Note over ProviderCatalogue: Where to find the pub/sub information
    ProviderCatalogue->>-PubSub: Publishes its information
```

# add a DataProduct to the federated Data Catalog


```mermaid
sequenceDiagram
    actor Anna
    participant Wallet
    participant ICP
    participant Compliance
    participant Labelling
    participant Verifier
    participant Notarization
    participant WizardTool
    participant ProviderCatalogue

    Anna->>WizardTool: Create VC Service (form)
    activate Anna
    activate WizardTool
    WizardTool->>+Notarization: Request VP creation
    Notarization-->>-WizardTool: VP service
    WizardTool-->>Anna: Get VP service
    Anna->>Anna: Signature of VCs and VP
    Anna-->>WizardTool: VP signed (with VCs signed)
    deactivate Anna
    WizardTool->>Wallet: Store VC Service
    deactivate WizardTool
    

    Anna->>WizardTool: Compliance request
    activate Anna
    deactivate Anna
    activate WizardTool
    WizardTool->>+ICP: Compliance request for the service with the service information
    deactivate WizardTool
    ICP->>+Wallet: Retrieval of the VP
    Wallet-->>-ICP: Service VP for compliance
    ICP->>+Compliance: Request for service compliance
    Compliance->>+Verifier: Check credentialStatus
    Verifier-->>-Compliance: Return true
    Compliance-->>-ICP: Compliance Service VC
    ICP->>-Wallet: Store the VC in the wallet
    loop Waiting for compliance VC
        WizardTool->>+Wallet: Ping for Compliance Service VC (via WaltID API)
        Wallet-->>-WizardTool: Service VP (compliance)
    end

    Anna->>WizardTool: Labelling request
    activate Anna
    deactivate Anna
    activate WizardTool
    WizardTool->>+ICP: Labelling request for the service VP with service information
    deactivate WizardTool
    ICP->>+Wallet: Retrieval of the VP
    Wallet-->>-ICP: Service VP for labelling
    ICP->>+Labelling: Request for service label
    Labelling->>+Verifier: Check credentialStatus
    Verifier-->>-Labelling: Return true
    Labelling-->>-ICP: Labelling Service VC
    ICP->>-Wallet: Store the VC in the wallet
    loop Waiting for labelling VC
        WizardTool->>+Wallet: Ping for Labelling Service VC (via WaltID API)
        Wallet-->>-WizardTool: Service VP (compliance + labelling)
    end

    Anna->>+WizardTool: Request for publication of the service VC
    activate Anna
    deactivate Anna
    WizardTool->>-ProviderCatalogue: Publication of the service VC index
    activate ProviderCatalogue
    deactivate ProviderCatalogue
```

# Onboard a DataConsumer to the Federation 

__Description__

Anna is the administrator of Company A and wants to on-board Company A as a federated service consumer, then create VC for Alice, a BI Analyst of Company A

__Steps__

* Anna retrieves Participant (“Data Consumer”) schema description
* Anna fulfills the Participant (“Data Consumer”) Self-Description according to the schema
* Anna send the Self-description to the GXCDH Trust Framework API
* GXCDH  Trust Framework validates the participant (“Data Provider”) Self-Description request (checks the claims, run accreditation process) then returns a VC
* Anna creates a signed VC for Alice with specific attributes

```mermaid
sequenceDiagram
actor Anna
participant Wallet
participant ICP
participant employeeDirectory
participant Registry
participant Compliance
participant Verifier
Activate Anna
    Anna -->>+ ICP: request a VCEmployee 
    ICP -->>- Anna: redirect to the employee Directory
    Anna -->>+ employeeDirectory: login 
    employeeDirectory -->>- Anna: logued 
    Anna -->>+ ICP: request VCEmployee
    ICP -->>+ Registry: request VC Employee 
    Registry -->>- ICP: return VC Employee
    ICP -->>+ Registry: add CredentialStatus 
    Registry -->>- ICP: return CredentialStatus
    ICP ->> ICP: sign VC Employee 
    ICP -->>+ Registry: store VC Employee 
    Registry -->>- ICP: VC stored 
    ICP -->>- Anna: Display QRCode to get VC
    Anna -->>+ Wallet: Scan QRCode 
    Wallet -->>+ ICP: get VC employee
    ICP -->>- Wallet: return VC
    Wallet -->>- Anna: VC Employee return 
deactivate Anna 
```


# search in a federated catalog of a data product 

__Description__

Alice is a BI analyst from Company A and wants to search a Data Product

__Steps__

* Alice connects to the federation Portal
* The federation Portal checks Alice credentials in regards to the authentication module and grants access
* Alice search Data Product in the federated Data Catalog based on the attributes of a Data Product self description (title, description, tags, data set, types, localization, terms & conditions, license, ...)
* Alice discovers the Data Product from Company B

```mermaid
sequenceDiagram
actor Anna
participant Wallet
participant ICP
participant FederationPortal 
Activate Anna
    Anna -->>+ FederationPortal: login 
    FederationPortal -->>- Anna: loggued
deactivate Anna 
```

# dataproduct discovered by employee of the data consummer 

__Description__

Alice of company A negotiates, agrees and signs a Data Contract with Bob of company B

__Steps__

Alice and Bob agree on terms via a Data Intermediary (Assumption: in the demo, the Federation Portal acts as a Data Intermediary). Terms are defined in the Data Product Description which serves as the starting point of the negociation (instanciated into a Data Product Usage Contract)

```mermaid
sequenceDiagram
actor Anna
actor Provider 
participant Catalog 
participant ICP 
participant Registry 
participant Verifier
participant ContractNegociationConnector 
participant DigitalSignatureConnector
Activate Anna
    Anna -->>+ Catalog: request contract
    Catalog ->> Catalog: check policy 
    Catalog -->>+ ContractNegociationConnector : request contract creation 
    ContractNegociationConnector -->>- Catalog: document created and shared 
    activate Provider 
    loop Negociation
        Anna ->> ContractNegociationConnector: update  
        Provider ->> ContractNegociationConnector: update
    end
    Anna ->> ContractNegociationConnector: validate 
    Provider ->> ContractNegociationConnector: validate 
    deactivate Provider 
    ContractNegociationConnector ->> ContractNegociationConnector: generate pdf and other required documents 
    ContractNegociationConnector ->> Catalog: contract validated
    Catalog -->>+ DigitalSignatureConnector: start signature 
    DigitalSignatureConnector -->>+ Anna: notification to sign a contract 
    Anna -->>- DigitalSignatureConnector: sign contract
    DigitalSignatureConnector -->>+ Provider: notification to sign a contract 
    Provider -->>- DigitalSignatureConnector: sign contract
    DigitalSignatureConnector -->>- Catalog:Document signed, return a VC contract
    Catalog -->>- Anna: contrat signed and email sent with link to ICP
    Anna -->>+ ICP :request VC contract 
    ICP -->>+ Anna: display QRCOde for VPr Gaia-x Human or legal Participant 
    Anna -->>+ Wallet: scan QRCode for VPr 
    Wallet -->>+ ICP: send VP human or legal Participant 
    ICP -->>+ Verifier: check VC 
    Verifier -->>- ICP: documents checked 
    ICP -->>- Wallet: documents accepted
    Wallet -->>- Anna: credentials accepted 
    ICP -->>+ DigitalSignatureConnector: get VC Contract for the user
    ICP ->> ICP: sign VC Contract 
    ICP -->>+ Registry: store VC vcContract
    Registry -->>- ICP: vcContract stored
    DigitalSignatureConnector -->>- ICP: return VC Contract 
    ICP -->>- Anna: display QRCode to get VC contract 
    Anna -->>+ Wallet: scan QR code
    Wallet -->>-Anna: VC Contract Added  
deactivate Anna
```

# Alice get access to the Data Product

__Description__

Alice is a BI analyst from Company A and wants to access the Data Product

__Steps__

* Alice connects to the Federation Portal
* URI is defined in the distribution part of the Data Product
* The Federation Portal checks Alice credentials in regards to Data Contract, checks for Data Consent (Nov. 2023) and grants access to the Data Product (Sept 2023)
* The Data Transaction is notarized by the Federation Portal (seen DataProduct Conceptual Model)

Question : what is a data consent ? a VC ?
Note: Data consent is related to GDPR -> Data Usage Agreement. Is a a VC, but not yet specified.

__ Simple Scenario __

## Step 06 - Consume a service
```mermaid
%%Step 06 - Consume a service%%
sequenceDiagram
    actor alice as Alice <br/> <<actor>>
    participant pa as Participant Agent
    participant sic as [Company A] Service Instance Trust Store <br/> (Active contracts)
    participant si as Service Instance of Company B
    participant PDP as [Service Instance] <br/>PDP - Policy Rule Checker
    participant Service Policy Rule Provider as [Service Instance]<br> Service Policy Rule Provider
    participant Business Data Referential as [Service Instance]<br> Business Data referential
    participant Service
    
    opt in or out of scenario
        alice->>+pa: Authenticates
        pa-->-alice: OK
        alice->>+pa: request service instances for Alice
        pa->>+sic:request service instances for Alice
        sic->>sic: Check all Credentials validity
        sic->>+pa: return list of active service instance
        pa-->-alice: return list of active service instance
    end

    Note right of alice: Need an exchange with si <br> to get list of information to be included in the VP
    alice->>alice: create VP
    alice->>+si: Authenticates with VP to consume
    si->>+PDP: Request authorization
    PDP->>+Service Policy Rule Provider: Request rules for Services (contract)
    Service Policy Rule Provider-->>-PDP: Applicable Rules for the service
    PDP->>+Business Data Referential: Request Business Data (contract)
    Business Data Referential-->>-PDP: Applicable Business Data for the service
    PDP->>PDP: Check Credential validity
    PDP->>PDP: All rules and VP are OK
    PDP-->>-si:  Authorize
    si->>+Service: send request
    Service-->-si: return output (consume)
    si-->-alice: return output (consume)
```

For the Demo, the ContractNegociationConnector will be a collaborative tool like nextcloud but for live environements a real negociation tool should be use. 

__ blockchain scenario __

```mermaid
sequenceDiagram
    actor Alice
    participant StorageDataContract
    participant ProviderOffchainService
    participant PricingDataContract

    Alice->>+StorageDataContract: Request for service consumption
    activate Alice
    StorageDataContract->>+ProviderOffchainService: Verify availability and characteristics of service
    ProviderOffchainService-->>-StorageDataContract: Respond with requested information
    StorageDataContract->>+PricingDataContract: Request creation of payment contract
    PricingDataContract->>Alice: Ask for payment
    Alice-->>PricingDataContract: Complete payment in stablecoin
    deactivate Alice
    PricingDataContract->>-StorageDataContract: Inform about successful payment
    StorageDataContract->>-ProviderOffchainService: Notify about successful subscription
    Alice->>ProviderOffchainService: Consume the service
```

# Revokation 

__Description__

Alice is leaving the company 

__Steps__
* Anna revoke the employee Alice 
* Alice cannot accès to the data. 

## Step Revokation 

```mermaid
%%Step Revoke a User%%
sequenceDiagram
actor Anna
actor Alice
actor Provider 
participant ICP 
participant Registry 
Activate Anna
    Anna -->>+ ICP: acces to the admin portal 
    ICP -->>- ICP: display Portal 
    Anna -->>+ ICP: search the VC Employee used by Alice 
    ICP -->>- ICP: display Employee VC found for Alice 
    Anna -->>+ ICP: revoke the VC 
    ICP -->>- ICP: VC Employee revoked 
deactivate Anna
```

## Step Test if the revokation is working 

```mermaid
%%Step Alice cannont acces data%%
sequenceDiagram
actor Alice
actor Wallet 
actor ProviderIDP
actor Verifier 
    Alice -->>+ ProviderIDP: try to login
    ProviderIDP -->>- Alice: request a Verifiable Definition with a VC Employee Displaying a QRCode
    Alice -->>+ Wallet: Scan the QRCode 
    Wallet -->>+ ProviderIDP: return required documents 
    ProviderIDP -->>+ Verifier: check VC from Alice 
    Verifier -->>- ProviderIDP: invalid VC
    ProviderIDP ->>- Wallet: invalid credentials
    Wallet -->>- Alice: Invalid credentials 
```

# Chain Of Trust 



