## Aster-X membership VC

* Author(s): [Gérôme](mailto:gerome.egron@wescale.fr)
* Date: 2024-05-28
* Status: **ACCEPTED**

### Context and Problem Statement

In order to authorize issuance of Aster-X compliance for services, we need to recognize if the requester of this
compliance is a member of Aster-X.

We decided to issue Aster-X membership VC.

We now need to decide how to issue it and how to use to authorize Aster-X compliance.

### Decision Drivers

- We need to use this VC to authorize issuance of Aster-X compliance VC with machine-to-machine usage;
- We want an Aster-X membership's VC as close as possible that Gaia-X membership's VC;
- Aster-X membership VC is an enabler to check authorization of issuance but not the main subject, we need to find a
  quick win to issue it.

### Considered Options

#### 1. Membership VC is stored inside Participant Agent

We manually generate Membership VC for the Aster-X provider. The VC is signed by Aster-X federation and is stored
inside each **Participant Agent**. This VC will be used by Credential Generated to be authenticated and
get `access_token`
to interact with other components of the Aster-X ecosystem.

To be able to store it inside **Participant Agent**, the VC id need to be prefixed with Provider Participant Agent URL.

Example of membership VC :

```json
{
  "@context": [
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials",
    "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020"
  ],
  "@type": [
    "VerifiableCredential"
  ],
  "@id": "https://ovhcloud.provider.dev.gxdch.ovh/organization/4788298634074486817542acbbb953f0/data.json",
  "issuer": "did:web:aster-x.demo23.gxfs.fr",
  "credentialSubject": {
    "@context": [
      "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity",
      "https://www.w3.org/2006/vcard/ns#"
    ],
    "@id": "https://ovhcloud.provider.dev.gxdch.ovh/organization-json/4788298634074486817542acbbb953f0/data.json",
    "@type": "vcard:Organization",
    "vcard:title": "Aster-X Membership",
    "vcard:hasMember": {
      "@type": "vcard:Individual",
      "vcard:fn": "OVHCLOUD",
      "aster-conformity:hasDid": "did:web:ovhcloud.provider.dev.gxdch.ovh"
    }
  },
  "expirationDate": "2024-11-21T06:48:56.593264+00:00",
  "issuanceDate": "2024-05-25T06:48:56.593264+00:00",
  "credentialStatus": {
    "type": "StatusList2021Entry",
    "id": "https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/aster-x-revocation#1232",
    "statusPurpose": "revocation",
    "statusListIndex": "1232",
    "statusListCredential": "https://revocation-registry.aster-x.demo23.gxfs.fr/api/v1/revocations/credentials/aster-x-revocation"
  },
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:aster-x.demo23.gxfs.fr#JWK2020-RSA",
    "created": "2024-05-25T06:48:56.593264+00:00",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..RNZPgDPUmvzGXF0m-n65FZk9dCwXx9dc9E_NGlM37ZcgSywWpdBDtWWD_UQXYe-T0OmUhExVUGK9iBOGdVGxT8lQrZFriu3eQYzROYOyd_AE686qX4E9wqLLxHltcGQjRTNbsUFBCKQrTyvEna1XQGzf_xBJ79KdU5YBlKrrB45oxO0LsHwBN6hqgqif-P2nQuGqSFSfXBup6dVXtdhhect654HpU98a2MqcvCze9Mi6VNRfJpa7Q8Ubx7aiKEfKSEyW2obe0oh9nRViGwuFBTi7vL01jm3ks8AUUpTfOYm1XvSfrOzsu6GVuyQoNFd6ZhQXRmfyevgAuaUMhgw37Q"
  }
}
```

#### 2. Membership VC is not stored inside Participant Agent

We manually generate Membership VC for the Aster-X provider. The VC is signed by Aster-X federation and the VC id is
prefixed with **Aster-X federation agent**.

This solution is cleaner because more respectful of VC usage:

1. The VC is not exposed on internet and not reachable with its id;
2. The issuer of the VC (federation) generate the id of the VC;
3. The VC is issued and stored inside a Wallet by the holder (Provider).

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://w3id.org/security/suites/jws-2020/v1"
  ],
  "@type": [
    "VerifiableCredential"
  ],
  "@id": "https://abc-federation.dev.gaiax.ovh/organization/4cd02195-d98b-4690-915e-4fdc0a488014/data.json",
  "issuer": "did:web:abc-federation.dev.gaiax.ovh",
  "credentialSubject": {
    "@id": "https://abc-federation.dev.gaiax.ovh/organization-json/4cd02195-d98b-4690-915e-4fdc0a488014/data.json",
    "@type": "vcard:Organization",
    "vcard:title": "Aster-X Membership",
    "vcard:hasMember": {
      "@type": "vcard:Individual",
      "vcard:fn": "Dufour Storage",
      "aster-conformity:hasDid": "did:web:dufourstorage.provider.dev.gaiax.ovh"
    }
  },
  "expirationDate": "2024-11-24T09:22:11.420459+00:00",
  "issuanceDate": "2024-05-28T09:22:11.420459+00:00",
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "verificationMethod": "did:web:abc-federation.dev.gaiax.ovh#JWK2020-RSA",
    "created": "2024-05-28T09:22:11.420459+00:00",
    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..I7MFZmpEyFk2d4byaEYZC4m6ibgRb-0uDeV_yAqVhfKcNMKQlENHb-_jLDkYEnWTtXnF0GSoJbH_Ceg4sgUIovg6W7A0k503Id7cMNcK65WBYqJsnTtwezbox7TJVKSB39XbIAjvLkXlWgo9VtIoWbnt0_gUyRe_UIE0H0BX3T6wRgjj7_KhIvVeJOfYt4gikt1gQ8D-hkLcrSrmF5FYkdxGG9HnHsBCnC2pdWAw5J9sJgXCMdv3AscXQOiVmmTC2OONRsE8sYKUKsSoaK5iA1f0jGuwOOrjL87ujVwRI_WL-C2Oqk2vhX8b_oCe93I-u53dfOpOL1SAr-E1yE9CIw"
  }
}
```

### Usage of the Membership VC regardless the choice of issuance and storage.

Usage of the VC to be authenticated and get `access_token` :

````mermaid
sequenceDiagram
    actor user as User
    participant cred_generator as Credential Generator
    participant gateway as Aster-X Gateway
    participant user_agent as Participant Agent
    participant idp_bridge as IDP Bridge
    participant aster_x_compliance as Aster-X Compliance

    user->>cred_generator: Launch command to get Aster-X conformity

    cred_generator->>+gateway: Ask Aster-X conformity
    gateway->>gateway: Check access_token
    gateway->>-cred_generator: No authorize, redirect to authenticate

    cred_generator->>+gateway: /authenticate with VC membership
    gateway->>+user_agent: /authenticate-aster-x throught OID4VP

    user_agent->>+idp_bridge: /verify with Aster-X scope
    idp_bridge->>user_agent: VP request with associated state_id
    user_agent->>idp_bridge: /verify/state/{state} with vp_token
    idp_bridge->>-user_agent: access_token
    user_agent->>-gateway: access_token
    gateway->>-cred_generator: access_token

    cred_generator->>+gateway: Ask Aster-X conformity with access_token
    gateway->>+idp_bridge: Check access_token
    idp_bridge->>-gateway: access_token OK
    gateway->>+aster_x_compliance: Ask Aster-X conformity
    aster_x_compliance->>-gateway: Aster-X Conformity VC
    gateway->>-cred_generator: Aster-X Conformity VC
````

### Decision Outcome

Chosen option: **1. Membership VC is stored inside Participant Agent**.

According to the delay, we need to choose the quick win solution to be able to implement OID4VP protocol with credential
generator and IDP bridge.
