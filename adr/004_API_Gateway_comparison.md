# API Gateway comparison

* Author(s): [David](mailto:david.drugeon-hamon@wescale.fr)
* Date: 2024-05-28
* Status: **CREATED**
* Technical Story: [#GXFSFR-909](https://gaia-x.atlassian.net/browse/GXFSFR-909)

## Context and Problem Statement
As part of our distributed architecture, we are observing a continuous increase in the number of components necessary
to manage various essential functions for a provider or a federation. The tasks include initializing its
"self-description objects" such as LegalParticipant, creating data related to its service offerings or product catalog,
and constructing its catalog.

Each of these components uses its own security mechanism, data format, or communication protocols, making the system
increasingly complex for a new participant wishing to join our Aster-X federation.

Given this growing complexity, it becomes crucial to simplify integration and interaction within our ecosystem. This is
why we are considering introducing a well-established element in microservices architectures: **the API Gateway**.

This centralized solution will standardize security, data management, and communication protocols through a single,
coherent interface. By acting as a unified entry point, the API Gateway will greatly simplify the onboarding of new
participants and improve the overall efficiency of our system, while maintaining a high degree of security and
compliance within the context of our self-sovereign identity (SSI) project.

### What is an API Gateway?

The API Gateway acts as a unifying interface within complex system architectures, offering a centralized access point
for all external clients. This gateway masks the complexity of distributed systems behind a simplified facade, ensuring
that only authenticated and authorized users can access protected routes. Beyond security management, the API Gateway
enriches system-client interaction with a multitude of advanced features such as request and response transformation,
conversion between different communication protocols (e.g., from gRPC to REST), enhanced observability of data flows,
and caching to optimize performance.

![api_gateway_features.png](images/api_gateway_features.png)

Here are the main features that an API Gateway can offer:

- **Security**:
  - **Authentication**: Integration with various protocols such as API Keys, OAuth 2.0, or OIDC, and the ability to
  collaborate with an Identity Provider (IDP) to manage these processes.
  - **TLS and mTLS**: Management of connection security, including support for TLS to encrypt communications with
  clients and mTLS for secure communications with microservices.
  - **Authorization**: After authentication, the API Gateway examines access rights to authorize or deny requests.
- **Observability**:
  - Detailed logging of requests, their metrics, and adding a correlation ID to facilitate interaction tracing.
  The metrics can often be exported in standard formats such as Prometheus or OpenTelemetry.
- **Caching**:
  - Caching responses to frequent requests to improve performance and reduce the load on upstream services.
- **Adaptation**:
  - Ability to communicate using different protocols for input and output; for example, receiving GraphQL requests and
  calling microservices via gRPC.
  - Transformation of data retrieved from various microservices to provide a unified format to clients.
- **Rate Limiting**:
  - Restriction of the number of requests allowed per IP address or certain HTTP headers to prevent overload.
- **Circuit Breaker**:
  - Implementation of the "Circuit Breaker" architecture pattern: Requests will no longer be routed to a failing
  service, allowing the architecture to be more resilient.
- **Error Management**:
  - Standardization of error messages returned to clients, thereby facilitating exception handling and improving the
  user experience.
- **Version Management**:
  - Different API versions are offered in parallel, facilitating the transition from one version to another.

### What are the advantages and disadvantages of using an API Gateway?

An API Gateway allows for efficient routing of requests to the appropriate microservice while ensuring security,
observability, and verification of the expected exchange format. The benefits of introducing an API Gateway into our
distributed system are numerous:

- **Simplification of interactions with external clients**: From the perspective of an external client, there is only
- one entry point into the architecture, which facilitates exchanges. The external client does not need to know the
- complexity of the distributed system.
- **Reduction of latency**: The API Gateway acts as a reverse proxy and allows for caching responses, compressing
- responses, etc. This reduces processing time and also reduces the workload on the most solicited microservices.
- **Enhanced security**: Being the sole entry point, the API Gateway allows for simplified management of request
- authentication and authorization. It ensures encrypted communication with clients. Finally, the API Gateway allows
- for limiting the number of requests per client, which ensures the resilience of our distributed system.

However, the introduction of an API Gateway raises the following issues:
- **Configuration management**: The configuration for routing requests to different microservices can become
increasingly complex.
- **Failure**: Being the only entry point to our distributed system, the API Gateway becomes a Single Point of Failure
(SPOF). It is necessary to have a resilient, highly available, and secure architecture.
- **Performance**: The API Gateway, being the only entry point, must guarantee a high level of performance as the
number of requests increases.

## Decision Drivers

To select the most suitable API Gateway for our distributed system, we must consider the following criteria:
1. Security - Evaluate the security mechanisms offered by each solution, including access management, data security,
and attack prevention.
2. Extensibility - Analyze how easily we can extend and customize the functionalities according to our business needs.
3. Documentation - Examine the quality and completeness of the available documentation to facilitate integration and
development.
4. Community Support - Consider the size and activity of the community around each solution, as it can provide valuable
resources and support.
5. Integration - Assess the ease of integration with our existing infrastructure, including compatibility with our SSI
environment.

## Considered Options

The following API Gateway solutions are considered for integration into our distributed system:
- **[Tyk Gateway - Open Source](https://tyk.io/docs/deployment-and-operations/tyk-open-source-api-gateway/quick-start/)**
- **[Kong Gateway - Open Source](https://konghq.com/kong/)**
- **[Krakend](https://www.krakend.io/docs/overview/introduction/)**
- **[Apache Apisix](https://apisix.apache.org/docs/apisix/getting-started/README/)**

To compare these solutions, we will evaluate them thanks to a demo project. This project will be based on docker-compose
and will include the following components to test the main features of each API Gateway:
- **API Gateway**: The selected API Gateway solution to manage the interactions between the components
- **User-agent**: GXFS-FR participant web agent to store and display VCs
- **VC-issuer**: GXFS-FR participant VC issuer to generate and sign VCs
- **Provider catalog**: GXFS-FR participant provider catalog to manage service offerings
- **Redis**: In-memory data structure store to cache VCs

![api_gateway_prototpe.png](images/api_gateway_prototype.png)

The aim of these tests is to see how easily the solution can be configured to:
- configure routing according to either patterns in the request path or http headers
- cache the VC and DID Document exposed by the user-agent
- protect the user-agent and provider-catalogue APIs using an authentication mechanism (ideally JWT and OpenID)
- transform requests and responses (adding/deleting HTTP headers, for example)
- add data compression if requested by the client (negotiation)
- extensibility with the addition of homemade plugins
- Send audit events (when a VC is signed, for example) on certain called routes
- retrieve observability data (metrics, logs and traces) from a grafana dashboard

The solution must be easy to deploy and ideally stateless, for reasons of resilience, stability and scalability.

Demo project can be found on [gitlab](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/infrastructure/poc-api-gateway)

## Decision Outcome

After evaluating the different API Gateway solutions based on the demo project, we have the following conclusions:

**Apache Apisix** is the most suitable solution for our distributed system because:
- It offers a comprehensive set of features and is easy to configure.
- The dashboard provided by Apisix simplifies the management of routes, plugins, consumers, and
upstreams.
- The configuration file is in yaml format, which makes it easy to manage. We can have a gitops workflow to
manage the configuration.
- OIDC is natively supported, which is essential for authenticating users with oidc4vp.
- Metrics, logs, and traces can be exported to external services (for example, Prometheus, Zipkin, Jaeger), which
- is essential for monitoring the API Gateway.
- We can extend it by developing plugins in Java, Go, or Python. It also provides a WASM proxy SDK to develop plugins
in Rust or other programming languages.
- Different compression algorithms are supported (Gzip and Brotli), which is essential to compress responses to improve
performance.
- We can mock API responses, which is essential to test the API Gateway without calling the real services.
- We can also aggregate responses from multiple services, which is essential to call multiple services in sequence
(for example, call the user-agent service to sign a VC and then call the observability stack to store cloud events).
- We can also interact with pub/sub systems (Kafka, RabbitMQ, etc.) which is essential to send audit events on certain
- called routes.

## Pros and Cons of the Options
For each option, we will list the main features and limitations to help us make an informed decision.

### Tyk Gateway - Open Source

#### Main features

| Feature                     | Description                                                                                                                                                |
|-----------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| License                     | Mozilla Public License 2.0                                                                                                                                 |
| Language                    | Golang                                                                                                                                                     |
| Auth N/Z                    | OIDC by default, JWT, certificats client<br> Other authentication mecanism can be added thanks to  https://github.com/TykTechnologies/tyk-identity-broker. |
| Supported protocols         | REST<br>SOAP<br>GraphQL<br>gRPC<br>TCP                                                                                                                     |
| Open API                    | Support d'OAS 2.x et OAS 3.0.1                                                                                                                             |
| Transformation              | Ability to change input and output format, add or remove HTTP headers, interface with protocols other than the input protocol                              |
| Plugins                     | plugins developed in Python, Javascript or Go (or any language supporting gRPC).                                                                           |
| Rate limiting               | Quota management per authenticated customer                                                                                                                |
| API Versioning              | Management of different API versions, with the option of automatically deprecating a version at a certain date                                             |
| Access Control              | Granular management of access to one or more routes, versions or other features                                                                            |
| Blacklisting / Whitelisting | Management of white or black lists to facilitate security on certain routes. Ability to restrict access to certain IPs                                     |
| CORS                        | CORS headers added for APIs that do not have them                                                                                                          |
| Kubernetes native           | K8s Operator to facilitate management of the GW API                                                                                                        |
| Observability               | Metrics, Detailed logs by API, Trace                                                                                                                       |

#### Pros

- Tyk Gateway can be extended with tyk identity broker to support other authentication mechanisms. This can be useful to authenticate users with oidc4vp.
- Tyk Gateway supports OIDC by default. This can be useful to authenticate users with oidc4vp.
- Cache can be configured based on the path of the request. This can be useful to cache VC requests from the user-agent service.

#### Cons

- Tyk Gateway requires a Redis database to store configuration and rate limiting data. This is an additional component to manage.
- Configuration is complex (there are two format to configure routes)
- Dashboard is not available in the open source version.
- Tyk documentation is complex. It can be difficult to find the information we need (configuration, deployment options etc.)


### Kong Gateway - Open Source

#### Main features

| Feature                     | Description                                                                                                                                                                                    |
|-----------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| License                     | Apache 2.0                                                                                                                                                                                     |
| Language                    | Based on Nginx + runtime lua                                                                                                                                                                   |
| Auth N/Z                    | OAuth 2.0 (limited support), JWT Auth, HMAC and Basic Auth in community edition. OIDC available only in enterprise edition                                                                     |
| Protocoles supportés        | REST<br>SOAP<br>gRPC<br>TCP<br>GraphQL (only in enterprise edition)                                                                                                                            |
| Open API                    | -                                                                                                                                                                                              |
| Transformation              | In community edition: add or remove headers, add or remove JSON properties and  query strings                                                                                                  |
| Plugins                     | Lua OpenResty Library<br>Plugin library available to extend features however number is limited in community edition<br> Plugin SDK in Go, Javascript or Python                                 |
| Rate limiting               | Basic rate limiting in community edition                                                                                                                                                       |
| API Versioning              | -                                                                                                                                                                                              |
| Contrôle d'accès            | Basic in community edition                                                                                                                                                                     |
| Blacklisting / Whitelisting |                                                                                                                                                                                                |
| CORS                        | Yes                                                                                                                                                                                            |
| Kubernetes native           | Kong Ingress Controller, Helm chart and Operator                                                                                                                                               |
| Observabilité               |                                                                                                                                                                                                |

#### Pros

- Routing configuration is simple. We can route requests based on the path (for example, route all requests starting with `/api` to the user-agent service) or route to a specific path of a service (for example, route `/catalogue/items` to catalogue service)
- Caching is possible based on the path of the request. This can be useful to cache VC requests from the user-agent service.
- API Key authentication is supported in the community edition. This can be useful to authenticate the user-agent service.
- Plugins exist to export prometheus metrics or opentelemetry traces. This can be useful to monitor the API Gateway. Logs can be exported to an external service (for example, ELK stack).

#### Cons

- Routing based on headers is not possible in the community edition. This can be a limitation if we want to route requests based on the value of a header (for example, route requests with a specific `Authorization` header to the user-agent service).
- The community edition does not support OIDC, which is a common authentication mechanism. This is a strong limitation if we want to use oidc4vp to authenticate users.
- Compression of responses is not easily configurable. We need to change nginx configuration to enable compression, which can be complex.
- It is not possible to chain requests. This can be a limitation if we want to call multiple services in sequence (for example, call the user-agent service to sign a VC and then call the observability stack to store cloud event).
- If a feature does not exist in community edition, we need to develop our proper plugin. This can be complex and time-consuming.

### Krakend

#### Main features

| Feature                     | Description                                                                            |
|-----------------------------|----------------------------------------------------------------------------------------|
| License                     | Apache 2.0                                                                             |
| Language                    | Golang                                                                                 |
| Auth N/Z                    | OAuth <br>JWT<br>Integration with Keycloak<br>Integration with Auth0<br><br>TLS 1.3    |
| Supported protocols         | GraphQL<br>HTTP<br>AMQP<br>Kafka<br>NATS<br>GCP PubSub<br>AWS SNS<br>Azure Service Bus |
| Open API                    | ****Feature only available in Enterprise edition****                                   |
| Transformation              | Response transformation (XML => JSON and JSON => XML)                                  |
| Plugins                     | Plugins developed in  Go, scripts Lua, Martian or Google CEL                           |
| Rate limiting               | -                                                                                      |
| API Versioning              | -                                                                                      |
| Access Control              | -                                                                                      |
| Blacklisting / Whitelisting | -                                                                                      |
| CORS                        | CORS headers added for APIs that do not have them                                      |
| Kubernetes native           | Krakend provides helm chart to easily deploy into K8s cluster                          |
| Observability               | Compatible with OpenTelemetry + Datadog, Grafana, Prometheus, Zipkin and Jaeger        |

#### Pros

- Easy to deploy and configure. Routes can be defined in a configuration file, which makes it easy to manage. We can have a gitops workflow to manage the configuration.
- Caching is possible based on the path of the request. This can be useful to cache VC requests from the user-agent service.
- Metrics, Logs and Traces can be exported to external services (for example, Prometheus, Grafana, Zipkin, Jaeger). This can be useful to monitor the API Gateway.

#### Cons

- In the community edition, we cannot configure to route all requests to a specific path of a service. This can be a limitation if we want to route all requests starting with `/` to the user-agent service to get VCs or DIDs.
- In the community edition, open api is not supported. This can be a limitation if we want to generate import or expose open api documentation.
- In the community edition, gzip compression is not supported. This can be a limitation if we want to compress responses to improve performance.
- In the community edition, api key authentication is not available. We need to rely on user-agent, vc-issuer or catalogue to handle api key authentication, which can be complex.
- Rate limiting is not supported in the community edition. This can be a limitation if we want to limit the number of requests per IP address or certain HTTP headers to prevent overload.

### Apache Apisix

#### Main features

| Feature                     | Description                                                                       |
|-----------------------------|-----------------------------------------------------------------------------------|
| License                     | Apache 2.0                                                                        |
| Language                    | Based on Nginx + runtime lua                                                      |
| Auth N/Z                    | Key-auth<br/>JWT<br/>Basic Auth<br/>Wolf RBAC<br/>Keycloak<br/>OAuth 2.0<br/>OIDC |
| Supported protocols         | TCP/UDP<br/>MQTT<br/>gRPC<br/>HTTP/JSON<br/>WebSocket                             |
| Open API                    |                                                                                   |
| Transformation              | Plugins to easily transforms headers, body and request                            |
| Plugins                     | Java/Go/Python SDK, WASM proxy SDK, plugin library                                |
| Rate limiting               | Yes per customer or groups                                                        |
| API Versioning              | Yes                                                                               |
| Access Control              | Yes                                                                               |
| Blacklisting / Whitelisting | Yes                                                                               |
| CORS                        | CORS headers added for APIs that do not have them                                 |
| Kubernetes native           | Helm chart                                                                        |
| Observability               | Prometheus, Zipkin, External loggers (Kafka, Datadog...)                          |

#### Pros

- Apisix provides a dashboard to easily manage routes, plugins, consumers, upstreams, etc. This can be useful to manage the API Gateway. An Admin API is also available to manage the API Gateway programmatically.
- Configuration is simple. We can route requests based on the path (for example, route all requests starting with `/api` to the user-agent service) or route to a specific path of a service (for example, route `/catalogue/items` to catalogue service).
- Configuration file is in yaml format, which makes it easy to manage. We can have a gitops workflow to manage the configuration.
- OIDC is supported. This can be useful to authenticate users with oidc4vp.
- Metrics, Logs and Traces can be exported to external services (for example, Prometheus, Zipkin, Jaeger). This can be useful to monitor the API Gateway.
- We can develop plugins in Java, Go or Python. This can be useful to extend the features of the API Gateway.
- Different compression algorithms are supported (Gzip and Brotli). This can be useful to compress responses to improve performance.
- We can mock api responses. This can be useful to test the API Gateway without calling the real services.

#### Cons

- Documentation is complex however the community is active and can help. There are many blog articles and tutorials to help understanding the API Gateway.
- Different deployment architectures are possible (control plane and data plane on same machine, separated or only data plane). This can be complex to understand and manage.
- We need to install etcd to manage configuration. This is another spof that we need to manage.

## Links
- [Introduction to API Gateway micro-service architecture](https://imesh.ai/blog/introduction-to-api-gateway-in-microservices-architecture/)
- [5 reasons why we chose Krakend](https://medium.com/@PavankumarHarikar/5-reasons-that-made-us-pick-krakend-as-our-api-gateway-db2d5869aff4)
- [the need for API Gateways and how to solve it with krakend](https://medium.com/@kocabaybugra/the-need-for-api-gateways-and-how-to-solve-it-with-krakend-2183813d51d4)
- [Kong - custom authentication](https://dev.to/jeffersonxavier/custom-authentication-with-kong-mf3)
- [Apisix Workshop](https://boburmirzo.github.io/apisix-workshop/)
- [Apisix security best practices - part 1](https://blog.frankel.ch/secure-api-practices-apisix/1/)
- [Apisix security best practices - part 2](https://blog.frankel.ch/secure-api-practices-apisix/2/)
