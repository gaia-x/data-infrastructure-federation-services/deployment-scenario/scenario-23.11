

# Identifiers and Credentials management

* Author(s): [Guillaume](mailto:guillaume.hebert@eviden.com), [David](mailto:david.drugeon-hamon@wescale.fr), [Henry](mailto:henry.faure-geors@atos.net), [Olivier T.](mailto:olivier.tirat@byo-networks.com)
* Date: 2024-03-19 <!-- optional -->
* Status: **ACCEPTED** <!-- optional -->

## Context and Problem Statement

The proposal of this document is to define what identifiers are within the Gaia-X ecosystem and to provide common rules for their creation and usage in credentials (*Verifiable Credentials* and *Verifiable Presentations*).

- [Part I. Decentralized Identifier](#part-i-decentralized-identifier)
	- [I.1. Introduction](#i1-introduction)
	- [I.2. DID as a simple string](#i2-did-as-a-simple-string)
	- [I.3. DID architecture](#i3-did-architecture)
	- [I.4. Resolver vs. Dereferencer](#i4-resolver-vs-dereferencer)
	- [I.5. DID Methods](#i5-did-methods)
- [Part II. Gaia-X Participant Identifier](#part-ii-gaia-x-participant-identifier)
	- [II.1. Abstract](#ii1-abstract)
	- [II.2. Proposal](#ii2-proposal)
	- [II.3. Example](#ii3-example)
- [Part III. Gaia-X Credential](#part-iii-gaia-x-credential)
	- [III.1. Abstract](#iii1-abstract)
	- [III.2 Proposal](#iii2-proposal)
	- [III.3. Example](#iii3-example)


## Part I. Decentralized Identifier
### I.1. Introduction
The W3C has approved the [DIDCore V1.0 specification](https://www.w3.org/TR/did-core/) as an official W3C Recommendation on the 19th of July. Consequently, DIDs are now an open web standard ready for use and further development. This kind of identifier was chosen by Gaia-X to identify objects or entities in their ecosystem, thanks to their decentralized nature, cryptographic security, and interoperability features.

The  [W3C DID-Core](https://www.w3.org/TR/did-core/)  is a significant advancement in the realm of digital identity. Here is a short summary of the key concepts to best understand usage :
1.  **Globally Unique**: DIDs are globally unique identifiers designed to represent subjects such as individuals, organizations, or entities in a decentralized manner.
2.  **Decentralized**: DIDs are created and managed independently of any central authority, enabling self-sovereign identity and reducing reliance on centralized systems.
3.  **Cryptographically Secure**: DIDs are associated with cryptographic keys/materials, ensuring secure authentication, verification, and integrity of information.
4.  **Interoperable**: DIDs are designed to be compatible with various systems and platforms, facilitating seamless interaction and data exchange across different applications and domains.
5.  **Verifiable**: DIDs enable verifiable claims and proofs, allowing subjects to assert their identity or ownership of information in a trustworthy and tamper-evident manner.
6.  **Privacy-Preserving**: DIDs support selective disclosure of information, empowering subjects to control the release of personal data and maintain privacy in digital interactions.

### I.2. DID as a simple string
In concrete and technicals terms, a DID is a simple text string consisting of three parts:
- the `did` URI scheme identifier,
- the identifier for the [DID method](https://www.w3.org/TR/did-core/#dfn-did-methods),
- the DID method-specific identifier.
-
![A simple example of a decentralized identifier](https://www.w3.org/TR/did-core/diagrams/parts-of-a-did.svg)

### I.3. DID architecture
![Overview of DID architecture](https://www.w3.org/TR/did-core/diagrams/did_brief_architecture_overview.svg)

DID is resolvable to a [DID document](https://www.w3.org/TR/did-core/#dfn-did-documents) with which must be stored on a [Verifiable Data registry](https://www.w3.org/TR/did-core/#dfn-verifiable-data-registry). Regardless of the specific technology used, any system that supports data recording and returning data necessary to produce DID Document can be used as VDR.

DID documents contain elements associated with a DID and formated using JSON format.

![Standard elements of a DID doc](https://upload.wikimedia.org/wikipedia/commons/f/f9/Decentralized-identifiers-dids-the-fundamental-building-block-of-selfsovereign-identity-ssi-30-1024_The-standard-elements-of-a-DID-doc.jpg)

A [DID URL](https://www.w3.org/TR/did-core/#dfn-did-urls) extends the syntax of a basic [DID](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers) to incorporate other standard [URI](https://www.w3.org/TR/did-core/#dfn-uri) components such as path, query, and fragment in order to locate a particular resource (i.e: a cryptographic public key inside a DID document `did:example:123456798abcdefghi#key-1`).

### I.4. Resolver vs. Dereferencer

**:warning: RESOLVING is not equivalent to DEREFERENCING :warning:**

Using DID involves a [DID resolver](https://www.w3.org/TR/did-core/#dfn-did-resolvers): a system component that takes a DID as input and produces a DID Document as output.

Using DID URL involves a [DID URL dereferencer](https://www.w3.org/TR/did-core/#dfn-did-url-dereferencers): a system component that takes a DID URL as input and produces a [resource](https://www.w3.org/TR/did-core/#dfn-resources) as output.

![DID Resolving and DID URL Dereferencing](https://www.w3.org/TR/did-core/diagrams/did_url_dereference_overview.svg)

Considering a DID Document made of 6 differents *verification methods*, that is to say, 6 differents keys managed and controlled by the DID Controller :
- `did:example:123#key-1`, the public key of an Octet string Key Pair (OKP) which uses *Ed25519* curve ;
- `did:example:123#key-2`, the public key of an Elliptic Curve Key Pair (EC) which uses *secp256k1* curve ;
- `did:example:123#key-3`, the public key of a RSA Key Pair with a key size of 2048 bits ;
- `did:example:123#key-4`, the public key of an Elliptic Curve Key Pair (EC) which uses *P-256* curve ;
- `did:example:123#key-5`, the public key of an Elliptic Curve Key Pair (EC) which uses *P-384* curve ;
- `did:example:123#key-6`, the public key of an Elliptic Curve Key Pair (EC) which uses *P-521* curve.

Using a DID resolver on `did:example:123` will resolve to this DID Document.
<details open>
<summary> DID Document Example </summary>

```json
{
  "@context": [
    "https://www.w3.org/ns/did/v1",
    "https://w3id.org/security/suites/jws-2020/v1"
  ],
  "id": "did:example:123",
  "verificationMethod": [
    {
      "id": "did:example:123#key-1",
      "type": "JsonWebKey2020",
      "controller": "did:example:123",
      "publicKeyJwk": {
        "kty": "OKP",
        "crv": "Ed25519",
        "x": "VCpo2LMLhn6iWku8MKvSLg2ZAoC-nlOyPVQaO3FxVeQ"
      }
    },
    {
      "id": "did:example:123#key-2",
      "type": "JsonWebKey2020",
      "controller": "did:example:123",
      "publicKeyJwk": {
        "kty": "EC",
        "crv": "secp256k1",
        "x": "Z4Y3NNOxv0J6tCgqOBFnHnaZhJF6LdulT7z8A-2D5_8",
        "y": "i5a2NtJoUKXkLm6q8nOEu9WOkso1Ag6FTUT6k_LMnGk"
      }
    },
    {
      "id": "did:example:123#key-3",
      "type": "JsonWebKey2020",
      "controller": "did:example:123",
      "publicKeyJwk": {
        "kty": "RSA",
        "e": "AQAB",
        "n": "omwsC1AqEk6whvxyOltCFWheSQvv1MExu5RLCMT4jVk9khJKv8JeMXWe3bWHatjPskdf2dlaGkW5QjtOnUKL742mvr4tCldKS3ULIaT1hJInMHHxj2gcubO6eEegACQ4QSu9LO0H-LM_L3DsRABB7Qja8HecpyuspW1Tu_DbqxcSnwendamwL52V17eKhlO4uXwv2HFlxufFHM0KmCJujIKyAxjD_m3q__IiHUVHD1tDIEvLPhG9Azsn3j95d-saIgZzPLhQFiKluGvsjrSkYU5pXVWIsV-B2jtLeeLC14XcYxWDUJ0qVopxkBvdlERcNtgF4dvW4X00EHj4vCljFw"
      }
    },
    {
      "id": "did:example:123#key-4",
      "type": "JsonWebKey2020",
      "controller": "did:example:123",
      "publicKeyJwk": {
        "kty": "EC",
        "crv": "P-256",
        "x": "38M1FDts7Oea7urmseiugGW7tWc3mLpJh6rKe7xINZ8",
        "y": "nDQW6XZ7b_u2Sy9slofYLlG03sOEoug3I0aAPQ0exs4"
      }
    },
    {
      "id": "did:example:123#key-5",
      "type": "JsonWebKey2020",
      "controller": "did:example:123",
      "publicKeyJwk": {
        "kty": "EC",
        "crv": "P-384",
        "x": "GnLl6mDti7a2VUIZP5w6pcRX8q5nvEIgB3Q_5RI2p9F_QVsaAlDN7IG68Jn0dS_F",
        "y": "jq4QoAHKiIzezDp88s_cxSPXtuXYFliuCGndgU4Qp8l91xzD1spCmFIzQgVjqvcP"
      }
    },
    {
      "id": "did:example:123#key-6",
      "type": "JsonWebKey2020",
      "controller": "did:example:123",
      "publicKeyJwk": {
        "kty": "EC",
        "crv": "P-521",
        "x": "AVlZG23LyXYwlbjbGPMxZbHmJpDSu-IvpuKigEN2pzgWtSo--Rwd-n78nrWnZzeDc187Ln3qHlw5LRGrX4qgLQ-y",
        "y": "ANIbFeRdPHf1WYMCUjcPz-ZhecZFybOqLIJjVOlLETH7uPlyG0gEoMWnIZXhQVypPy_HtUiUzdnSEPAylYhHBTX2"
      }
    }
  ],
  "authentication": [
    "did:example:123#key-1",
    "did:example:123#key-2",
    "did:example:123#key-3",
    "did:example:123#key-4",
    "did:example:123#key-5",
    "did:example:123#key-6"
  ],
  "assertionMethod": [
    "did:example:123#key-1",
    "did:example:123#key-2",
    "did:example:123#key-3",
    "did:example:123#key-4",
    "did:example:123#key-5",
    "did:example:123#key-6"
  ]
}
```

</details>

Using a DID derefencer on `did:example:123#key-1` will resolve to this resource, that is to say the key-1 controled by DID Controler whose ID is `did:example:123`.
<details open>
<summary> Document Resource Example </summary>

```json
{
  "id": "did:example:123#key-1",
  "type": "JsonWebKey2020",
  "controller": "did:example:123",
  "publicKeyJwk": {
    "kty": "OKP",
    "crv": "Ed25519",
    "x": "VCpo2LMLhn6iWku8MKvSLg2ZAoC-nlOyPVQaO3FxVeQ"
  }
}
```

</details>

In the next part we will focus on the management of this DID in our Gaia-X ecosystem.

### I.5. DID Methods
[DID methods](https://www.w3.org/TR/did-core/#dfn-did-methods) are the mechanisms by which a particular type of DID and its associated [DID document](https://www.w3.org/TR/did-core/#dfn-did-documents) are created, resolved, updated, and deactivated.

## Part II. Gaia-X Participant Identifier
## II.1. Abstract
Gaia-X Participants must have unique and secure identifiers to access GAIA-X services and resources. This helps ensure the security and trustworthiness of interactions within the ecosystem.

That's why it is mandatory for any participant willing to join Gaia-X to create an identifier that conforms to the **did:web** standard method.

## II.2. Proposal
A valid DID document is therefore composed of :
- A *context*
- An *id*
- An array of *verificationMethod*
- An array of *authentication* which can reference some key previously declared in "*verificationMethod*
- An array of *assertionMethod* which can reference some key previously declared in "*verificationMethod*
- An array of "service"

We highly recommand to read ICAM part related to DID creation ([credential_format](https://gaia-x.gitlab.io/technical-committee/federation-services/icam/credential_format/)).

## II.3. Example
An example of a *did:web* for a participant with the domain name abc-participant would be as follows :

<details open>
<summary> DID Document Example using did:web </summary>

```json
{
  "@context": [
    "https://www.w3.org/ns/did/v1",
    "https://w3id.org/security/suites/jws-2020/v1"
  ],
  "id": "did:web:abc-participant.com",
  "verificationMethod": [
    {
      "@context": "https://w3id.org/security/suites/jws-2020/v1",
      "id": "did:web:abc-participant.com#JsonWebKey2020-RSA",
      "controller": "did:web:abc-participant.com",
      "type": "JsonWebKey2020",
      "publicKeyJwk": {
        "kty": "RSA",
        "n": "tsb8jEW3ID6w_qcRcvRdakPe73j9XiA4I985jHc74ekzk37ABAGHQ7opmCDpfrv4FMPY-aa6tqZvML3JTlDujCqUog5ueCYD09ROYw9gJ1DyDe4_hsAsqpYHSyLZAAR_Mv1xSBPYqVLkosAxOROBbMuQOAq4KVcGSRh2Zfl4K6dbaJRRQOudBH4PCByfF87UDJnfq7MOxhaynsjcWpTTl0OQjQ8eli8E7aptdwnVKB7Sv_H2cigquXt4Pt7PTAIhqqPgXCVjCl03jzVdMMkO_GzbeoaJWPZMaQRfkOp3IBVNg_FzUCI4bSBep6lxLI-DMIm0Py0DrS3X3HbjwOFZaw",
        "e": "AQAB",
        "kid": "Xa2vc0GSLJeDGi2IFetvFYDzDtC1wcvQHwqThNSfkak",
        "alg": "PS256",
        "x5u": "https://abc-federation.gaia-x.community/.well-known/chain.pem"
      }
    },
    {
      "@context": "https://w3id.org/security/suites/jws-2020/v1",
      "id": "did:web:abc-participant.com#JsonWebKey2020-Ed25519",
      "controller": "did:web:abc-participant.com",
      "type": "JsonWebKey2020",
      "publicKeyJwk": {
        "crv": "Ed25519",
        "x": "ecBxqTm7YKx9lja3U-EzxMGLNz4xzBTm_iki3LUA-zc",
        "kty": "OKP",
        "x5u": "did:web:abc-participant.com/.well-known/chain-ed.pem"
      }
    }
  ],
  "authentication": [
    "did:web:abc-participant.com#JsonWebKey2020-Ed25519"
  ],
  "assertionMethod": [
    "did:web:abc-participant.com:issuer#JsonWebKey2020-RSA"
  ],
  "service": [
    {
      "id": "did:web:abc-participant.com#service",
      "type": "ServiceEndpoint",
      "serviceEndpoint": "https://www.abc-participant.com/did-endpoint"
    }
  ]
}
```

</details>

We notice the identifier is `did:web:abc-participant.com`, and we can resolve it [URI](https://www.rfc-editor.org/rfc/rfc3986) following the [DID-Syntax](https://www.w3.org/TR/did-core/#did-syntax) via a universal resolver such as https://dev.uniresolver.io/. We also can verify its syntax validity using https://didlint.ownyourdata.eu/validate.

The resolution allows obtaining the DID Document of the Participant *abc-participant* and acquiring their identity(ies). In this example, it is evident that there are two different keys for two different purposes:

- `did:web:abc-participant.com#JsonWebKey2020-Ed25519`, an asymmetric cryptography algorithm based on the Edwards elliptic curve *Curve25519*, used in this case for **authentication** (`authentication`).

- `did:web:abc-participant.com#JsonWebKey2020-RSA`, a symmetric cryptography algorithm used in this case for making **assertions** (`assertionMethod`).

This will be important for the creation of *Verifiable Credential* which includes a proof made with a purpose (*proofPurpose*) and a verification method (*verificationMethod*). For instance, to issue this VC below, which is an **assertion**, only the `JsonWebKey2020-RSA` verification method can be used since it is used for making assertions, while `JsonWebKey2020-Ed25519` can not be used since it is only usable for **authentication**.

<details open>
<summary> DID Document Example </summary>

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://w3id.org/security/suites/jws-2020/v1"
  ],
  "type": [
    "VerifiableCredential"
  ],
  "issuer": "did:web:abc-participant.com",
  "issuanceDate": "2023-09-28T00:00:00Z",
  "credentialSubject": {
	  ".." : "..."
  },
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "created": "2023-01-01T00:00:00Z",
    "verificationMethod": "did:web:abc-participant.com#JsonWebKey2020-RSA",
    "jws": "..."
  }
}
```

</details>

The identifier `did:web:abc-participant.com` is considered unique since no one else than *abc-participant* can have this identifier. In fact, resolving the *did:web* on the domain *abc-participant.com* implies that only abc-participant can control and assert their identifier. But for other identifiers like objects, it's more complicated since they don't have a domain. We detail how we can resolve this in next pararaph.

## Part III. Gaia-X Credential
### III.1. Abstract
Gaia-X encourages participants to provide detailed information about their product and/or service offerings within the ecosystem, as well as information about themselves. It is what we call *Gaia-X Credential*.

These *Gaia-X Credential* are crucial for enabling discovery and interoperability among participants. They include business information about **what** they describe (terms and conditions of use, name and technical identifier, standards and compliance, interfaces, security policies, etc.) and **who** describes them.  This allows stakeholders to understand how to interact with each other and to be aware of each other's offerings: it constitutes what we call a *Federated Catalog*.

These *Gaia-X Credential* are [W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model-2.0/#credentials)  containing claims expressed in  [RDF](https://www.w3.org/TR/rdf11-primer/) using the Gaia-X schema in their  [context](https://www.w3.org/TR/json-ld11/#the-context).

RDF allows us to make statements about resources. The format of these statements is simple. A statement always has the following structure:
`<subject> <predicate> <object>`

>Example :
`<did:web:abc-participant.com>`
`<is a>`
`<legalParticipant>`
or
`<did:web:abc-participant.com>`
`<offers>`
`<serviceOffering #2>`

The aim of the Federated Catalogue in term of a base graph is to :
 - find `object` given a `subject`ID
 - find `subject`given a `object` ID
 - store valid assessments (`predicate`) about `object` given by a `subject`.

Consequently it is mandatory to well identify `subject` and `object`. We propose to formalize these identifiers usage and format.

### III.2 Proposal
In our system, participants are well identified, as seen earlier, and they are encouraged to provide information about themselves and services which have to be identified since they are objects whose aim is to be resolvable and findable in a base graph federated catalogue.

The identifiers must conform to the constraints of :
- Graph-based hierarchy
- [W3C Verifiable Credential Data Model](https://www.w3.org/TR/vc-data-model/)
- [W3C DID-core](https://www.w3.org/TR/did-core/)

That is to say, when issuing a Gaia-X Credential (*Verifiable Credential*), it must be :
- uniquely identified by its controler / issuer ;
- exposed and persisted on a web storage ;
- dereference able by its controler / issuer ;
- resolvable by everyone ;
- be a [URI](https://www.rfc-editor.org/rfc/rfc3986).

ID of a Document =
 `<domain_name> / <type> / <uuid> /`
with :
- `<domaine_name>` : domain name of the participant which issues and persists the document (which can be a verifiable credential or a simple JSON data) ;
- `<type>` : the type of the document ;
- `<uuid>` : a uuid created by the controller of the document ;

**:warning: We highly recommand using [UUID version 4](https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_%28random%29) :warning:**

For a *Participant* named `abc-participant` who issues a `service-offering` (*Verifiable Credential*) which describes a `service-offering` (*JSON Data*) using JSON format :
- JSON Data ID :
	- `https://abc-participant.com/service-offering-json/076791f6-e908-4e78-b605-ac10b550f989/`
- Verifiable Credential ID :
	- `https://abc-participant.com/service-offering/55407da8-6fdc-4935-8197-286843648373/`


### III.3. Example
#### Service-Offering JSON Data
The identifier `https://abc-participant.com/service-offering-json/076791f6-e908-4e78-b605-ac10b550f989/` dereferences this document (*JSON Data*) :

<details open>
<summary> Service-Offering JSON Data Example </summary>

```json
{
  "id": "https://abc-participant.com/service-offering-json/076791f6-e908-4e78-b605-ac10b550f989/",
  "type": "gx:ServiceOffering",
  "gx:providedBy": {
    "id": "https://abc-participant.com/participant/ddf4cd25-fd36-47f9-90c0-ec1fc1412dfe/"
  },
  "gx:policy": "",
  "gx:termsAndConditions": {
    "gx:URL": "https://termsandconds.com",
    "gx:hash": "d8402a23de560f5ab34b22d1a142feb9e13b3143"
  },
  "gx:dataAccountExport": {
    "gx:requestType": "API",
    "gx:accessType": "digital",
    "gx:formatType": "application/json"
  }
}
```

</details>

#### Service-Offering Verifiable Credential
The identifier
`https://abc-participant.com/service-offering/55407da8-6fdc-4935-8197-286843648373/` dereferences this document (*Verifiable Credential*) :

<details open>
<summary> Service-Offering Verifiable Credential Example </summary>

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.gaia-x.eu/v1/context.jsonld",
    "https://w3id.org/security/suites/jws-2020/v1"
  ],
  "id": "https://abc-participant.com/service-offering/55407da8-6fdc-4935-8197-286843648373/",
  "type": [
    "VerifiableCredential",
    "ServiceOfferingCredential"
  ],
  "issuer": "did:web:abc-participant.com",
  "issuanceDate": "2023-09-28T00:00:00Z",
  "credentialSubject": {
      "id": "https://abc-participant.com/service-offering-json/076791f6-e908-4e78-b605-ac10b550f989/",
      "type": "gx:ServiceOffering",
      "gx:providedBy": {
        "id": "https://abc-participant.com/participant/ddf4cd25-fd36-47f9-90c0-ec1fc1412dfe/"
      },
      "gx:policy": "",
      "gx:termsAndConditions": {
        "gx:URL": "https://termsandconds.com",
        "gx:hash": "d8402a23de560f5ab34b22d1a142feb9e13b3143"
      },
      "gx:dataAccountExport": {
        "gx:requestType": "API",
        "gx:accessType": "digital",
        "gx:formatType": "application/json"
      }
  },
  "proof": {
    "type": "JsonWebSignature2020",
    "proofPurpose": "assertionMethod",
    "created": "2023-09-28T00:00:00Z",
    "verificationMethod": "did:web:abc-participant.com#JsonWebKey2020-RSA",
    "jws": "<string:base64-encoded-signature>"
  }
}
```

</details>
