## Architecture Decisions Records

### Rules

Only the status of the ADR can be changed.
The status can be:
* __Proposed__ (under review)
* __Accepted__ (approved and ready for implementation)
* __Rejected__ (not approved)
* __Deprecated__ (A change in the architecture or a new feature implemetation makes the decision unapplicable or out of purpose)
* __Superseded__ (superseded by another decision)

Please use the following naming convention:

`###_Name.md`

* 3-Digit index (to be incremented when the document is created)
* Name is a summary of the problem
* Use underscore for spaces
* Markdown format

### Template
Use the template in the repository

### Sources

More about ADR:
* [EN] https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions
* [FR] https://blog.wescale.fr/bonnes-pratiques-pour-%C3%A9crire-un-adr
* Template from https://github.com/nyudlts/adr-templates

