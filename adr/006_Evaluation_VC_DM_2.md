# Aster-X membership VC

* Author(s): [Henry](mailto:henry.faure-geors@eviden.com)
* Date: 2024-06-17
* Status: **created**

## Context and Problem Statement

In Gaia-X ecosystem (i.e. Aster-X), Credentials and Presentations are created / managed using the [VC Data Model V1](https://www.w3.org/TR/vc-data-model/). As Gaia-X is moving toward the implementation of [VC Data Model V2](https://www.w3.org/TR/vc-data-model-2.0/#securing-mechanisms), it is important to evaluate the elements that are changing as well as its impact on the Aster-X Ecosystem

As such, this Architecture Decision Record will act as a report to evaluate the cost of changing to VC Data Model V2


## Decision Drivers

- Sooner or latter VC Data Model V1 will be deprecated
- VC Data Model V2 is the standard used within the [EUDI Wallet](https://github.com/eu-digital-identity-wallet)
- Talks about VC Data Model V2 are ongoing and this report might help during the discussions in Working Group

## Changes in VC Data Model 2

### 1. Holder

In VC Data Model V2, the notion of self-asserted Verifiable Credential is introcuded, which is how most Gaia-X Credential are issued. 
One condition when presenting self-asserted VC in a Verifiable Presentation where the same security mecanism is used for both Verifiable Credential and Verifiable Presentation is to include a **Holder** property. 
*
The condition to validate a **Holder** property is that it must be equal to the **Issuer** of the self-asserted credentials and any **Verifier** must reject the Presentation if that is not the case.

Example of a Verifiable Presentation using **Holder** property 

```json
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://www.w3.org/ns/credentials/examples/v2"
  ],
  "type": ["VerifiablePresentation"],
  "holder": "did:example:12345678",
  "verifiableCredential": [{
    "@context": "https://www.w3.org/ns/credentials/v2",
    "type": ["VerifiableCredential", "FoodPreferenceCredential"],
    "issuer": "did:example:12345678",
    "credentialSubject": {
      "favoriteCheese": "Gouda"
    },
    ...
  }],
  "proof": [...]
}
```

**Aster-X Impact**

- Verifier components (ICP, IDP-Bridge): As a Verifier, the component must check if the Verifiable Presentation received in the OID4VP Protocol includes self-asserted credential and if so, *``holder``* property must be included.
- Aster-X Compliance: The Aster-X compliance must check if the Verifiable Presentation received in the REST Call includes self-asserted credential and if so, *``holder``* property must be included.

### 2. Issuer

The *``issuer``* property can be used to validate the previously defined *``holder``* property. In the recommendation, the *``issuer``* field may be represented by a *string* or an *object*, allowing the **Issuer** to display its name allong its did/URI. It is therefore recommended to be able to use that property within all the use cases.

Example of *issuer* property 

```json
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://www.w3.org/ns/credentials/examples/v2"
  ],
  "id": "http://university.example/credentials/3732",
  "type": ["VerifiableCredential", "ExampleDegreeCredential"],
  "issuer": {
    "id": "did:example:76e12ec712ebc6f1c221ebfeb1f",
    "name": "Example University"
  },
  "validFrom": "2010-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
    "degree": {
      "type": "ExampleBachelorDegree",
      "name": "Bachelor of Science and Arts"
    }
  }
}
```

**Aster-X Impact**

- Verifier components (ICP, IDP-Bridge, Aster-X Compliance): As a Verifier, the component must be able to retrieve the issuer id (if the issuer is an object and not just a string indicating its id) if needed in its verification process
- Issuer components (Aster-X Compliance, ICP, VC-Issuer): As an issuer, it should be possible to include in the configuration of the component whether it should display only the id of the issuer, of the complete object

**Limitations**

As seen with changes in properties such as the credentialSubject (object or array), it is possible that wallet may not understand or implement the other possible format for the issuer field. Further testing must be done using multiple identity wallets.

### 3. Valid From/ Valid Until

Some properties are deprecated, and are replaced by new one. 

|deprecated|replaced by|
|--|--|
|*``issuanceDate``*|*``validFrom``*|
|*``expirationDate``*|*``validUntil``*|


Example VC

```json
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://www.w3.org/ns/credentials/examples/v2"
  ],
  "id": "http://university.example/credentials/3732",
  "type": ["VerifiableCredential", "ExampleDegreeCredential"],
  "issuer": {
    "id": "did:example:76e12ec712ebc6f1c221ebfeb1f",
    "name": "Example University"
  },
  "validFrom": "2010-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
    "degree": {
      "type": "ExampleBachelorDegree",
      "name": "Bachelor of Science and Arts"
    }
  }
}
```

**Aster-X Impact**

- Verifier components (ICP, IDP-Bridge): As a Verifier, the component must be able to check the existence of those property and check they are not incorrect (i.e. *``validFrom``* property should be set to a time after the current time).
- Aster-X Verifier: One point that was not treated by the Aster-X Verifier is that in certain case, an expired VC Can still be used and thus the component must not return an error if that is the case but rather a warning. However it should add a rule to check that the ValidFrom date is not after the current time.
The Verifier should also support *``validFrom``* and IssuanceDate to guarantee retrocompatibility
- Issuer components (Aster-X Compliance, ICP, VC-Issuer): As an issuer, the component should replace deprecated fields with the new ones.

**Limitations**

Some general purpose and well-known *Wallet* may not be compatible with v2.


### 4. Changes in proof

In VC Data Model V2, the proof element has endured few changes. 
The most impactful on how Gaia-X digitaly signs its Verifiable Credential concerns the property *``jws``*, which has now been replaced by the field proofValue.

However, this *``proofValue``* is also linked to a new field: *``cryptosuite``*, which is supposed to indicate to a Verifier Component which *cryptosuite* has been used to sign the credential, allowing it to retrieve the *serialization*, *hash* and *signature* algorithm used.
One main key point in this changement: it links the VC Data Model V2 to the [Data Integrity Document](https://www.w3.org/TR/vc-data-integrity/). Following this document, the *JSON Web Signature 2020* cryptosuite is deprecated and thus those property only matters if an elliptic curve cryptography is used to sign document. To use RSA, refer to §5. Enveloped Credential.

Example VC

```json
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://www.w3.org/ns/credentials/examples/v2"
  ],
  "id": "http://example.gov/credentials/3732",
  "type": ["VerifiableCredential", "ExampleDegreeCredential"],
  "issuer": "did:example:6fb1f712ebe12c27cc26eebfe11",
  "validFrom": "2010-01-01T19:23:24Z",
  "credentialSubject": {
    "id": "https://subject.example/subject/3921",
    "degree": {
      "type": "ExampleBachelorDegree",
      "name": "Bachelor of Science and Arts"
    }
  },
  "proof": {
    "type": "DataIntegrityProof",
    "cryptosuite": "eddsa-rdfc-2022",
    "created": "2021-11-13T18:19:39Z",
    "verificationMethod": "https://university.example/issuers/14#key-1",
    "proofPurpose": "assertionMethod",
    "proofValue": "z58DAdFfa9SkqZMVPxAQp...jQCrfFPP2oumHKtz"
  }
}
```

**Aster-X Impact**

- Verifier components (ICP, IDP-Bridge, Verifier): As a Verifier, the component should change its signature verification mecanism to be compatible with elliptic curve cryptography and implements a factory depending on the cryptosuite detected in the cryptosuite field. This is a breaking change and would require a refactoring of the component.
- Issuer components (ICP, VC-Issuer): As the JsonWebSignature is deprecated, the component may be deprecated as well and may require breaking changes in order to be compatible with new signature mecanism for jsonLD

**Limitations**

Some general purpose and well-known Wallet may not be compatible with v2 but most of them implement elliptic curve cryptography so it is probably covered.


### 5. Enveloped Credential (Most breaking change)

The alternative to *JSON-LD* VC using elliptic curve cryptography is the use of JWT to secure Verifiable Credential, which is specified in the [Securing Verifiable Credentials using JOSE and COSE Document](https://www.w3.org/TR/vc-jose-cose/#sotd).

This mecanism is the one that is currently discussed in ICAM Working Group as it enables the signature and exchange of Verifiable Credential and Presentation using RSA.

The main difference with *JSON-LD* VC is that the credential is transformed into a JWT (using enveloping proof) and then stored within an ``*id*`` object within a JSON object.

Example VC

```json
{
  "@context": "https://www.w3.org/ns/credentials/v2",
  "id": "data:application/vc+ld+json+jwt;eyJraWQiOiJFeEhrQk1XOWZtYmt2VjI2Nm1ScHVQMnNVWV9OX0VXSU4xbGFwVXpPOHJvIiwiYWxnIjoiRVMzODQifQ.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvZXhhbXBsZXMvdjIiXSwiaWQiOiJodHRwOi8vdW5pdmVyc2l0eS5leGFtcGxlL2NyZWRlbnRpYWxzLzE4NzIiLCJ0eXBlIjpbIlZlcmlmaWFibGVDcmVkZW50aWFsIiwiRXhhbXBsZUFsdW1uaUNyZWRlbnRpYWwiXSwiaXNzdWVyIjoiaHR0cHM6Ly91bml2ZXJzaXR5LmV4YW1wbGUvaXNzdWVycy81NjUwNDkiLCJ2YWxpZEZyb20iOiIyMDEwLTAxLTAxVDE5OjIzOjI0WiIsImNyZWRlbnRpYWxTY2hlbWEiOnsiaWQiOiJodHRwczovL2V4YW1wbGUub3JnL2V4YW1wbGVzL2RlZ3JlZS5qc29uIiwidHlwZSI6Ikpzb25TY2hlbWEifSwiY3JlZGVudGlhbFN1YmplY3QiOnsiaWQiOiJkaWQ6ZXhhbXBsZToxMjMiLCJkZWdyZWUiOnsidHlwZSI6IkJhY2hlbG9yRGVncmVlIiwibmFtZSI6IkJhY2hlbG9yIG9mIFNjaWVuY2UgYW5kIEFydHMifX19.d2k4O3FytQJf83kLh-HsXuPvh6yeOlhJELVo5TF71gu7elslQyOf2ZItAXrtbXF4Kz9WivNdztOayz4VUQ0Mwa8yCDZkP9B2pH-9S_tcAFxeoeJ6Z4XnFuL_DOfkR1fP",
  "type": "EnvelopedVerifiableCredential"
}
```

This format can cause breaking changes in Verifier as well as Holder components as it adds another layer of parsing to retrieve or store the credential. Current exchanges protocol may need more time to be executed as it will add the necessity to parse the credential before rejecting or accepting it. This also add another layer of complexity due to the mediaType of the credential received. As it is a JSON Object, the media type will be application/vc+ld+json (like a normal VC), but in the end it must be treated as the media type located in the id field (vc + ld + json + jwt)

**Aster-X Impact**

- Verifier components (ICP, IDP-Bridge, Verifier): As a Verifier, the component should be able to understand how to parse and verify JWT, as well as include a pipe to verify, for each jsonld vc received, whether its an envelopped credential or a JSON-LD VC.
- Issuer components (ICP, VC-Issuer): As the JsonWebSignature is deprecated, the component may be deprecated as well and may require breaking changes in order to be compatible with this new format.

**Limitations**

Some general purpose and well-known Wallet may not be compatible with this new format.


### 6. Lifecycle

As in VC Data Model V1, a notion of lifecycle of the credential is introduced. The recommendation chosen for VC Data Model V2 is the [Bitstring Status List](https://www.w3.org/TR/vc-bitstring-status-list/).

This status list add the ability to create a personalized status list. However, this should ne documented within the Trust Layer of the ecosystem where the credential is created/exchanged. One example could be only consider the status 0x00 as valid.

This concept also modify the statusList Field to adopt the same properties as the credentialSubject (can be both object and array) as an issuer could maintain multiple status for an object 

Example VC

```json
{
  "@context": [
    "https://www.w3.org/ns/credentials/v2",
    "https://www.w3.org/ns/credentials/examples/v2"
  ],
  "id": "http://license.example/credentials/9837",
  "type": ["VerifiableCredential", "ExampleDrivingLicenseCredential"],
  "issuer": "https://license.example/issuers/48",
  "validFrom": "2020-03-14T12:10:42Z",
  "credentialSubject": {
    "id": "did:example:f1c276e12ec21ebfeb1f712ebc6",
    "license": {
      "type": "ExampleDrivingLicense",
      "name": "License to Drive a Car"
    }
  },
  "credentialStatus": [{
    "id": "https://license.example/credentials/status/84#14278",
    "type": "BitstringStatusListEntry",
    "statusPurpose": "revocation",
    "statusListIndex": "14278",
    "statusListCredential": "https://license.example/credentials/status/84"
  }, {
    "id": "https://license.example/credentials/status/84#82938",
    "type": "BitstringStatusListEntry",
    "statusPurpose": "suspension",
    "statusListIndex": "82938",
    "statusListCredential": "https://license.example/credentials/status/84"
  }]
}
```

**Aster-X Impact**

- Verifier components (ICP, IDP-Bridge, Verifier): Verifier which have already integrated bitstring status list are also used to only one list, thus it is needed to add the ability to check all the list in the credentials
- Issuer components (ICP, VC-Issuer): Issuing components may needs to add the ability to put a credential on multiple status lists if needed

**Limitations**

As seen with the issue with credentialSubject, it is unlikely that wallet supports status list in form of a list 





