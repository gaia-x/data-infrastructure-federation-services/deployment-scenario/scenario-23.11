## Gaia-x Observability based on CloudEvents

- Author(s):
	- [Guillaume Hébert](mailto:guillaume.hebert@eviden.com),
	- [David Drugeon-Hamon](mailto:david.drugeon-hamon@wescale.fr),
	- [Gérôme Égron](mailto:gerome.egron@wescale.fr)
	- [Alexandre Nicaise](mailto:alexandre.nicaise@softeam.fr)
	- [Benoit Tabutiaux](mailto:benoit.tabutiaux@imt.fr)
	-  [Ngoc-Anh Le](mailto:ngoc-anh.le@atos.net)

- Date: 2024-04-22
- Status: **created**

Technical Story: We would like to store events related to federation in a third-party participant to track any type of exchanges (contract negotiation, contractualization and data exchanges).

### Context and Problem Statement

In the current architecture of scenario 23.11, an observability brick https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/observability is available. This micro-service helps to store events relating to the onboarding, contracting and consumption of a data product. It also provides method to search for a set of events. Currently, data format is proprietary and not based on any standard. We would like to rely on an established standard to help interoperability across technologies.

### Decision Drivers

- Data format must be extensible so that new event types can easily be added,
- Data format must be interoperable,
- Data format must be agnostic of the means of transmission (HTTP, Event Buses or Message Queues).

### Considered Options

- option 1: [CloudEvents](cloudevents.io) standard
- option 2: Use Current event format provided by Observability Micro-service

### Decision Outcome

Chosen option: **OPTION 1** because this standard becomes a common format for Event-Driven Architecture, it is agnostic from messaging transport layer.

### Pros and Cons of the Options

#### option 1

CloudEvents is a CNCF graduated project that defines a common format to describe events. This standard begins to be well adopted by different actors like Google, Adobe, RedHat etc...

- Good, because CloudEvents is interoperable between cloud providers. It is also format used to synchronize federated catalogues thanks to Gaia-X CES.
- Good, because CloudEvents is agnostic on transport layer. Messages or Events can be transmitted by HTTP, Kafka, AMQP, WebSocket, MQTT etc...
- Good, because it provides two format (binary and structured). It can be easily adopted in our architecture.
- Good, because only metadata are normalized. Payload can be anything we want. We can transmit json, xml even binary
- Good, because we can begin to have an event driven architecture in the dataspace
#### option 2

Current api proposed by observability stack is proprietary https://observability-api.aster-x.demo23.gxfs.fr/swagger-ui/index.html#/

- Good, because we handle data format
- Bad, because it is proprietary to GXFS demo
- Bad, because it is less agile.

### Links

- [CloudEvents](https://cloudevents.io)
- [Observability stack](https://observability-api.aster-x.demo23.gxfs.fr/swagger-ui/index.html#/)
