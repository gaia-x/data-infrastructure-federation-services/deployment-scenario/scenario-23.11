
## Federation Compliant VC for a Partcipant

* Author(s): [Guillaume](mailto:guillaume.hebert@eviden.com), [Gérôme](mailto:gerome.egron@wescale.fr), [David](mailto:david.drugeon-hamon@wescale.fr), [Benoit](mailto:benoit.tabutiaux@imt.fr), [Henry](mailto:henry.faure-geors@atos.net)
* Date: 2023-09-04 <!-- optional -->
* Status: **ACCEPTED** <!-- optional -->

### Context and Problem Statement

A ***Federation*** in the Gaia-X ecosystem refers to the network of interconnected data spaces and/or services that adhere to Gaia-X principles which are described by ***Participants***.

***Participants*** are organizations, such as businesses, government agencies, or other entities, that join the Gaia-X ecosystem. They create and manage data spaces and/or services and make them available into a ***Federation*** after joining it throw ***Catalogue***.

Data sharing and service sharing can be possible only after verification of the federation participation of the participants. 
Examples : 
- Consummer C can consumme service of Provider P in Federation F, only if Consumer C and Provider P belong to the Federation F.
- Provider P can describe and push service S (***Service Offering***) to its ***Provider Catalogue*** which is synchronise"d to ***Federated Catalogue*** of Federation F only if Provider belongs to the Federation F.
- ...

Consequently, we need to determine whether a ***Participant*** belongs to a ***Federation***, just as we need to determine whether a participant belongs to Gaia-X ecosystem (call GXDCH for Gaia-X Digital Clearing House).

### Decision Drivers

* During participant onboarding into GXDCH, ***Participant*** has to present three VCs : *Gaia-x Terms and Condition VC*, *Legal Registration Number VC*, *Legal Participant VC*. In return ***Participant*** get back a *GXDCH Compliance Participant VC* issued by GXDCH meaning that ***Participant*** is compliant and is officialy take part to Gaia-X Ecosystem.
* The only way to know if a ***Participant*** is a member of Gaia-X is if that ***Participant*** presents its *GXDCH Participant Compliance VC* which has been issued by GXDCH.
* During participant onboarding into a Federation, we respect same workflow as GXDCH. ***Parrticipant*** has to present two VCs : *Federation Terms and Conditions VC* and *GXDCH Participant Compliance VC*)

### Considered Options

How to know if a participant belongs to a federation ?
1. Maintening a participants memberlist in the/a registry of the federation
2. Present a *Federation Participant Compliance VC*

### Decision Outcome

Chosen option: **OPTION 1**, 

Presenting a *Federation Participant Compliance VC* is more SSI than maintening a memberlist and more deccentralized since it's the aim of the VC.

*abc-checker sidecar* component can implement a simple rule "is participant" which can return true or false if presentation of *Federation Participant Compliance VC* is valid.