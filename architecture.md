# Aster-X architecture document

## Global View

![Global View](assets/Aster-X_architecture_global_view.jpg)

## Aster-X architecture - Catalogue view

<img src="assets/Aster-X_architecture_Catalogue_view.jpg" alt="Catalogue view" width="600"/>

## Aster-X architecture - contract negotiation view

<img src="assets/Aster-X_architecture_contract_negotiation_view.jpg" alt="contract negotiation view" width="600"/>

## Aster-X architecture - authentication and authorization view

<img src="assets/Aster-X_architecture_authentication_and_authorization_view.jpg" alt="authentication and authorization view" width="600"/>
