# VC Generation workflow for Aster-X demonstration

## Global view

We see generation sequence of the VC used by Aster-X demonstrator from the on-boarding of a participant inside Gaia-X to
the Aster-X label generation.

There is a sequence because data inside VC are used by other VC according to use case. For instance, to compute a label
we need information about ServiceOffering and its Location.

Each VC card bring information about the VC like the issuer, the holder but also technical information about the
component used so sign the VC.

![Workflow_VC_Generation](assets/workflow_vc_generation_all.jpg)

## Step 1 - Participant on-boarding

First step is to on-board a participant inside Gaia-X, so we need to get the Compliance VC about participant through
GXDCH compliance API.

To get the compliance we need to generate VC about the participant, the legal-registration-number and the
TermsAndCondition.

To generate a LegalParticipant we can use the two tools build by GXFS-FR team :

- Wizard tool (UI)
- Credential Generator (CLI)

![Workflow_VC_Generation](assets/workflow_vc_generation_part1.jpg)

## Step 2 - Catalogue generation with Gaia-X compliance

To build a Gaia-X catalogue and share data with CES, we need to get Gaia-X compliance about our Service Offering.
So we generate Service Offering VC thantks to Credential Generator or ICP and we use it to get Gaia-X compliance through
GXDCH API.

![Workflow_VC_Generation](assets/workflow_vc_generation_part2.jpg)

## Step 3 - Catalogue generation with Aster-X objects

Aster-X has its own ontology which is an extension of Gaia-X ontology. We can create VC about this object but cannot get
Gaia-X compliance. These VCs will be useful to compute an Aster-X label after.

![Workflow_VC_Generation](assets/workflow_vc_generation_part3.jpg)

## Step 4 - Aster-X label generation

We need to generate different VC and credential to get the configuration of Label Service according to PRLD. With this
parameters and LocatedServiceOffering VC we have the necessary to compute label thanks to Aster-X Labelling Service.

![Workflow_VC_Generation](assets/workflow_vc_generation_part4.jpg)
