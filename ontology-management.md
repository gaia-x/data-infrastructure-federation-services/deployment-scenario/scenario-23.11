# Ontology management

## Our need

- Extend Gaia-X ontology to be able to get compliance and to store data from our context
- Have an available context to reference it in our credentials
- Easy to add evolution and to deploy our ontology

## Components and stack

<img src="assets/ontology_components.jpg" alt="Ontology components" width="600"/>

To create and manage ontology, we use 2 components :

- Schema
  Provider [Gitlab](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/schema-provider-from-linkml)
- Schema
  Registry [Gitlab](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/schema-registry)

These components are deployed here for the usage of Aster-X federation :

- [Schema Provider](https://schema-provider-linkml.abc-federation.dev.gaiax.ovh/)
- [Schema Registry](https://schema-registry.aster-x.demo23.gxfs.fr/api/doc)

### Schema Provider

This component is used to build our ontologies. From LinkML we are going to generate multiple format like json-ld
context, SHACL and documentation. The developer need to generate file before to push because the CI/CD don't generate
file but build a website to exposed what is generated.

Ontologie generated like json-ld context and SHACL doesn't know where they will be exposed to they don't know target
URL. The generated file are going to use URI Tag. This will be the goal of registry to change this tag with real URL.

### Schema Registry

This component expose necessary files like context.

By config map injection, the registry knows which source context he will exposed. These source context are exposed by
Schema Provider and the Schema Registry replace tag URL with his URL before to cache the result and expose it through an
REST API. 