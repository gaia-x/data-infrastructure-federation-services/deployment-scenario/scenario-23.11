# 🧰 GXFS FR Toolbox

[TOC]

## Description

This page present the GXFS FR Toolbox. It includes all **the component** developped and/or used in the context of Scenario 23.11 and Scenario 24.

👉 The components are presented according to the services provided **independently of each other** in order to highlight their main benefits and fields of action.

💡 **Note**: This page does not show how the Aster-X Ecosystem works, the architecture diagram is presented on this page.

##  📋✍ Tools for generating, signing, issuing and administering credentials

### Issuance Credential Protocol (ICP)

**Short functional description (10 lines)**
 A tool to issue credentials & administrate them
 - Deliver VC / Issue VC using standard protocols (OIDC4VCI draft 8/13, Bittring Status List)
 - Present VC / Verify VC using standard protocols (OIDC4VP draft 20, presentation exchange 2.0)
 - A User friendly Portal to issue credential with everal type of issuance: form, presentation, keycloak
 - An administration portal to administrate credential (search, suspend, revoke, reactivate)
- Ability to easily add new credentials using a scalable plugin system 
 
**Expected benefits of the component**
The tool target a full compatibilty with Gaia-X Clearing houses, wallet from the Market (Talao/Alt.me, Lissi, BDR-Wallet, Meeco, Panasonic & future European Wallet) and credential specification and format (SD-JWT, jwt_vc_json-ld, ldp_vc)
The tool was designed to be integrated inside an existing IT system (to deliver employee credential according to your existing identity provider)
The tool is designed to allow the adjunction of new credential (plugin system)
The tool is delivered with APIs and user interface (Issuance portal & Admin Portal)
 
**Identified limitations of the component**
- Not yet fully compatible with Walt.id
- Dependency with the "revocation registry" to perform revocation
 
 **Possible improvements identified for the component**
 - Make sure it remains up-to-date with the latest W3C & Gaia-X specs
 
 **Source and contact**
 
 - *Origin (company / authors)*: Docaposte for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer 
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/tree/main?ref_type=heads#introduction 
 - *Main Contact*:  @Jeremy.hirth-daumas


### VC Issuer

**Short functional description (10 lines)**
 VC-Issuer is a web application designed for performing signatures of Gaia-X credentials following the W3C Verifiable Credentials Data Model 2.0
- it supports severals JSON Web Signature and Encryption Algorithms
- it generate VP Token according to OID4VP protocol.
- it supports severals lifecycle management mechanisms with status list : (Status list 2021 and Bitstring Status List)

 **Expected benefits of the component**
- A simple tool to perform all the VC issuance tasks through an API
- Design with a micro-service approach.
- The tool is under constant evolution since two years. Its simplicity allow to adapt shortly and to closely fit with the lastest specifications in order to easily test new VC issuance mechanism and format.
 
 
 **Identified limitations of the component**
 The tool do not offer administration feature such as ICP
 
 **Possible improvements identified for the component**
 

 **Source and contact**
 
 - *Origin (company / authors)*: OVHCloud + Eviden for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer 
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer/-/blob/develop/README.md?ref_type=heads
 - *Main Contact*: @ghebert & @geromeegron_ovh


###  Sd-JWT, JWT  & Multi-signer Credential Issuing Service

**Short functional description (10 lines)**
The project aims at experimenting the concepts around sd-jwt & jwt credentials as well as W3CDM2 with the newly introduced proofSet & proofChain for securing credentials' integrity
- Implement self-disclosure to all an employee or a company to only disclose the right-full information
- test the issuance of JWT credentials
- test the issuance of multi-signer credentials

**Expected benefits of the component**
Test new protocol and signature concepts to provide feedback while writting specification in the Gaia-X ICAM WG

**Identified limitations of the component**
not yet in the Gaia-X specification

**Possible improvements identified for the component**

**Source and contact**

 - *Origin (company / authors)*: Eviden for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/sd-jwt-issuer 
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/sd-jwt-issuer/-/blob/main/README.md?ref_type=heads
 - *Main Contact*: @ghebert & @hoanhoang


 ### Revocation registry 

**Short functional description (10 lines)**
 
An implementation of Verifiable Credential Status List v2021 and Bitstring Status List 
The Bitstring Status list allows Issuer to manage the lifecycle of the Verifiable Credential they are issuing by exposing various list and then referencing them in the fied CredentialStatus.
Using this component, implementors can create list of both suspention and revocation within the same list, as well as create much more detailed status.
 
 **Expected benefits of the component**
 The tool allow the implementation of VC lifecyle using a status list.
 
 **Identified limitations of the component**
 
 **Possible improvements identified for the component**
 
 **Source and contact**
 
 - *Origin (company / authors)*: Eviden for GXFR FR
 - *Code reference point*:
    - Status list v2021 implementation: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/revocation-registry
    - Bitstring implementation https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/revocation-registry-bitstring 
 - *Documentation reference point*: 
    - Status list v2021 implementation: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/revocation-registry/-/blob/develop/README.md?ref_type=heads
    - Bitstring implementation: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/revocation-registry-bitstring/-/blob/main/README.md?ref_type=heads
 - *Main Contact*: @hoanhoang


### Credential Generator

**Short functional description (10 lines)**
The Credential Generator CLI is an open-source command-line interface (CLI) under the Apache v2.0 license, enabling the generation of JSON-LD files describing Gaia-X compliant objects from CSV files that detail Gaia-X participant and their services.
This CLI also allows uploading these files to a participant user agent, signing them to create Verifiable Credentials, validating compliance with Gaia-X, and pushing them to a service catalog (in case of provider participant).

- *Initialization*: Easily ingest CSV files describing participant and their service offerings into a local SQLite database.
- *JSON-LD Generation*: Easily generate JSON-LD files that comply with Gaia-X LegalParticipant and ServiceOffering objects.
- *Participant agent Upload*: Send the generated files to a web server to make them accessible online.
- *Verifiable Credentials Signing*: Sign the JSON-LD files to turn them into Verifiable Credentials, enhancing their authenticity.
- *Gaia-X Conformity*: Check the files for compliance with Gaia-X standards to ensure optimal interoperability.
- *Integration with Service Catalog*: Push validated files into a federated service catalog as well as Gaia-X CES for centralized management.
 
 **Expected benefits of the component**
The component is useful for a company to generate or regenerate credential at a large scale.
Starting from a CSV, the tool will automatically generate all required files (Did, JSON, VC) and exchange with APIs (GXDCH, Ecosystem Compliance/Membership/Label/Catalogue)
 
 **Identified limitations of the component**
 - Dependency with the User-Agent, VC Issuer and Participant Catalogue
 - Designed to simulate a participant in a catalogue.
 
 **Possible improvements identified for the component**
 - Ongoing development to convert the tool into a service
 
 **Source and contact**
 
 - *Origin (company / authors)*: OVHCloud/WeScale for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/-/blob/main/README.md?ref_type=heads 
 - *Main Contact*: @david.drugeon-hamon

### Service Wizard Tool

**Short functional description (10 lines)**
 Wizard Tool is the entry point for service providers to describe and create their services. The Wizard Tool can be seen as a tool to help service providers creating their VPs and VCs which are mandatory to publish them to the Federated Catalogue with compliance requirements and Aster-x Label (based on Gaia-x Label).
 - Serve as the entry point for service providers to describe and create their services VPs and VCs.
- Ensure services meet the compliance and label requirements to be listed in the Federated Catalogue.
- Streamline and simplify the process of integration into the Gaia-X ecosystem (TrustFramework v22.10) through a unique and user-friendly design (UX).
 
 **Expected benefits of the component**
 - The Wizard Tool stands as the entry point for service providers, allowing them to describe and craft their services in an effective and structured manner. It not only acts as a gateway for providers but also as a guide, aiding them in creating their VPs and VCs. 
 - The Wizard Tool has an easy-to-use design that makes the tricky steps of fitting in with Gaia-X's rules simpler. By following the steps outlined by the tool, providers can ensure that their services not only meet the high standards of Gaia-X (TrustFramework v22.10) but are also presented in an appealing and coherent manner to end users in the catalogue
 - The adoption of the Wizard Tool by service providers guarantees a smooth transition and successful integration into the Gaia-X ecosystem. This makes users trust the system, knowing that each service listed in the federated catalogue adheres to a strict set of standards and criteria.
 
 **Identified limitations of the component**
- The tool is strongly related with the first implementation of GXFS FR Scenario 22.10 and need adjustment to be integrated into a different ecosystem (schema/label)
- dependency with the schema, type of credential and data
- dependency with user-agent and notary tool
 
 **Possible improvements identified for the component**
- allow reusability with more schema and labelling scheme
 
 **Source and contact**
 
 - *Origin (company / authors)*: Docaposte for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/service-description-wizard-tool
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/service-description-wizard-tool/-/blob/main/README.md?ref_type=heads
 - *Main Contact*: @anicaise @vbruna

### Company Wallet (Soon)

**Short functional description (10 lines)**
 A tool for managing contracts and certificates within a company (roles, rights, sharing, access)
 
 
 **Expected benefits of the component**
 
 
 **Identified limitations of the component**
 
 
 **Possible improvements identified for the component**
 
 
 **Source and contact**
 
 - *Origin (company / authors)*: Docaposte for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/wallet-corporate
 - *Documentation reference point*: not ready
 - *Main Contact*: @vbruna

## 🏛 📜Tools for creating, administering and organising the governance of an ecosystem.

### Ecosystem Registry Service

**Short functional description (10 lines)**
This tool is a fork of GXDCH Registry (https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/blob/development/README.md). 
It has been modified in order to be used as the main registry of the Ecosystem (in order to implement an ecosystem own compliance/conformity tool)
The main point of divergence between this registry and the GXDCH revolves are:
- the flexibility of the terms of condition with two additionnal method to add and modify Terms And condition. 
- the lifecycle mecanism which can be implemented in the ecosystem. As per the latest draft of lifecycle mecanism, the most recent method is to use Bitstring Status List. (cf revocation registry). To enable interoperrability within the ecosystem, implementors should maintain a list of all possible messages used by participants in the ecosystem. This registry is able to host and expose this kind of messages
- Inclusion of a new trusted issuer which is its own conformity checker.  
 
 **Expected benefits of the component**
 This tool is based on the GXDCH registry so the feature, routes and behaviour is the identical at both Gaia-X and Ecosystem level.
 Some feature have been added in order to easiest the maintenance and remain conformant with other provided tools.
 
 **Identified limitations of the component**
 As a fork, the tool should follow the GXDCH change list to avoid losing track of the latest changes.
 To that extend, we created a dedicated branch on the GXDCH registry
 
 **Possible improvements identified for the component**
 
 
 **Source and contact**
 
 - *Origin (company / authors)*: fork of https://gitlab.com/gaia-x/lab/compliance/gx-registry/ . Adjunction by Eviden for GXFS FR
 - *Code reference point*: 
    - Fork: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/fork-gxdch/registry
    - Branch: https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/tree/gxfsfr-dev/src?ref_type=heads 
 - *Documentation reference point*: 
    - https://registry.lab.gaia-x.eu/docs/ 
    - https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/fork-gxdch/registry/-/blob/dev/README.md?ref_type=heads
 - *Main Contact*: @hfaure-geors

 ### Schema provider from linkml and Schema registry

**Short functional description (10 lines)**
This service is a combination of two services designed to extend the Gaia-X Ontology by adding new object and attributes specific to the ecosystem.
- From a single point of truth which is a Linkml description, the *schema provider* will generate schema with different formats.
-  Schema Registry is a simple webserver designed to host the schema generated by the schema provider.
A provided exemple is the `Aster-X Located Service Offering` extending `Gaia-X Service Offering`
 
 **Expected benefits of the component**
 The component allow an ecosystem to extend Gaia-X Ontology and to deploy and store it on a webserver.
 We have separated the schema provider and the schema registry to allow the ecosystem to choose how to compose its architecture.
 
 **Identified limitations of the component**
 The tool is using linkml for the generation and packaging of the ontology. Specific knowledge on Linkml is needed to edit the Ontology.
 
 **Possible improvements identified for the component**
 
 
 **Source and contact**
 
 - *Origin (company / authors)*: OvhCloud/Wescale for GXFS FR
 - *Code reference point*: 
    - https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/schema-provider-from-linkml
    - https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/schema-registry/-/tree/develop/schema_registry/infra/api/rest?ref_type=heads
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/schema-provider-from-linkml/-/blob/develop/README.md?ref_type=heads
 - *Main Contact*: @geromeegron_ovh


 ### Ecosystem Compliance/Membership

**Short functional description (10 lines)**

A component that can be used to apply a set of rules to a GX Participant or their service in order to let them access an ecosystem through the use of w3c Verifiable Credentials (as Gaia-X Conformity).
The component is based on the usage of Open-Policy-Agent to apply business rules. 

Using this component, implementors can create personalized rules for both membership and service offering compliance. 
Example of rules can be found in ABC-Checker repository (rego). This component also act as a demonstrator of a versatile component able to understand multiple credential format (jsonld, vc-jwt, sd-jwt).

 
**Expected benefits of the component**
The component allow an ecosystem to be built on top of Gaia-X conformity by adding a layer of ecosystem limited rules to Gaia-X Credentials.

While being similar to GXDCH Clearing house compliance component in usage, this composant revolves around the usage of JSON Path to parse any incoming VP depending on the chosen rules (to allow more flexibility).
This component is able to use jsonld, vc-jwt as well as vc-jwt (VC Data Model 1).

 
**Identified limitations of the component**
The component has a dependency to other GXFS FR Components: User Agent (to store and expose VC), VC issuer (to sign credentials), Ecosystem Verifier (apply technical rules) and ABC-Checker (OPA Server).

**Possible improvements identified for the component**
The component is a prototype and not production ready

**Source and contact**

- *Origin (company / authors)*: Eviden for GXFS FR
- *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-compliance 
- *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-compliance/-/blob/main/README.md?ref_type=heads
- *Main Contact*: @hfaure-geors

### Ecosystem Verifier (to check)

**Short functional description (10 lines)**
A component that can be used to verify the status of a Verifiable Credential. It revolves arround the usage of Open-Policy-Agent. 
The tool can be implemented in multiple ecosystem components (catalogue, membership, ...) where there is a needs to check the validity of a credential or a set of credential. 
Appart from signature verification, implementors are expected to create technical rules in OPA or uses the one used in Aster-X Verifier.

 
**Expected benefits of the component**
Participant in an ecosystem may not be staffed to review code and make sure VC verifier are trustable. To bring trust in the ecosystem, it is possible for an ecosystem operator to provide the tool and/or run it as a service.

This component allow flexibility in technical verification as it apply verification in the form of a tree data structure (failed rules may only produces warnings and not errors) and allow implementors to implements custom rules based on their governance

**Identified limitations of the component**
 This components must run allong an OPA server which uses rego as a programming language. Implementors needs to be able to code using this language in order to implements custom rules
 
**Possible improvements identified for the component**
 The component is a prototype and not production ready
 
**Source and contact**
 
- *Origin (company / authors)*: Eviden for GXFS FR
- *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-verifier
- *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-verifier/-/blob/main/README.md?ref_type=heads
- *Main Contact*: @hfaure-geors

### Ecosystem Notarization Service

**Short functional description (10 lines)**

The notarization service role is to issue VCs of certifications on behalf of auditors or certification issuers who are not yet able to issue VCs. 
We can therefore summarize the role of notarization in a component that makes it possible to transform signed pdf documents of proof of certification into VC.

Notarization Service is composed of two services :
- PDF Signature verification based on european trusted lists.
- Create a VC based on a certification scheme and create a VP to package all VCs

**Expected benefits of the component**
Since auditors or certifications providers do not yet issue certifications in VC format properly interpreted in gaia-x, Notarization Service will be called to transform current signed PDF certification documents into VCs.
 
**Identified limitations of the component**
- Dependency to the DSS Web Application v5.11
- The notarization service has been developped in the context of the GXFS FR demo presented during the 2022 Summit.
- It is aligned with Gaia-X specification 22.04 and strongly depend on GXFS FR Service Wizard tool.
 
**Possible improvements identified for the component**
This tool need to be adapted once the labelling objects are properly defined according to specs 23.11.
 
**Source and contact**
 
- *Origin (company / authors)*: Eviden for GXFS FR
- *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/notarization-service
- *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/notarization-service/-/blob/main/README.md?ref_type=heads
- *Main Contact*: @ghebert

### Labelling Tool

**Short functional description (10 lines)**
API labelling is a compliance and labeling technology process that automates all the verifications necessary to issue a service a specific label. Labelling will make it possible to classify services according to different criteria.
The tool integrate a label implementation based on the Gaia-X label 22.11 description and a GXFR FR ontology stored in the Aster-X Schema Registry.
The tool can be used to issue any type of label based on this ontology.

**Expected benefits of the component**
The tool verify a set of rules defined in an object call ComplianceCertificationScheme.
The tool was designed to demonstrate the issuance of Gaia-X PRC based label according to specification 22.04 and 22.11.

**Identified limitations of the component**
The tool is not compatible with the latest developpement in the Gaia-X Policy Rule Committee.
The ontology used to described the certification scheme has not been validated in the Gaia-X specification.
 
 **Possible improvements identified for the component**
 A modified version using CASCO vocabulary has been described and proposed to the Gaia-X Service Characteristics WG.
 
 **Source and contact**
 
 - *Origin (company / authors)*: Docaposte for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/labelling
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/labelling/-/blob/main/Readme.md?ref_type=heads
 - *Main Contact*: @vbruna


## 📚🗣🛒 Tools for sharing services and setting up a federated catalogue.

### Provider Catalogue

**Short functional description (10 lines)**
Provider Catalogue is a comprehensive management tool for service offerings, data products, and other resources
associated with a provider. It facilitates the addition or removal of self-description Verifiable Credentials (VCs) related to a provider's offerings. 
Furthermore, it provides the capability to push the provider's catalogue to a federated catalogue or to the Gaia-X Centralized Event Service (CES), offering greater visibility and accessibility for the provider's services.
 
**Expected benefits of the component**
The component allow the Provider to control and manage the way service offering, data products and ressource are shared to an ecosystem catalogue or the CES.
The approach is similar to the way company exposes their offerings on their own website (but using Gaia-X Credentials).
It brings control to the provider to choose which and when credentials are pushed to federated catalogue or CES.


**Identified limitations of the component**
The tool is compatible with the GXFS FR federated catalogue and CES.
 
**Possible improvements identified for the component**
The catalogue don't implement any control on the policies attached to data product or service offering. If any restriction expressed in the policies restrict the publication of the ressource in the CES or Federated CAtalogue, it would not be interpreted nor applied.
 
**Source and contact**
 
 - *Origin (company / authors)*: OVHCloud/WeScale for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/-/blob/develop/README.md?ref_type=heads
 - *Main Contact*: @geromeegron_ovh

 ### Federated Catalogue

**Short functional description (10 lines)**

The component implement a federated approach to be used in an ecosystem or federation. Each participant exposes a list of Verifiable Credentials they want to share with the federation through their Participant Catalog. 
The Federated Catalog is a concatenation of all the Participant Catalogs, stored inside a graph database with search and filtering features for Verifiable Credentials.
The system also includes a rule checker to verify the compliance of objects submitted to the federated catalogue with ecosystem policies.
Synchronisation of data is performed either:
- by pushing your catalogue to Redis
- by pushing your catalogue inside the Credential Event Service
- by registrering your catalogue through the API
 
**Expected benefits of the component**
The tool create a graph database based on the Verifiable Credential. The tool centralizes the information in a graph to allow discovery. The synchronization mechanism allow the providers to remains in control of the publicated information.

**Identified limitations of the component**
The tool is not distributed
The tool has a dependency with the provider catalogue and the CES.

**Possible improvements identified for the component**
distribute querying in the graph database

 
 **Source and contact**
 
 - *Origin (company / authors)*: OVHCloud/WeScale for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue/-/tree/develop/documentation?ref_type=heads
 - *Main Contact*: @geromeegron_ovh

 ### Web Catalogue

**Short functional description (10 lines)**
The web catalogue is a web interface based on top of the Federated Catalogue. It offers an easy way to explore, compare, subscribe to and negotiate a variety of services, all in one place. 
The catalogue offers a wide range of services of different types (data product, storage, VPN, VPS, etc.),
some of which have one or more certifications. 
The services available in the catalogue are subject to Gaia-X compliance requirements. 
 
**Expected benefits of the component**
- Provide an overview of services from several providers
- simplify search for specific services and data
- provide information on service labels and Gaia-X Compliance
- Facilitate access, verification, subscription or negociation over a service or a data product.
 
**Identified limitations of the component**
The tool has a dependency with the GXFR FR Federated Catalogue API
The negociation feature is linked with the GXFS FR negociation tool.
The tool is using GXFS FR Aster-X extension of service offering (located service offering) and Label implementation. Any Gaia-X service offering will be displayed but with a shorter description.
 
**Possible improvements identified for the component**
Add the possibility to index and negociate data product stored on a IDS-protocol based connector
 
 **Source and contact**
 
 - *Origin (company / authors)*: Docaposte for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2/-/blob/main/README.md?ref_type=heads
 - *Main Contact*: @vbruna


## 🔏💰 Tools for negociating and contracting.

### Negociation tool

**Short functional description (10 lines)**
 The negociation tool allow a participant and a consumer to reach an agreement on the terms and conditions of usage of the service or the data usage. 
 The tool gather all the information from the participants (consumer and provider) and the service from the Gaia-X Credentials.
 Participants would connect to an interface where the ODRL policies values are mapped to a contract template human readable. The document can be collaboratively edited through an open-source tool (nextcloud). 
 When the agreement is reached, both PDF and ODRL version will be send to the signature gateway.
 
 **Expected benefits of the component**
 The tool is designed to provide a human-readable interface will keeping a relation with the underlying Odrl policies.
 The design of contract template is a good exercice for law and technical people to exchange of the terms of a contract.
 
 **Identified limitations of the component**
 The tool do not implement the dataspace protocol negociation workflow.
 Contract template needs to be edited and provided by the provider for the process to be achieved.
 
 **Possible improvements identified for the component**
 Implement an underlying negociation protocol using ODRL-Request/Offers/Agreement Object for observability.
 
 **Source and contact**
 - *Origin (company / authors)*: Docaposte for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/negociation-tool
 - *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/negociation-tool/-/blob/main/Readme.md?ref_type=heads
 - *Main Contact*: @vbruna

### Signature Gateway

**Short functional description (10 lines)**
The Digital Signature Gateway component helps to connect the Gaia-X ecosystem with the electronic signature solutions from the market via API routes. 
The connector manages only generic interactions:
- Ability to sign any type of document (pdf, word, exel, etc.),
- Adding several documents as appendices
- Addition of several signatories. 
A plugins architecture has been implemented to make it easy to add signature services without having to modify the Digital Signature Gateway source code.

**Expected benefits of the component**
The tool is designed to allow the signature of document with signature tool from the market.
It is possible to add a plugin so any solution from the market could be compatible and used.
So far plugin for solution from docaposte and Eviden could be used.

**Identified limitations of the component**
Object used to sign the document are not a part of the Gaia-X Ontology.

**Possible improvements identified for the component**

**Source and contact**
- *Origin (company / authors)*: 
- *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/digital-signature-gateway
- *Documentation reference point*:  https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/digital-signature-gateway/-/blob/main/README.md?ref_type=heads
- *Main Contact*: @vbruna

### Observability

**Short functional description (10 lines)**
This tool helps to store events relating to the onboarding, contracting and consumption of a data product. It also provides method to search for a set of events. 
 
**Expected benefits of the component**
The tool helps to keep the trace of data transaction within an ecosystem.
An observer could search for event in the context of an audit.
 
**Identified limitations of the component**
The tool do not implement a standard event format. An ADR has been written to jump onto a more suitable solution based on CloudEvent: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/main/adr/003_Observability_based_on_CloudEvents.md?ref_type=heads#gaia-x-observability-based-on-cloudevents
 
 **Possible improvements identified for the component**
 Use Cloudevents, allow authentification for components trigerring events and observers
 
 **Source and contact**
 
 - *Origin (company / authors)*: Dawex for GXFS FR
 - *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/observability
 - *Documentation reference point*: soon...
 - *Main Contact*: @frederic.bellaiche


### Smart Contract Manager (soon)

**Short functional description (10 lines)**
A tool to manage smart contract linked with ODRL constrains (for instance billing & payment in relation with a payment gateway)

**Expected benefits of the component**
The component bring trust between a consumer and a provider by providing a way to safely enforce constraint over a transaction using blockchains.

**Identified limitations of the component**
So far only payment is implemented (fixed periodic and fixed amount)
 
**Possible improvements identified for the component**
add more type of smartcontracts and scenario

**Source and contact**

- *Origin (company / authors)*: Docaposte for GXFS FR
- *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contract-manager
- *Documentation reference point*: soon
- *Main Contact*: @vbruna

### Hands on ODRL (in progress)

**Short functional description (10 lines)**
A repository where people would translate and express contractual clause in ODRL format.
- to provide a book of contractual clause ready-be-used
- to identify terms to be inserted in a Gaia-X ODRL Profile

**Expected benefits of the component**
- Get help on how to translate a contract clause in ODRL
- Get access to sample of ODRL implementation

**Identified limitations of the component**
 
**Possible improvements identified for the component**

**Source and contact**

- *Origin (company / authors)*: IMT, Dawex, Eviden for GXFS FR
- *Code reference point*: 
- *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/workshop-hands-on-odrl
- *Main Contact*: @btabutiaux


## 🛂🔓 Tools for identifying, authorising or granting access to consume data products or services 

### IDP Bridge

**Short functional description (10 lines)**
IDP Bridge aims at bridging the SSI and standard OIDC worlds by providing tools and services to allow user to request authorized verifiable credential in different formats and authenticate with a traditional IDP using their SSI Wallet. Whereas the former is achieved by implementing the OID4VCI protocol, we achieve the latter by implementing OID4VP/SIOPv2 protocols and providing the brokering service which is capable of interacting with OIDC-enabled traditional IDP.
Two scenario are implemented: 
- Human to Machine 
- Machine to Machine

**Expected benefits of the component**
The tool allow any system based on traditionnal identification (OIDC) to get compatible with SSI Wallets.
The tool has been used in our API Gateway and Data Consumption Scenario

**Identified limitations of the component**


**Possible improvements identified for the component**


**Source and contact**

- *Origin (company / authors)*: Eviden for GXFS FR
- *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/idp-bridge
- *Documentation reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/idp-bridge/-/blob/main/README.md?ref_type=heads
- *Main Contact*: @hoanhoang

### Data consumption scenario

**Short functional description (10 lines)**

The project is developed with the purpose of helping Data Consumer to access Datasets included in a previously signed Data Contract. In the context of the scenario, all datasets are hosted on a minio instance on the Data Provider side, the data consumption service is responsible for interacting with Minio to retrieve all the Datasets that a user is authorized to access. 
Through an interface or API, it authenticate the user and display all authorized datasets.

**Expected benefits of the component**
The tool uses keycloak and object storage protocol to authenticate aund provide access to the dataset on an object storage based on an ODRL contract.

**Identified limitations of the component**
The scenario allow access to data and do not provide any limitation the way the data will be transfered. 

**Possible improvements identified for the component**
 
**Source and contact**

- *Origin (company / authors)*: Eviden for GXFS FR
- *Code reference point*: 
    - https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/data-consumption-service-1
    - https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/identity-provider 
- *Documentation reference point*: 
    - https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/data-consumption-service-1/-/blob/develop/README.md?ref_type=heads
    - https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/identity-provider/-/blob/main/README.md?ref_type=heads
- *Main Contact*: @hoanhoang

### API Gateway (soon)

**Short functional description (10 lines)**

The API Gateway authorise a provider to request any underlying route as soon as he is able to attest its membership in the ecosystem.
The gateway will centralise, secure and monitor the services offered by the ecosystem.

**Expected benefits of the component**
When requesting compliance for all its services (like in the credential generator usecase), a provider may send a huge number of requests to different components. Within an ecosysteme, ecosystem services are restricted to its members. 
The gateway allow the centralisation of authentication to secure all the services at once and avoid multiple authentication scenario per components.

**Identified limitations of the component**
This scenario need the issuance of a membership credential

**Possible improvements identified for the component**

**Source and contact**

- *Origin (company / authors)*: OVHCloud/Wescale + Eviden for GXFS FR
- *Code reference point*: So far, still an ADR: 
    - https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/main/adr/005_Aster-X_membership-VC.md?ref_type=heads
    - https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/main/adr/005_Aster-X_membership-VC.md?ref_type=heads#aster-x-membership-vc
- *Documentation reference point*: (soon)
- *Main Contact*: @david.drugeon-hamon @geromeegron_ovh


## 🗄 Tools and extension for the EDC Connector 

### Minio S3 Extension for Eclipse dataspace connector

**Short functional description (10 lines)**
Minio S3 Extension that works with the Eclipse Dataspace Connector allowing operations into the Minio S3 Storage.

**Expected benefits of the component**
All existing components offers compatibility with cloud providers such as AWS or Azure

**Identified limitations of the component**


**Possible improvements identified for the component**
Based on this implement, the OVHCloud extension is on its way.

**Source and contact**

- *Origin (company / authors)*: OvhCloud/Wescale for GXFS FR
- *Code reference point*: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/edc-minio-s3
- *Documentation reference point*: soon
- *Main Contact*: @david.drugeon-hamon
