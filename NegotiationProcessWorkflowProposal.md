
  

# Data Usage Consent and Data Set Negotiation Process Workflow Proposal

  

## Introduction

  

This document outlines the sequence of events involving Data Usage Consent and Data Set Negotiation in the context of data exchange. It details the steps from data discovery to the co-signing of the Data Product Usage Contract (DPUC), including the role of Nextcloud in the negotiation process.

  

## Terms

  

**Dataset consentment** : Once the negociation is done between Consumer and Provider, the Licensor has to sign the document, build a VC and send it to notarization.

The Dataset Consentment contains the policies (RGPD ...)

  

**DataProduct Usage Contract (DPUC)**: Once the Dataset Consentment is issued, the Contract is submitted for signature from our SignatureComponent.

It includes :

- The Dataset consentment

- The Dataset self description

- Other relevant informations

  

## Sequence of Events

  

```mermaid

sequenceDiagram

  

  

participant  Consumer  as  Data  Consumer

  

  

participant  Catalog

  

  

participant  Provider  as  Data  Provider

  

participant  CustomSignatureConenctor  as  SignatureComponent

  

participant  Nextcloud  as  Nextcloud  Negotiator

  

  

participant  Notary  as  Notaries

  

participant  Licensor  as  Data  Licensor

  

Note  over  Consumer, Catalog: Steap 1 : Data discovery process

  
Consumer->>  Catalog  : Consumer logins with Keycload using it's wallet with a LegalPerson and Participant verifiable presentation

Consumer->>Catalog: Discover Data Product

  

  

Catalog->>Consumer: Provide Self-Description & Usage Consent Terms

  


Consumer->>Catalog: Discover Data Product

  

Catalog->>Consumer: Provide Self-Description & Usage Consent Terms

Consumer->>Catalog: Ask for a negociation process

  

Note  over  Consumer, Nextcloud: Step 2 : Negotiation Process (modification and direct approval from provider scenario)

  

Catalog->>Nextcloud: Ask for a negociation process creation

Nextcloud  ->>  Nextcloud  : Create a negociation file using a pre-existing template on the provider's dataspace.

Nextcloud  ->>  Nextcloud  : Share the negociation file to both provider's and consumer's space.

Nextcloud->>Provider: Notify provider about the newly opened negociation

Consumer->>Nextcloud: Login using the same process as Step 1.

Provider->>Nextcloud: Login using the same process as Step 1.

Consumer->>Nextcloud: Modify Self-Description (e.g., Price, Access Rights) and possibly policies

  

Provider->>Nextcloud: Accept updated values

  

Consumer->>Nextcloud: Accept modificatiohns

  

Note  over  Consumer, Licensor: Step 3 : Dataset Consentment generation

  

Provider->>Licensor: Submit Dataset Consentment for signature

  

  

Licensor->>Licensor: Signs the consentment

  

Licensor->>  Provider  : Sends signed Dataset Consentment

  

Licensor  ->>  Notary  : Store signed Dataset Consentment

  

Note  over  Consumer, SignatureComponent : Step 4 : DPUC generation and signature

  

Licensor  ->>  Nextcloud: Sends signed Dataset Consentment for DPUC finalization

  

Nextcloud  ->>  Nextcloud: Includes Dataset Consentment inside DPUC.

  

Nextcloud  ->>  SignatureComponent  : Submit DPUC for signature

SignatureComponent  ->>  Provider  : Notify provider about a signature process opening

SignatureComponent  ->>  Consumer  : Notify consumer about a signature process opening

  

Consumer->>SignatureComponent  : Sign DPUC

  

Provider->>SignatureComponent  : Sign DPUC

  

SignatureComponent  ->>  Consumer: Sends signed DPUC

  

SignatureComponent  ->>  Provider: Sends signed DPUC

  

Provider->>  Notary  : Stores DPUC
  

```

### 1. Data Set Discovery

The Data Consumer logings using a Keycloak provider with a custom identity provider, then gives it's LegalPerson as well as it's Participant Credential.
He then discovers and selects a specific Data Product from a catalog, reviewing the Data Product Self-Description (I.E, . This includes understanding the data scope, operational characteristics, and any associated policies.
  
### 2. Data Set Negotiation

The Consumer engages with Nextcloud to negotiate the data product's terms. The Consumer can modify self-descriptions, such as price and access rights, and the Provider (data provider) accepts the updated values. Once both parties agree, the Consumer accepts the modifications.

  
### 3. Dataset Consentment generation

The Provider submits the Dataset Consentment to the Licensor for signature. The Licensor signs the consentment, and the signed consentment is sent back to the Provider and stored by the Notary.

  
### 4. Co-Signing of DPUC


Nextcloud incorporates the signed Dataset Consentment into the Data Product Usage Contract (DPUC). Nextcloud submits the DPUC to the SignatureComponent for signatures. The Consumer and Provider both sign the DPUC. The final signed DPUC is stored by the Notary.

  

## Current workflow implementation (demo)

  

Current workflow proposes to skip the Dataset Consentment signed by the Licensor.

```mermaid
sequenceDiagram

  

participant  Consumer  as  Data  Consumer

  

participant  Catalog

  

participant  Provider  as  Data  Provider

participant  CustomSignatureConenctor  as  SignatureComponent

participant  Nextcloud  as  Nextcloud  Negotiator

  

participant  Notary  as  Notaries

participant  Licensor  as  Data  Licensor

  

Note  over  Consumer, Catalog: Step 1 : Data discovery process

Consumer->>  Catalog  : Consumer logins with Keycload using it's wallet with a LegalPerson and Participant verifiable presentation

Consumer->>Catalog: Discover Data Product

  

Catalog->>Consumer: Provide Self-Description & Usage Consent Terms

Consumer->>Catalog: Ask for a negociation process

Note  over  Consumer, Nextcloud: Step 2 : Negotiation Process (modification and direct approval from provider scenario)

Catalog->>Nextcloud: Ask for a negociation process creation

Nextcloud  ->>  Nextcloud  : Create a negociation file using a pre-existing template on the provider's dataspace.

Nextcloud  ->>  Nextcloud  : Share the negociation file to both provider's and consumer's space.

Nextcloud->>Provider: Notify provider about the newly opened negociation

Consumer->>Nextcloud: Login using the same process as Step 1.

Provider->>Nextcloud: Login using the same process as Step 1.

Consumer->>Nextcloud: Modify Self-Description (e.g., Price, Access Rights) and possibly policies

  

Provider->>Nextcloud: Accept updated values

  

Consumer->>Nextcloud: Accept modificatiohns

  

Note  over  Consumer, SignatureComponent : Step 3 : DPUC generation and signature

Nextcloud  ->>  SignatureComponent: Sends finalized contract

SignatureComponent  ->>  Provider  : Notify provider about a signature process opening*

SignatureComponent  ->>  Consumer  : Notify consumer about a signature process opening

Consumer->>SignatureComponent  : Sign DPUC

Provider->>SignatureComponent  : Sign DPUC

SignatureComponent  ->>  Consumer: Send signed DPUC

SignatureComponent  ->>  Provider: Send signed DPUC

Provider->>  Notary  : Store DPUC
  ```

## Nextcloud for Data Usage Consent Negotiation

  

Nextcloud can serve as a Contract Negotiation Connector, facilitating the negotiation process between the Data Consumer and Data Provider. It allows both parties to collaborate on the Data Product Self-Description, including modifications to the Data License and other enforceable Terms of Usage. Nextcloud is opensource is widely used

  

### Example Fields for Modification:

-  **Price:** Negotiating the cost associated with data usage.

-  **Access Rights:** Defining who can access the data and under what conditions.

-  **Policy Modifications:** Adjusting policies to comply with regulations such as GDPR.

  

### Integration with the workflow

  

- A colaborative document sharing and editing functionnality.
- A notification functionnality.
- A Keycloak implementation.
- A gateway between catalog and Nextcloud.
- A gateway between Netxcloud and the signature component.

  

## Policies for Data Exchange

  

Policies in data exchange reflect different aspects to specify terms and conditions for data usage. These include:

  

-  **Contract Policies:** Interoperable policies that form the basis for a contract between participants.

-  **Runtime Policies:** Derived from Contract Policies and used for execution.

-  **Usage Control:** An extension to traditional access control, regulating what must (not) happen to data.

-  **Access Control:** Restricts access to resources, defining attribute-based access control policies.

  