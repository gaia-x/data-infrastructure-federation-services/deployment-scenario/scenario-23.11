# Step02 - Test Data.md

### Mont Blanc IOT (aka company A)
https://montblanc-iot.provider.gaia-x.community/participant/39100b2d1113ff9136dc29a65dcae51f48e73735df72e38c1c375f3c7a08ba87/data.json

```json
{
    designation': 'Mont Blanc IOT',
    'registrationNumber': '123456789',
    'legalAddress': {
        'country-name': 'FR',
        'street-address': 'Place de l'aiguille du midi',
        'locality': 'Chamonix',
        'postal-code': '74400'
    },
    'headQuarterAddress': {
        'country-name': 'FR',
        'street-address': 'Place de l'aiguille du midi',
        'locality': 'Chamonix',
        'postal-code': '74400'
    },

    'leiCode': '',
}
```

