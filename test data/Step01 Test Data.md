#Step01 - Test Data

## Participant Informations

### ABC-Federation

```json
{
    'designation': 'ABC Federation',
    'registrationNumber': '',
    'legalAddress': {
        'country-name': 'BE',
        'street-address': 'Avenue des Arts 6-9',
        'locality': 'Bruxelles',
        'postal-code': '1210'
    },
    'leiCode': '',
}
```

### Dufour Storage (aka company B)
https://dufourstorage.provider.gaia-x.community/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json

```json
{
    'designation': 'Dufour Storage',
    'registrationNumber': '123456789',
    'legalAddress': {
        'country-name': 'IT',
        'street-address': 'Via Europa',
        'locality': 'Milano',
        'postal-code': '20121'
    },
    'headQuarterAddress': {
        'country-name': 'IT',
        'street-address': 'Via Europa',
        'locality': 'Milano',
        'postal-code': '20121'
    },
    'leiCode': '',
    'termsAndConditions': 'https://dufourstorage.gaia-x.community/terms-and-conditions/'
}
```


Location 
- Austria, InnsBruck
- Italy, Milano
- France, Grenoble
- Germany, Munchen

Services
- Storage Service
- Identity Provider
- Backup Service
