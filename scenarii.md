# Scenario: Acquisition of data 

## Steps 

* Onboard Company B as a Participant (“Data Provider”), then create VC for employee Bob
* Add Data Product offering to the federated Data Catalog. 
* Onboard Company A as a Participant (“Data Consumer”), then create VC for employee Alice
* Alice searches the Data Catalog for a Data Product based on criteria 
* Alice and Bob Bob negotiate and agree on Data Contract
* Alice get access to the Data Product

Assumptions:
* Company A and B are part of a same federation created for the scenario
* Company A, company B, Alice and Bob have a Wallet with a valid DID
* Wallet have all VC used for claims like ISO_27001 and not provided by Gaia-x or GXCDH

Schemas proposed in the document are __as functionnal as possible__ and the purpose is not to describe technicaly how it works in details and let some implementation choices. 

## Schema

### Part I, onboard  

Description:
* Bob is the administrator of Company B and want to identify Company B as a provider to the GXDCH 
Steps: 
* Bob retrieves Participant (“Data Provider”) schema description
* Bob fulfills the provider self description according to the schema
* Bob send the Self-description to the GXDCH Trust Framework API
* GXDCH Trust Framework validates the provider Self-Description request (checks the claims, run accreditation process) then returns a VC

Assumptions:
* WalletCompanyB have a first VC employee created for the administrator of Company B
* Bob issue a VC Participant 
* The administrator (Bob) may use the Wallet Company B

```mermaid
sequenceDiagram
actor Bob
participant WalletBob
participant notarization
participant compliance
Bob ->> WalletBob: create a legal person participant vc 
Bob ->> WalletBob: request to add a registrationNumberCompliance
WalletBob ->> notarization: request registrationNumberCompliance and add it 
Bob ->> WalletBob: request a legal person participant compliance 
WalletBob ->> compliance: request participant compliance for Bob 
Bob ->> WalletBob: add participant compliance for Bob to the Wallet 
Bob ->> compliance: request a legal participant Data Provider Compliance
```

### Part II Add Data Product offering to the federated Data Catalog. 

Description :
* Bob is the administrator of Company B declared as provider inside the GXDCH
* Bob want to expose a Data Product according to its terms of use

Steps :
* Bob retrieves “Data Product" schema description 
* Bob fulfills a service Self-Description of the Data Product according to the schema (title, description, tags, data set, types, data policy, license, ...) 
* Bob send the Self-description to the GXDCH Trust Framework API
* Bob calls the GXDCH Trust Framework API to validate the Data Product Self-Description request.
* Bob register his Data Product offering to the federated Data Catalog

```mermaid
sequenceDiagram
actor Bob
participant WalletCompanyB
participant participantCatalog
participant FederetedCatalog
participant compliance
Bob ->> FederetedCatalog: Bob declare the participant catalog endpoint to index the participantCatalog 
FederetedCatalog ->> participantCatalog: index the Catalog 
Bob ->> Bob: create a Data prodcut and sign it (wizard or command line)
Bob ->> WalletCompanyB: add the VC data product for the data product into to Wallet 
Bob ->> participantCatalog: add a new Data Product from the Catalog 
FederetedCatalog ->> participantCatalog: get the new Data Product from the Catalog
Bob ->> compliance: request compliance for the Data Product 
Bob ->> WalletCompanyB: add the compliance to the Wallet 
Bob ->> WalletCompanyB: request a VP for the service
Bob ->> participantCatalog: add the service to the federated catalog 
FederetedCatalog ->> participantCatalog: index the catalog to get the new Data Product 
```

### Part III Onboard Company A as a Participant (“Data Consumer”), then create VC for employee Alice

Description :
* Anna is the administrator of Company A and wants to on-board Company A as a federated service consumer, then create VC for Alice, a BI Analyst of Company A

Steps :
* Anna retrieves Participant (“Data Consumer”) schema description
* Anna fulfills the Participant (“Data Consumer”) Self-Description according to the schema
* Anna send the Self-description to the GXCDH Trust Framework API
* GXCDH  Trust Framework validates the participant (“Data Provider”) Self-Description request (checks the claims, run accreditation process) then returns a VC
* Anna creates a signed VC for Alice with specific attributes

```mermaid
sequenceDiagram
actor Anna
actor Alice
participant WalletAnna
participant WalletAlice 
participant notarization
participant compliance
participant CompanyRegistry
Anna ->> WalletAnna: create a legal person participant data consumer object 
Anna ->> WalletAnna: request to add a registrationNumberCompliance
WalletAnna ->> notarization: request registrationNumberCompliance
Anna ->> WalletAnna: request a legal person participant compliance 
WalletAnna ->> compliance: request participant compliance for Anna 
Anna ->> CompanyRegistry: update company registry to add Alice as employee with right to get a gaia-x participant vc 
Alice ->> WalletAlice: request Gaia-x Human Person Participant VC  
WalletAlice ->> CompanyRegistry: request Gaia-x Human Person Participant VC
Anna ->> CompanyRegistry: allow Anna to get a Gaia-x Human Person Participant VC
WalletAlice ->> CompanyRegistry: add Gaia-x Human Person Participant VC
```

### Part IV Alice searches the Data Catalog for a Data Product based on criteria

Description:
* Alice is a BI analyst from Company A and wants to search a Data Product 
Steps :
* Alice connects to the federation Portal
* The federation Portal checks Alice credentials in regards to the authentication module and grants access
* Alice search Data Product in the federated Data Catalog based on the attributes of a Data Product self description (title, description, tags, data set, types, localization, terms & conditions, license, ...)
* Alice discovers the Data Product from Company B

```mermaid
sequenceDiagram
actor Alice
participant WebFederationCatalog
participant WalletAlice
participant IAM as Identity and Access Management
Alice ->>IAM:  request access 
IAM ->> WalletAlice: Verifiable Presentation Request (VPR)
WalletAlice ->> IAM: Verifiable Presentation (VP)
IAM ->> Alice: provide access (token)
Alice ->> WebFederationCatalog: search a data product using an authenticated session
```

### Part V Alice and Bob negotiate and agree on Data Contract

Description:
* Alice of company A negotiates, agrees and signs a Data Contract with Bob of company B

Steps:
* Alice and Bob agree on terms via a Data Intermediary (Assumption: in the demo, the Federation Portal acts as a Data Intermediary). Terms are defined in the Data Product Description which serves as the starting point of the negociation (instanciated into a Data Product Usage Contract) 

A typical negotiation workflow is the following:

![image](contract.negotiation.state.machine.png)

* A Data Product Usage Contract is digitally signed
* The Data Product Usage Contract VC is sent by the Data Intermediary to the GXCDH Trust Framework API to validate the schema
* The Data Product Usage Contract is notarized and logged by the Data Intermediary

Note: Data Product Conceptual Model

![image](dataproductmodel.png)

```mermaid
sequenceDiagram
actor Alice
actor Bob
participant WebFederationCatalog
participant ProviderContract
Alice ->> WebFederationCatalog: request a standard contract url
Alice ->> ProviderContract: display a contract including some attachments to have VC ... and sign it
Alice ->> ProviderContract: sign contract
Bob ->> ProviderContract: sign contract
ProviderContract ->> Alice: issue a VC Contract (including all links signed + ODRL Contract is present)
ProviderContract ->> Bob: issue a VC Contract (including all links signed + ODRL Contract is present)
```

TODO update ontology to have specific fields to start a contract process from the web front (provider url to initiate a process)

Assumtions: 
* Company B already have a contract with a ProviderContract 
* Assumption on DataProduct: API based, located in Europe, Terms & conditions: one month offering, no more than 10 calls / min 

### Part VI Alice get access to the Data Product

Description :
* Alice is a BI analyst from Company A and wants to access the Data Product 

Steps :
* Alice connects to the Federation Portal
* URI is defined in the distribution part of the Data Product 
* The Federation Portal checks Alice credentials in regards to Data Contract, checks for Data Consent (Nov. 2023) and grants access to the Data Product (Sept 2023)
* The Data Transaction is notarized by the Federation Portal (seen DataProduct Conceptual Model)

Question : what is a data consent ? a VC ?
Note: Data consent is related to GDPR -> Data Usage Agreement. Is a a VC, but not yet specified. 

```mermaid
sequenceDiagram
actor Alice
participant WalletAlice 
participant FederationPortal
participant ProviderService
Alice ->> FederationPortal: required authentication
FederationPortal ->> WalletAlice: Verifiable Presentation Request
Alice ->> WalletAlice: provide the VP Request
WalletAlice ->> Alice: provide Access Token
Alice ->> FederationPortal: access to a data product and check data consent
```

# Scenario:On-boarding company B as Data Provider, create VC for Bob

### Part VII Alice is leaving company A 

Description:
* Alice of company A quit the job and Anne MUST revoke the rights

Steps:
* Anna revoke VC employee for Alice 
* Alice cannot acces ressources anymore 

### Part VIII chain of Trust

Description :
* Alice Demonstration of a Trusted Data Transaction 

Steps :
* Alice connects to the Federation Portal
* Alice get access to logs for Data Contract (notatization)
* Alice get access to logs for Data Transactions (logs)
* (optional) Alice get access to Data Consent logs 

