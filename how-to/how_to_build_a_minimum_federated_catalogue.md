# How to build a minimum Federated-Catalogue

- [Objective](#objective)
- [Prerequisites](#prerequisites)
  - [Mandatory components](#mandatory-components)
  - [Forge cryptographic material for your provider components](#forge-cryptographic-material-for-your-provider-components)
- [With Docker Compose](#with-docker-compose)
  - [Launch the docker-compose file to launch the components](#launch-the-docker-compose-file-to-launch-the-components)
  - [Initialise DufourStorage provider catalogue](#initialise-dufourstorage-provider-catalogue)
  - [Register your provider catalogue inside the Federated-Catalogue](#register-your-provider-catalogue-inside-the-federated-catalogue)
  - [Launch the HTTP synchronisation](#launch-the-HTTP-synchronisation)
- [With each container to launch manually](#with-each-container-to-launch-manually)
  - [Launch Database components](#launch-database-components)
  - [Launch User-Agent and VC-Issuer](#launch-user-agent-and-vc-issuer)
  - [Launch Provider catalogue](#launch-provider-catalogue)
  - [Initialize provider catalogue](#initialize-provider-catalogue)
  - [Aliment a federated-catalogue](#aliment-a-federated-catalogue)
- [Optional operation](#optional-operation)

## Objective

Have a minimal configuration to build and launch a Federated-Catalogue in local.

> Note: This guide is for testing purposes only and should not be used in production.
> You will not have any Gaia-X compliance due to self-signed certificate.

We offer you two methods to launch this Minimum Federated-Catalogue,

- First one with a docker-compose file to launch minimum necessaries components to build your Federated-Catalogue
- Second one with a description of each Container to launch to have a better comprehension of each component, to be
  functional you will need to launch your container with access to host network.

## Prerequisites

### Mandatory components

To be able to build and launch a local federated catalogue, you need to have the following tools installed:

- [Docker](https://docs.docker.com/get-docker/
- [Docker-compose](https://docs.docker.com/compose/install/)

And the following gaia-x components:

- [User-Agent](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent)
- [VC-Issuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer)
- [Credential-generator](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator)
- [Provider-catalogue](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue)
- [Federated-Catalogue](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue)

### Forge cryptographic material for your provider components

Before deploying any component, you need to forge cryptographic material. To proceed
you need to generate a self-signed certificate and a private key.

```bash
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes
```

Fill the required fields and you will get a `key.pem` and a `cert.pem` file.
Store them in a directory and export the path to the environment variable `CERT_DIR`.

```bash
export CERT_DIR=/path/to/cert/dir
```

## With Docker Compose

### Launch the docker-compose file to launch the components

````shell
cd how_to_build_a_minimum_federated_catalogue/
docker-compose up
````

When the docker-compose file is terminated, you have following components launched :

- Redis database
- GraphDB database
- User-agent component
- VC-Issuer component
- Provider-Catalogue
- Federated-Catalogue

### Initialise DufourStorage provider catalogue

You need to install credential-generator to initialize the provider catalogue. Follow the [installation guide](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator)

Before initializing the provider catalogue, you need to describe the service offering thanks to this [file format
](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/-/blob/main/docs/tutorials/00_database_initialization/01_input_file_format.md?ref_type=heads)

create a directory structure to store the federation and provider description.
For example, create a directory named `federation-data`, create a subdirectory named `federation-x` and `dufourstorage` inside the `federation-data` directory. Export the path to the environment variable `FEDERATION_DATA`.

#### Describe federation

In the federation directory, create a file named 1-Participant.csv and fill it with the following content:

```csv
Role;Id;Designation;Street Address;Postal Code;Locality;Country Name;LeiCode;VatID;Terms And Conditions
federation;CAB-00;federation-x;123 Boulevard de l'Innovation;75001;Paris;FR-75;;FR99999999999;
```

Create a file named LabelingCriteria-Matrix.csv and fill it with the following content:

```csv
ID;Champ1;Compliance Resource Id;CR-ID;Ref Designation;Extrait;Category;Valid from;Valid Until;Reference Doc;Assertion Method;Assertion Date;Issuer Designation
```

Create a file named ComplianceResource.csv and fill it with the following content:

```csv
Category;criterion;criterion-name;description;used for label level;applicable;self-assessed;Verification Process ;Self-declaration / self-assesment;ISO19944;CISPE Data Protection Code of Conduct:::CR-01:Self-declared;CISPE Data Protection Code of Conduct:::CR-01:Certified;C5:::CR-13:Certified:::CR-16:Certified;TISAX:::CR-24:Certified;SOC2:::CR-12:Certified;SecNumCloud:::CR-10:Certified:::CR-09:Certified;ISO 27001:::CR-04:Certified;CSA STAR Level 1;CSA STAR Level 2:::CR-21:Certified;SWIPO IaaS Code:::CR-02:Certified:::CR-02:Self-Declared;SWIPO SaaS Code:::CR-35:Certified:::CR-35:Self-Declared
```

#### Describe provider service offering
In the dufourstorage directory, create a file named 1-Participant.csv and fill it with the following content:

```csv
Role;Id;Designation;Street Address;Postal Code;Locality;Country Name;LeiCode;VatID;Terms And Conditions
provider;;Dufour Storage;Via Europa;20121;Milano;IT-MI;;123456789;https://www.example.com/dufourstorage/terms-and-conditions/
```

a file named 3-Locations.csv and fill it with the following content:

```csv
;Locationid;Location Id;Provider Designation;City;State/Dpt;Country
1;Locid-DUFOUR STORAGE001;LOC-01;MIL-01;MILANO;;IT
2;Locid-DUFOUR STORAGE002;LOC-02;INN-01;INNSBRUCK;;AT
3;Locid-DUFOUR STORAGE003;LOC-03;GRE-01;GRENOBLE;;FR
4;Locid-DUFOUR STORAGE004;LOC-04;MUN-01;MUNCHEN;;DE
```

a file named 4-Services.csv and fill it with the following content:

```csv
;Servid;Service Id;Designation;Service Version;Comply With;Available In;
1;Servid-DUFOUR STORAGE001;S-01;Storage Service;1;"CR-04; CR-10";LOC-03;
2;Servid-DUFOUR STORAGE002;S-02;Identity Provider;1;"CR-04; CR-10";LOC-03;
3;Servid-DUFOUR STORAGE003;S-03;Backup Service;1;"CR-04; CR-10";LOC-03;
4;Servid-DUFOUR STORAGE004;S-04;Storage Service;1;;"LOC-01 ; LOC-02; LOC-04";
```

a file named 5-Layers.csv and fill it with the following content:

```csv
Layer;Service ID
IAAS;"S-01;S-02;S-03;S-04"
```
a file named 6-Keywords.csv and fill it with the following content:

```csv
Keyword;Service IDs
Storage;S-01
Authentication;S-02
Backup;S-03
Storage;S-04
```
// Credential generator
// TODO : check si possible de générer catalogue sans compliance
// a la fin -> catalogue généré au format json et VC signés
// provider-catalogue expose catalogue sur /catalogue

#### Bootstrap credential-generator database

Before bootstrapping provider data, you need to ingest the federation and provider data into the credential-generator database. See the following page to have more information about the [database initialization](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/-/blob/main/docs/tutorials/00_database_initialization/02_import_csv_files_into_database.md?ref_type=heads)
To do so, you need to launch the credential-generator with the following command:

```bash
poetry run credential-generator\
init \
--name federation-x\
--input-root-directory ${FEDERATION_DATA}
```

#### Bootstrap provider catalogue

To bootstrap the provider catalogue, you need to launch the credential-generator with the following command:

```bash
poetry run credential-generator\
  provider bootstrap \
  --name dufourstorage \
  --parent-domain provider.127-0-0-1.nip.io \
  --federation-parent-domain 127-0-0-1.nip.io \
  --output-root-directory ${PROVIDER_CATALOGUE_DATA}
```
#### Upload provider generated files

You need to upload the generated files to the provider user-agent. To do so, you need to launch the following command:

```bash
poetry run credential-generator\
  provider upload \
  --name dufourstorage \
  --parent-domain provider.127-0-0-1.nip.io \
  --output-root-directory ${PROVIDER_CATALOGUE_DATA} \
  --api-key ${API_KEY_AUTH} \
  --auditor-api-key ${API_KEY_AUTH}
```

#### Upload provider generated files into provider catalogue

You need to upload the generated files to the provider catalogue. To do so, you need to launch the following command:

```bash
poetry run credential-generator\
  provider sync \
  --name dufourstorage \
  --parent-domain provider.127-0-0-1.nip.io \
  --api-key ${PROVIDER_CATALOGUE_API_KEY_AUTH}
```

### Register your provider catalogue inside the Federated-Catalogue

````shell
curl --location --request POST 'https://localhost:5002/provider-catalogue-url?provider_catalogue_url=https%3A%2F%2Fcatalogue-api.dufourstorage.dev.gxdch.ovh%2Fcatalog' \
--header 'x-api-key: my_api_key'
````

You can check the registration with following command :

````shell
curl --location 'http://0.0.0.0:5002/provider-catalogue-url' --header 'x-api-key: my_api_key'
````

### Launch the HTTP synchronisation

````shell
curl --location --request POST 'http://0.0.0.0:5002/launch-async-http-synchro' --header 'x-api-key: a-key-for-security'
````

You can follow the synchronisation status with following command :

```shell
curl --location 'http://0.0.0.0:5002/http-synchro-status'
```

When the synchronisation is DONE, you can request Federated-Catalogue.

````shell
curl --location 'http://0.0.0.0:5002/query_page?page=0&size=25' --header 'Content-Type: application/json' --data '{}'
````

## With each container to launch manually

### Launch Database components

Create a volume to store the database data and export the path to the environment variable `REDIS_DATA`.
and launch a Redis database with port 6379

```shell
docker volume create REDIS_DATA
docker inspect REDIS_DATA
export REDIS_DATA=/path/to/mountpoint
docker run --rm -d --name redis -p 6379:6379 \
  -v $REDIS_DATA:/data \
  -t redis:7.2.4-alpine
```

Create a volume to store the database data and export the path to the environment variable `GRAPH_DATA`.
and launch a graphdb database with port 7200

````shell
docker volume create GRAPH_DATA
docker inspect GRAPH_DATA
export GRAPH_DATA=/path/to/mountpoint
docker run --name graphdb --rm -d -p 7200:7200 \
  -v $GRAPH_DATA:/opt/graphdb/data/repositories \
  khaller/graphdb-free:10.1.0
````

### Launch User-Agent and VC-Issuer

Launch VC-Issuer with the following command:

```bash
docker run --rm  -d --name vc-issuer -p 5003:5001 \
  -v $CERT_DIR/key.pem:/app/creds/tls.key \
  -v $CERT_DIR/cert.pem:/app/creds/tls.crt \
  -e TLS_CERT_PATH="/app/creds/tls.crt" \
  -e PRIVATE_KEY_PATH="/app/creds/tls.key" \
  -e DID_WEB_ISSUER="did:web:dufourstorage.provider.127-0-0-1.nip.io" \
  -e PARENT_DOMAIN="provider.127-0-0-1.nip.io" \
  registry.gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer:1.0.3
```

Create a directory to store the generated credentials and export the path to the environment variable `CREDENTIALS_DIR`.
and launch User-Agent with the following command:

Export user-agent api key to the environment variable `API_KEY_AUTH`

```bash
docker run --rm  -d --name user-agent -p 5001:5001 \
  -v $CERT_DIR/key.pem:/opt/participant-agent/creds/tls.key \
  -v $CERT_DIR/cert.pem:/opt/participant-agent/creds/tls.crt \
  -v $CREDENTIALS_DIR:/opt/participant-agent/public \
  -e API_KEY_AUTHORIZED=${API_KEY_AUTH} \
  -e TLS_CERT_PATH="/opt/participant-agent/creds/tls.crt" \
  -e PARTICIPANT_NAME="dufourstorage" \
  -e PARENT_DOMAIN="provider.127-0-0-1.nip.io" \
  -e PORT=5001 \
  -e VC_ISSUER_URL="http://dufourstorage.provider.127-0-0-1.nip.io:5003" \
  -e CATALOG_API_ENDPOINT="http://catalogue.127-0-0-1.nip.io:6001" \
  -e CATALOG_API_KEY="this-is-a-test" \
  -e PUBLIC_FOLDER_TO_EXPOSE="//opt/participant-agent/public" \
  -e LOG_LEVEL="DEBUG" \
  registry.gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent:1.1
```

### Launch Provider catalogue

Create a volume to store the provider catalogue data and export the path to the environment
variable `PROVIDER_CATALOGUE_DATA`.

Export provider catalogue api key to the environment variable `PROVIDER_CATALOGUE_API_KEY_AUTH`
And launch the provider catalogue with the following command:

```bash
docker volume create PROVIDER_CATALOGUE_DATA
docker inspect PROVIDER_CATALOGUE_DATA
export PROVIDER_CATALOGUE_DATA=/path/to/mountpoint
docker run --rm -d --name provider-catalogue -p 6001:5001 \
  -v $PROVIDER_CATALOGUE_DATA:/app \
  -e LOG_LEVEL=DEBUG \
  -e ENVIRONMENT=DEV \
  -e API_KEY_AUTHORIZED=$PROVIDER_CATALOGUE_API_KEY_AUTH \
  -e API_PORT_EXPOSED=5001 \
  -e REDIS_HOST="redis://localhost:6379/2" \
  -e CES_URL="https://ces-development.lab.gaia-x.eu" \
  -e FEDERATION_REPOSITORY="abc-federation" \
  -e PARTICIPANT_NAME="dufourstorage" \
  -e PARENT_DOMAIN="provider.127-0-0-1.nip.io" \
  -e PARTICIPANT_AGENT_HOST="http://localhost:5001" \
  -e PARTICIPANT_AGENT_SECRET_KEY="this-is-a-test" \
  registry.gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue:1.2
```

### Initialize provider catalogue
You need to install credential-generator to initialize the provider catalogue. Follow the [installation guide](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator)

Before initializing the provider catalogue, you need to describe the service offering thanks to this [file format
](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/-/blob/main/docs/tutorials/00_database_initialization/01_input_file_format.md?ref_type=heads)

create a directory structure to store the federation and provider description.
For example, create a directory named `federation-data`, create a subdirectory named `federation-x` and `dufourstorage` inside the `federation-data` directory. Export the path to the environment variable `FEDERATION_DATA`.

#### Describe federation

In the federation directory, create a file named 1-Participant.csv and fill it with the following content:

```csv
Role;Id;Designation;Street Address;Postal Code;Locality;Country Name;LeiCode;VatID;Terms And Conditions
federation;CAB-00;federation-x;123 Boulevard de l'Innovation;75001;Paris;FR-75;;FR99999999999;
```

Create a file named LabelingCriteria-Matrix.csv and fill it with the following content:

```csv
ID;Champ1;Compliance Resource Id;CR-ID;Ref Designation;Extrait;Category;Valid from;Valid Until;Reference Doc;Assertion Method;Assertion Date;Issuer Designation
```

Create a file named ComplianceResource.csv and fill it with the following content:

```csv
Category;criterion;criterion-name;description;used for label level;applicable;self-assessed;Verification Process ;Self-declaration / self-assesment;ISO19944;CISPE Data Protection Code of Conduct:::CR-01:Self-declared;CISPE Data Protection Code of Conduct:::CR-01:Certified;C5:::CR-13:Certified:::CR-16:Certified;TISAX:::CR-24:Certified;SOC2:::CR-12:Certified;SecNumCloud:::CR-10:Certified:::CR-09:Certified;ISO 27001:::CR-04:Certified;CSA STAR Level 1;CSA STAR Level 2:::CR-21:Certified;SWIPO IaaS Code:::CR-02:Certified:::CR-02:Self-Declared;SWIPO SaaS Code:::CR-35:Certified:::CR-35:Self-Declared
```

#### Describe provider service offering
In the dufourstorage directory, create a file named 1-Participant.csv and fill it with the following content:

```csv
Role;Id;Designation;Street Address;Postal Code;Locality;Country Name;LeiCode;VatID;Terms And Conditions
provider;;Dufour Storage;Via Europa;20121;Milano;IT-MI;;123456789;https://www.example.com/dufourstorage/terms-and-conditions/
```

a file named 3-Locations.csv and fill it with the following content:

```csv
;Locationid;Location Id;Provider Designation;City;State/Dpt;Country
1;Locid-DUFOUR STORAGE001;LOC-01;MIL-01;MILANO;;IT
2;Locid-DUFOUR STORAGE002;LOC-02;INN-01;INNSBRUCK;;AT
3;Locid-DUFOUR STORAGE003;LOC-03;GRE-01;GRENOBLE;;FR
4;Locid-DUFOUR STORAGE004;LOC-04;MUN-01;MUNCHEN;;DE
```

a file named 4-Services.csv and fill it with the following content:

```csv
;Servid;Service Id;Designation;Service Version;Comply With;Available In;
1;Servid-DUFOUR STORAGE001;S-01;Storage Service;1;"CR-04; CR-10";LOC-03;
2;Servid-DUFOUR STORAGE002;S-02;Identity Provider;1;"CR-04; CR-10";LOC-03;
3;Servid-DUFOUR STORAGE003;S-03;Backup Service;1;"CR-04; CR-10";LOC-03;
4;Servid-DUFOUR STORAGE004;S-04;Storage Service;1;;"LOC-01 ; LOC-02; LOC-04";
```

a file named 5-Layers.csv and fill it with the following content:

```csv
Layer;Service ID
IAAS;"S-01;S-02;S-03;S-04"
```
a file named 6-Keywords.csv and fill it with the following content:

```csv
Keyword;Service IDs
Storage;S-01
Authentication;S-02
Backup;S-03
Storage;S-04
```
// Credential generator
// TODO : check si possible de générer catalogue sans compliance
// a la fin -> catalogue généré au format json et VC signés
// provider-catalogue expose catalogue sur /catalogue

#### Bootstrap credential-generator database

Before bootstrapping provider data, you need to ingest the federation and provider data into the credential-generator database. See the following page to have more information about the [database initialization](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-generator/-/blob/main/docs/tutorials/00_database_initialization/02_import_csv_files_into_database.md?ref_type=heads)
To do so, you need to launch the credential-generator with the following command:

```bash
poetry run credential-generator\
init \
--name federation-x\
--input-root-directory ${FEDERATION_DATA}
```

#### Bootstrap provider catalogue

To bootstrap the provider catalogue, you need to launch the credential-generator with the following command:

```bash
poetry run credential-generator\
  provider bootstrap \
  --name dufourstorage \
  --parent-domain provider.127-0-0-1.nip.io \
  --federation-parent-domain 127-0-0-1.nip.io \
  --output-root-directory ${PROVIDER_CATALOGUE_DATA}
```
#### Upload provider generated files

You need to upload the generated files to the provider user-agent. To do so, you need to launch the following command:

```bash
poetry run credential-generator\
  provider upload \
  --name dufourstorage \
  --parent-domain provider.127-0-0-1.nip.io \
  --output-root-directory ${PROVIDER_CATALOGUE_DATA} \
  --api-key ${API_KEY_AUTH} \
  --auditor-api-key ${API_KEY_AUTH}
```

#### Upload provider generated files into provider catalogue

You need to upload the generated files to the provider catalogue. To do so, you need to launch the following command:

```bash
poetry run credential-generator\
  provider sync \
  --name dufourstorage \
  --parent-domain provider.127-0-0-1.nip.io \
  --api-key ${PROVIDER_CATALOGUE_API_KEY_AUTH}
```

### Aliment a federated-catalogue

#### Launch the Federated-Catalogue with port 5001

````shell
docker run --rm -d --name federated-catalogue -p 6002:5002 \
  -e API_KEY_AUTHORIZED=this-is-a-test \
  -e API_PORT_EXPOSED=5002 \
  -e CES_URL=https://ces-main.lab.gaia-x.eu \
  -e GRAPHDB_URL=http://localhost:7200 \
  -e PARENT_DOMAIN=dev.gaiax.ovh \
  -e PARTICIPANT_NAME=aster-x \
  -e REDIS_HOST=redis://localhost:6379/0 \
  registry.gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue:1.4.0
````

#### Register your provider catalogue inside the Federated-Catalogue

````shell
curl --location --request POST 'https://localhost:5002/provider-catalogue-url?provider_catalogue_url=https%3A%2F%2Fcatalogue-api.dufourstorage.dev.gxdch.ovh%2Fcatalog' \
--header 'x-api-key: my_api_key'
````

You can check the registration with following command :

````shell
curl --location 'http://0.0.0.0:5002/provider-catalogue-url' --header 'x-api-key: my_api_key'
````

#### Launch the HTTP synchronisation

````shell
curl --location --request POST 'http://0.0.0.0:5002/launch-async-http-synchro' --header 'x-api-key: a-key-for-security'
````

You can follow the synchronisation status with following command :

```shell
curl --location 'http://0.0.0.0:5002/http-synchro-status'
```

When the synchronisation is DONE, you can request Federated-Catalogue.

````shell
curl --location 'http://0.0.0.0:5002/query_page?page=0&size=25' --header 'Content-Type: application/json' --data '{}'
````

## Optional operation

- You can branch a Catalogue UI with your Federated-Catalogue as backend.
- You can get the Gaia-X compliance (requirement : deploy your provider components to get a resolvable URL)
- You can get the Aster-X label.
