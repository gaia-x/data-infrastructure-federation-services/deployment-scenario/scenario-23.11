## Getting Started

```
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/waltid-projects/waltid-walletkit.git
cd docker
docker-compose build
docker-compose up
```

### Login

the /api/auth/login route requires 1 mandatory parameter: id, to allow a connection.  
This route will return a bearer token, which will allow access to more routes.

```
curl -X 'POST' \
  'http://localhost:8080/api/auth/login' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
"id": "toto"
}'
```

### User

To consult the did contained in the wallet:

```
curl -X 'GET' \
  'http://localhost:8080/api/wallet/did/list' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8'
```

To create a did, you will need to fill in the parameter mandatory: method.  
3 options are possible for this parameter, a did with method key, a did with method url or a did with method ebsi:

```
curl -X 'POST' \
  'http://localhost:8080/api/wallet/did/create' \
  -H 'accept: text/plain' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8' \
  -H 'Content-Type: application/json' \
  -d '{
"method": "key"
}'
```

To consult the list of your credentials contained in your wallet:
```
curl -X 'GET' \
  'http://localhost:8080/api/wallet/credentials/list' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8'
```

To add a credential to your wallet, you will need to fill in the mandatory parameter id:
```
curl -X 'PUT' \
  'http://localhost:8080/api/wallet/credentials/<idVC>' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8' \
  -H 'Content-Type: application/json' \
  -d '{
    "complianceCredential": {
        "@context": [
            "https://www.w3.org/2018/credentials/v1"
        ],
        "type": [
            "VerifiableCredential",
            "ParticipantCredential"
        ],
        "id": "https://compliance.abc-federation.dev.gaiax.ovh/3420feb8-ed57-4285-8d2f-5934157b7d95",
        "issuer": "did:web:compliance.abc-federation.dev.gaiax.ovh",
        "issuanceDate": "2023-04-11T08:03:27.373Z",
        "expirationDate": "2023-07-10T08:03:27.373Z",
        "credentialSubject": {
            "hash": "603ca97f326b7c9d8eb30418edceba1cfac34de6c3ec36c2b9791321ed92eb2b",
            "type": "gx:complianceCredential"
        },
        "proof": {
            "type": "JsonWebSignature2020",
            "created": "2023-04-11T08:03:27.374Z",
            "proofPurpose": "assertionMethod",
            "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..GYGEb0dUvgfvWLSpVgqOhQ187ptqp_vQtxhCEyRIlDX_Ezzckq7tl1odruCUJQp1tOB-4y-j_BKg-tJ0a01bmq9ckhCZxzkZLF-NBXMGO8owEk6D6nkUqmn1IToYj65lamE_Uj1GdPuTio0xQHoi7tvSedBBRQ_AmloMakl7TYZPmzzfHLCsruO6AvA3FTbxqcj_MXvnMiMOBEV4Qb7IeyiT0TRUYjo9BX1R6QVWUVT92cZgfYXWWgropPXIxBmGRU0rH6WyT3AungLlDmV2jtG5C-n6vAKJFZTtX4Zb6x2utaa6h3CBO11p8gSC-uVzaRNri3l3XDb-wOTpiIwYfw",
            "verificationMethod": "did:web:compliance.abc-federation.dev.gaiax.ovh"
        }
    }
}'
```

To delete a credential:
```
curl -X 'DELETE' \
  'http://localhost:8080/api/wallet/credentials/delete/<idVC>' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8'
```

To consult the list of your keys contained in the wallet:
```
curl -X 'GET' \
  'http://localhost:8080/api/wallet/keys/list' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8'
```

To import a key into the wallet:
```
curl -X 'POST' \
  'http://localhost:8080/api/wallet/keys/import' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8' \
  -H 'Content-Type: text/plain' \
  -d -----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAtsb8jEW3ID6w/qcRcvRdakPe73j9XiA4I985jHc74ekzk37A
BAGHQ7opmCDpfrv4FMPY+aa6tqZvML3JTlDujCqUog5ueCYD09ROYw9gJ1DyDe4/
hsAsqpYHSyLZAAR/Mv1xSBPYqVLkosAxOROBbMuQOAq4KVcGSRh2Zfl4K6dbaJRR
QOudBH4PCByfF87UDJnfq7MOxhaynsjcWpTTl0OQjQ8eli8E7aptdwnVKB7Sv/H2
cigquXt4Pt7PTAIhqqPgXCVjCl03jzVdMMkO/GzbeoaJWPZMaQRfkOp3IBVNg/Fz
UCI4bSBep6lxLI+DMIm0Py0DrS3X3HbjwOFZawIDAQABAoIBAQCEqRK+gQ80USxH
...
-----END RSA PRIVATE KEY-----
```

To delete a key:
```
curl -X 'DELETE' \
  'http://localhost:8080/api/wallet/keys/delete/b7f86a86f56b4532b3444771c92ec00c' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8'
```

### Issuer procedure: send a credential in the wallet


* Step 1: Initialize a credential

    Mandatory parameter:  
      - Credential

```
curl -X 'POST' \
  'http://localhost:8080/issuer-api/default/credentials/issuance/request' \
  -H 'accept: text/plain' \
  -H 'Content-Type: application/json' \
  -d '{
    "credentials": [
        {
        "credentialData": {
            "credentialSubject": {
                "currentAddress":["1 Boulevard de la Liberté, 59800 Lille"],"dateOfBirth":"1993-04-08","familyName":"DOE","firstName":"Jane","gender":"FEMALE","id":"did:ebsi:2AEMAqXWKYMu1JHPAgGcga4dxu7ThgfgN95VyJBJGZbSJUtp","nameAndFamilyNameAtBirth":"Jane DOE","personalIdentifier":"0904008084H","placeOfBirth":"LILLE, FRANCE"}
        },
        "type": "VerifiableId"
        }
    ]
}'
```
output:
```
openid-initiate-issuance://?issuer=https%3A%2F%2Fissuer.abc-federation.dev.gaiax.ovh%2Fissuer-api%2Fdefault%2Foidc%2F&credential_type=VerifiableId&user_pin_required=false&op_state=b84a4b59-73e2-4b23-9dec-2e1a4547377f
```

* Step 2: Start issue initiated by issuer

    Mandatory parameter:  
      - oidcUri

```
curl -X 'POST' \
  'http://localhost:8080/api/wallet/issuance/startIssuerInitiatedIssuance' \
  -H 'accept: text/plain' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8' \
  -H 'Content-Type: application/json' \
  -d '{
"oidcUri": "openid-initiate-issuance://?issuer=https%3A%2F%2Fissuer.abc-federation.dev.gaiax.ovh%2Fissuer-api%2Fdefault%2Foidc%2F&credential_type=VerifiableId&user_pin_required=false&op_state=b84a4b59-73e2-4b23-9dec-2e1a4547377f"
}'
```
output:
```
4dae0410-9fed-4e95-8ed5-58a2a68508f2
```

* Step 3: Configure a did for the issuance

    Mandatory parameter:  
      - sessionId
      - did

```
curl -X 'GET' \
  'http://localhost:8080/api/wallet/issuance/continueIssuerInitiatedIssuance?sessionId=4dae0410-9fed-4e95-8ed5-58a2a68508f2&did=did%3Akey%3Az6MkqeTKZkqRyWMFut28TXmjJKfwcCquCy75wUsAyMzees9p' \
  -H 'accept: text/plain' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8'
```

* Step 4: Issue

Get issuance info:
    Mandatory parameter:  
      - sessionId

```
curl -X 'GET' \
  'http://localhost:8080/api/wallet/issuance/info?sessionId=4dae0410-9fed-4e95-8ed5-58a2a68508f2' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8'
```

To issue:
    Mandatory parameter:  
      - credentialTypes
      - did
      - issuerId
      - walletRedirectUri

```
curl -X 'POST' \
  'http://localhost:8080/api/wallet/issuance/start' \
  -H 'accept: text/plain' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0b3RvIn0.BWhqyRkm3ta2yQ2_D9uhheBCbFjj_yMb9xxGt45jRn8' \
  -H 'Content-Type: application/json' \
  -d '{
  "credentialTypes": [
    "VerifiableId"
  ],
  "did": "did:key:z6MkqeTKZkqRyWMFut28TXmjJKfwcCquCy75wUsAyMzees9p",
  "issuerId": "https://issuer.abc-federation.dev.gaiax.ovh/issuer-api/default/oidc/",
  "walletRedirectUri": ""
}'
```


## **Signing a VC with the walt-id SSI kit**

Install the SSI kit ([https://github.com/walt-id/waltid-ssikit](https://github.com/walt-id/waltid-ssikit) ) and launch it with Docker ( docker-compose build / docker-compose up)

API Usage

The necessary Swaggers:

-   [The Signatory API](http://localhost:7001/v1/swagger#) which will be used for signing the VC.
-   [The Core API](http://localhost:7000/v1/swagger#) which will be used for generating keys / linking with the DID.

### **Steps to follow**

**In the Core API:**

-   Use the route [_/v1/key/gen_](http://localhost:7000/v1/swagger#/Key%20Management/genKey) to generate a key

```json
    {
		"keyAlgorithm" : "ECDSA_Secp256k1"
	}
```


The route returns the generated key ID.`

-   Use the generated key ID to link it with a resolvable DID of your choice in the route [/v1/did/import](http://localhost:7000/v1/swagger#/Decentralized%20Identifiers/importDid)
    -   keyID: the key ID.
    -   Body:
        -   The resolvable DID (example: did:key:z6MkiFniw3DEmvQ1AmF818vtFirrY1eJeYxtSoGCaGeqP5Mu), in raw format, not JSON.

**In the Signatory API:**

-   Use the route [/v1/credentials/issue](http://localhost:7001/v1/swagger#/Credentials/issue) to sign a VC.

```json
{
   "templateId":"VerifiableId",
   "config":{
      "issuerDid":"The previously created DID",
      "subjectDid":"Any DID",
      "proofType":"LD_PROOF"
   },
   "credentialData":{
      "credentialSubject":{
         "firstName":"Severin"
      }
   }
}
```

The returned value is a signed VC.
