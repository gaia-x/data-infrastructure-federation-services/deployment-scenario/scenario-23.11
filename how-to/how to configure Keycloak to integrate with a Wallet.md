## Introduction
In the context of the project, we plan to integrate the wizard and service providers to an OIDC providers to allow users to authentificate themself using their SSI wallets. For this purpose, we have chosen Keycloak as Identity Broker and WaltId-IdpKit as an Identity Provider. The reason for not integrating OIDC Clients to Waltid-IdpKit is that we can benefit from the brokering feature of Keycloak to be gradually integrating more Identity Providers in the future. Moreover, Keycloak is a complete OIDC solution which provides an administration interface with all the customizable parameters for user management, authentication workflow, security policies, etc

## Config Waltid-IdpKit - Identity Provider
The first step in the process is to configure WaltId-IdpKit to support OIDC and SIOP protocols. These two protocols assume a smooth authentication flow between Wizzard -> Keycloak -> IdpKit -> User Wallet

1. Configure IDPKit Client

In order to make IdpKit accessible by Keycloak, we need to create a client in IDPKit for Keycloak. The credentials of the created client allows Keycloak to securely communicate with IdpKit. In order to create a client, we first need to generate an initial access token (IAT) with the following command:

```
waltid-idpkit config --oidc clients token
```
Then, we use the IAT to send the below POST request to IDPKit to create the client

```
POST /api/oidc/clients/register HTTP/1.1
[...]
Authorization: Bearer <mark>IAT Token</mark>

{
  "client_name": "my-ket-cloak-broker",
  "redirect_uris": [
    "https://my-keycloak-broker.com"
  ],
  "all_redirect_uris": false
}
```

We note that the <mark>redirect_uri</mark> is obtained by create an Identity Provider in Keycloak, which will be discussed in the next Section. Upon receiving a response from IdpKit, we keep the information related to the client to config Keycloak later.

2. Configure the VP versus ID/Access Token Conversion

Note that Keycloak only knows about the OIDC standard with ID/Access tokens and does not actually recognize the new terms in the SSI world such as Verifiable Credential/Presentation. Therefore, IDPKit takes the responsibility to convert VPs into ID/Access tokens and transmit them to Keycloak.

To this end, we need to config the <mark>idp-config.json</mark>. You can find below an example of the configuration:
```
{
  "externalUrl": "https://idp.abc-federation.dev.gaiax.ovh",
  "jsProjectExternalUrl":"http://nftkit-js:80",
  "openClientRegistration": true,
  "fallbackAuthorizationMode": "SIOP",
  "claimConfig": {
    "vc_mappings": [
      {
        "scope": [ "gaiax" ],
        "claim": "name",
        "credentialType": "ParticipantCredential",
        "valuePath": "$.credentialSubject.hasLegallyBindingName"
      },
      {
        "scope": [ "gaiax" ],
        "claim": "country",
        "credentialType": "LegalPerson",
        "valuePath": "$.credentialSubject.gx-participant:legalAddress.gx-participant:addressCountryCode"
      },
      {
        "scope": [ "profile" ],
        "claim": "name",
        "credentialType": "VerifiableId",
        "valuePath": "$.credentialSubject.firstName $.credentialSubject.familyName"
      },
      {
        "scope": [ "profile" ],
        "claim": "family_name",
        "credentialType": "VerifiableId",
        "valuePath": "$.credentialSubject.familyName"
      },
      {
        "scope": [ "profile" ],
        "claim": "given_name",
        "credentialType": "VerifiableId",
        "valuePath": "$.credentialSubject.firstName"
      },
      {
        "scope": [ "profile" ],
        "claim": "gender",
        "credentialType": "VerifiableId",
        "valuePath": "$.credentialSubject.gender"
      },
      {
        "scope": [ "profile" ],
        "claim": "birthdate",
        "credentialType": "VerifiableId",
        "valuePath": "$.credentialSubject.dateOfBirth"
      },
      {
        "scope": [ "address" ],
        "claim": "address",
        "credentialType": "VerifiableId",
        "valuePath": "$.credentialSubject.currentAddress[0]"
      }
    ],
    "nft_mappings":  [
      {
        "scope": [ "award" ],
        "claim": "awd",
        "chain": "GHOSTNET",
        "smartContractAddress": "KT1Rc59ukgW32e54aUdYqVzTM9gtHrA4JDYp",
          "factorySmartContractAddress": "",
        "trait": "award"
      }
    ],
    "default_nft_token_claim": {
      "chain": "GHOSTNET",
      "factorySmartContractAddress": "",
      "smartContractAddress": "KT1Rc59ukgW32e54aUdYqVzTM9gtHrA4JDYp"
    },
    "default_nft_policy":{
      "withPolicyVerification": false,
      "policy": "",
      "query": "",
      "inputs": {
          "Backgrounds": "Green"
      }
    }
  }
}
```
3. Configure the Verifier
All the VPs sent from the Wallet to the IDPKit need to be validated by the Verifier portal. An example of Verifier configuration can be found below
```
{
  "verifierUiUrl": "https://verifier.abc-federation.dev.gaiax.ovh/sharecredential?state=",
  "verifierApiUrl": "https://idp.abc-federation.dev.gaiax.ovh/api/siop/default",
  "additionalPolicies": [
  ],
  "wallets": {
    "walt.id": {
      "id": "walt.id",
      "url": "https://wallet.abc-federation.dev.gaiax.ovh",
      "presentPath": "api/siop/initiatePresentation/",
      "receivePath" : "api/siop/initiateIssuance/",
      "description": "walt.id web wallet"
    }
  }
}
```

## Configure Keycloak

1. Configure the Identity Provider
With regard to Keycloak, the Waltid-IdpKit plays the role of Identity Provider. For this purpose, we need to log in Keycloak as an administrator. To faciliate the configuration, you can simply import the realm.json located in the config folder and then change the information related to the Client created in the previous section. Basically, in the configuration file, you can configure the following information:
- Client ID
- Client Secret
- Scope: OpenId (default), Profile (default), and additional scopes can be added

2. Configure an OIDC clients for the Wizard

To enable OIDC-based authentication for Wizard, we need to create a client in Keycloak for it. Some informations required for such a creation is:
- Client ID
- Client Secret
- Type of Client: Confidential

## Config OIDC Clients: Wizard
After having created a client for Wizard in Keycloak, we can get the client credential to integrate with Keycloak.
For the testing purpose, you can also use an example project located [Here](https://github.com/walt-id/waltid-nft-auth-nextjs)
