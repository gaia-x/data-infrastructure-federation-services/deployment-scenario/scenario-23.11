# How to install and configure participant components

Welcome to our step-by-step guide on installing and configuring essential components for a participant to seamlessly
integrate with an ecosystem.
In this article, we will walk you through the entire process, covering the installation of necessary components,
their setup, data creation specific to the participant, and the seamless push of this data into the ecosystem's
federated catalog.

Whether you're a newcomer exploring Gaia-X or a seasoned professional looking for a detailed reference, this guide
aims to provide you with a clear and concise roadmap. By the end of this tutorial, you'll have a solid understanding
of the key steps involved in preparing a participant to be an integral part of the Gaia-X ecosystem.

Let's dive in and unlock the full potential of your participation in Gaia-X by ensuring the smooth installation
and configuration of participant components.

## Prerequisites

1. **Domain Name:**
   - A domain name is crucial for identifying and accessing your services within the Gaia-X ecosystem.
   - You need to own or have control over a domain name to set up and configure various components.
   >**Note:** This could be a new domain or an existing one that you want to use for Gaia-X participation.

2. **SSL Certificate for the Domain and its Subdomains:**
   - Gaia-X emphasizes security, and having an SSL certificate for your domain (and its subdomains) is a fundamental
   requirement. Thanks to this certificate with its associated key pairs, you will be able to sign your verifiable
   credentials.
   - This certificate must be issued from a Certificate Authorities (CAs) that is recognized in
   - Gaia-X Digital Clearing House's Trust Anchors .
   > **Note:** You can obtain SSL certificates from Certificate Authorities (CAs) like Let's Encrypt
   > that simplify the process _for demonstration purpose_. <br/>
   > **For production environment, EV SSL or eiDAS certificates
   > must be used to sign verifiable credentials and must be securely stored in your environment.**

3. **Kubernetes Cluster:**
   - All essential components required for a participant's involvement are packaged and distributed as Docker images.
   - This guide primarily focuses on deploying the VC Issuer into a Kubernetes cluster; however, it's worth noting that
   the deployment principles can be adapted for use with other Docker orchestrators.
4. **Cert manager:**
   - You must install cert manager on your kubernetes cluster to automatically get let's encrypt SSL certificates for
   your ingresses.
   - See [Cert Manager documentation](https://cert-manager.io/) to understand how to install and configure it.

By ensuring you have a domain name, an SSL certificate for secure communication, and a Docker orchestrator for
container management, you'll be well-prepared to proceed with the installation and configuration of participant
components in the subsequent steps of the tutorial.

## Installation
>**Note:** In the following example, we assume that you deploy your different components into a default namespace.
> Nevertheless, we recommend you to create a dedicated namespace for your participant

### Certificate secret creation
First of all, you need to create a secret containing your private key and its certificate in files tls.key and tls.crt.

```bash
kubectl create secret tls root-certificate --key="tls.key" --cert="tls.crt"
```

VC Issuer and User agent will use secret `root-certificate` to respectively sign Verifiable Credentials or
Verifiable Presentations and to expose public key and certificate chain.

### VC Issuer

The VC Issuer service requires the use of a private key, associated with the public key disclosed by the Participant
Agent, for signing a Credential to achieve Gaia-X Compliance. Furthermore, these keys must be affiliated with a
certificate acknowledged by Gaia-X Digital Clearing House Trust Anchors.

Repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer

This Step-by-Step guide will help deploy your participant VC Issuer on your Kubernetes cluster.

In the VC Issuer repository, fundamental Kubernetes manifests are available for deploying the service into a Kubernetes cluster. Customization of values to align with specific requirements can be accomplished through Kustomize.

1. Create a base directory and download base manifests found on the [VC Issuer repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer/-/tree/main/deployment/packages/base?ref_type=heads).
2. Create an overlay directory that contains the following content in a file named **kustomization.yaml**.

```yaml

apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ../base
namespace: <your namespace>

patches:
  - patch: |-
      - op: add
        path: "/spec/template/spec/containers/0/env/-"
        value:
          name: DID_WEB_ISSUER
          value: "did:web:<participant agent domain>"
      - op: replace
        path: "/spec/template/spec/containers/0/image"
        value: "<image tag>"
      - op: replace
        path: "/spec/template/spec/volumes/0/secret/secretName"
        value: "root-certificate"
    target:
      kind: Deployment
      name: vc-issuer
  - patch: |-
      # This patch removes the Ingress with the given name
      $patch: delete
      apiVersion: networking.k8s.io/v1
      kind: Ingress
      metadata:
        name: vc-issuer
```
> Note: Replace values <your namespace>, <image tag>, and <participant agent domain> with your environment values.

3. Deploy to your Kubernetes cluster
   - To deploy, run the following command in `overlay` directory:

   ```bash
   kubectl apply -k .
   ```

   - Check if vc-issuer is ready

   View the pod status to check that it is ready:

   ```bash
   kubectl get pod
   ```
   The output displays the pod created:
   ```bash
    NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
    vc-issuer-75f59d57f4-4nd6q   1/1     1            1           1m12s
   ```
   View the Deployment's status:
   ```bash
     kubectl get deploy vc-issuer
   ```

   The output displays that the Deployment was created:

   ```bash
    NAME        READY   UP-TO-DATE   AVAILABLE   AGE
    vc-issuer   1/1     1            1           2m21s
   ```
   View the Service's status:

   ```bash
   kubectl get svc vc-issuer
   ```

   The output displays that the Service was created:
   ```bash
   NAME                TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
   vc-issuer           ClusterIP   10.3.73.17    <none>        80/TCP     2m42s
   ```

   You can port-forward to service vc-issuer to check if it is running

   ```bash
   kubectl port-forward service/vc-issuer 5001:5001
   ```

   On another terminal, ping healthcheck route on vc-issuer
  ```bash
  curl http://localhost:5001/api/v0.9/status
  ```
   The output displays the status of vc-issuer pod:
  ```bash
  {
    "pk_loaded": true,
    "pk_path": "/app/creds/tls.key",
    "log_file": "/tmp/vcissuer.log",
    "algoType": "PS256",
    "algos_supported": [
      "PS256",
      "RS256"
    ],
    "did_web_issuer": "did:web:<participant agent domain>"
  }
  ```
### User Agent

The user agent service, also referred to as the participant agent, furnishes an identity to the ecosystem in the form of a DID Document accompanied by a key pair for signing Credentials, and it additionally exposes verifiable credentials.

Repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent

This Step-by-Step guide will help deploy your participant agent on your Kubernetes cluster.

In the user agent repository, fundamental Kubernetes manifests are available for deploying the service into a Kubernetes cluster. Customization of values to align with specific requirements can be accomplished through Kustomize.

1. Generate a secret that encompasses the Participant Agent API key, ensuring the safeguarding of specific routes within the participant agent.
```bash
kubectl create secret generic user-agent-api-key --from-literal=api_key_authorized=<your api key>"
```

2. Create a base directory and download base manifests found on the [User Agent repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/-/tree/main/deployment/packages/base?ref_type=heads).
3. Create an overlay directory that contains the following content in a file named **kustomization.yaml**.

```yaml

apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - ../base
namespace: <your namespace>
configMapGenerator:
  - name: the-map
    behavior: merge
    literals:
      - CATALOG_API_ENDPOINT=https://federated-catalogue-api.aster-x.demo23.gxfs.fr
      - PARTICIPANT_NAME=<participant name>
      - PARENT_DOMAIN=<participant parent domain>
patches:
  - patch: |-
     - op: replace
        path: "/spec/template/spec/containers/0/image"
        value: "<image tag>"
      - op: replace
        path: "/spec/template/spec/volumes/0/secret/secretName"
        value: "root-certificate"
    target:
      kind: Deployment
      name: participant-agent
  - patch: |-
      - op: replace
        path: "/spec/rules/0/host"
        value: "<participant name>.<participant parent domain>"
      - op: replace
        path: "/spec/tls/0/hosts"
        value: ["<participant name>.<participant parent domain>"]
    target:
      kind: Ingress
      name: participant-agent
```
> Note: Replace values <your namespace>, <image tag>, <participant name> and <participant parent domain> with your environment values.

4. Deploy to your Kubernetes cluster
   - To deploy, run the following command in `overlay` directory:

   ```bash
   kubectl apply -k .
   ```

   - Check if participant agent is ready

   View the pod status to check that it is ready:

   ```bash
   kubectl get pod
   ```
   The output displays the pod created:
   ```bash
   NAME                                 READY   STATUS    RESTARTS   AGE
   participant-agent-7fd85bf97c-7c7br   1/1     Running   0          1m26s
   ```
   View the Deployment's status:
   ```bash
     kubectl get deploy participant-agent
   ```

   The output displays that the Deployment was created:

   ```bash
   NAME                READY   UP-TO-DATE   AVAILABLE   AGE
   participant-agent   1/1     1            1           1m49s
   ```
   View the Service's status:

   ```bash
   kubectl get svc participant-agent
   ```

   The output displays that the Service was created:
   ```bash
   NAME                TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
   participant-agent   ClusterIP   10.3.46.36   <none>        80/TCP    2m02s
   ```

   View the Ingress's status:

   ```bash
   kubectl get ingress participant-agent
   ```

   The output displays that the Service was created:
   ```bash
   NAME                CLASS    HOSTS                                            ADDRESS       PORTS     AGE
   participant-agent   <none>   <participant name>.<participant parent domain>   37.59.19.13   80, 443   2m53s
   ```

   Check if participant-agent is ready:
   ```bash
   curl https://<participant name>.<participant parent domain>/healthcheck
   ```

5. Bootstraping your participant agent

Bootstrapping your participant agent is imperative for generating a DID Document and making it accessible along with the associated certificate chain and public keys.

Run the following command to bootstrap it:
```bash
curl -X "POST" "https://<participant name>.<participant parent domain>/api/bootstrap-provider" \
     -H 'x-api-key: <participant agent api key>'
```

Check if did document is available:
```bash
curl -X "GET" "https://<participant name>.<participant parent domain>/.well-known/did.json"
```
The output displays that the did document was created:

```javascript
{
    "@context": [
        "https://www.w3.org/ns/did/v1"
    ],
    "id": "did:web:<participant name>.<participant parent domain>",
    "verificationMethod": [
        {
            "@context": "https://w3c-ccg.github.io/lds-jws2020/contexts/v1/",
            "id": "did:web:<participant name>.<participant parent domain>",
            "type": "JsonWebKey2020",
            "controller": "did:web:<participant name>.<participant parent domain>#JWK2020-RSA",
            "publicKeyJwk": {
                "kty": "RSA",
                "n": "5wvl-3gapslGLm_LGLuZF3W8yh8tXImkJu4YspJLD6lz8cLkAFqQNEcjaDYsFy2nx6rkI744FVsTXHOFTXfNB3_us5_1AzQzBh0Yq9UogkVFtPnLLkPBS02dpPOufRU8DgdbsCWGfLD8F02eYC_tZsg68sK9mraD8xZMoU_cUT8gqoSBLPdibw-mctfW_wdsNxQ5Mw5efQdxq74UWS-U-BKzFEGOMXsVTvkjWkD__HSqHbLANoEwn8lEZv4X6D6I8nqjCSwAsgcdOPQIUI8KUZMoFn92FCxeV3hTBpzv42fYzWUQSRnBFR8TJ3uEn3wOLUfQ1pfIeazXcm2wiEXMIw",
                "e": "AQAB",
                "kid": "5LxN9y8b8PON3vGdj_gnsJJQEn0EJzRD8ocFirTtlwA",
                "alg": "PS256",
                "x5u": "https://<participant name>.<participant parent domain>/.well-known/chain.pem"
            }
        }
    ],
    "assertionMethod": [
        "did:web:<participant name>.<participant parent domain>#JWK2020-RSA"
    ]
}
```

### Provider Catalogue

The Provider Catalog service serves as the repository for storing verifiable credentials related to a participant's service offerings or data products.
Participant can decide which service offering or data product he wants to push into ecosystem federated catalogue or Gaia-X CES.

Repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue

This Step-by-Step guide will help deploy your participant provider catalogue on your Kubernetes cluster.

In the user agent repository, fundamental Kubernetes manifests are available for deploying the service into a Kubernetes cluster. Customization of values to align with specific requirements can be accomplished through Kustomize.

1. Generate a secret that encompasses the Participant Provider Catalogue API key, ensuring the safeguarding of specific routes within the provider catalogue service.
```bash
kubectl create secret generic catalogue-api-key --from-literal=catalogue-api-key=<your api key>"
```

2. Create a base directory and download base manifests found on the [Provider Catalogue repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue/-/tree/main/deployment/packages/base?ref_type=heads).
3. Create an overlay directory that contains the following content in a file named **kustomization.yaml**.

```yaml

apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- ../base
namespace: <your namespace>
configMapGenerator:
  - name: catalog-configuration
    literals:
      - LOG_LEVEL=INFO
      - ENVIRONMENT=PROD
      - REDIS_PUB_SUB_HOST=redis://<redis federated catalogue>:6379/1
      - REDIS_HOST=redis://redis-server:6379/0
      - PARTICIPANT_NAME=<participant name>
      - PARENT_DOMAIN=<participant parent domain>
      - PARTICIPANT_AGENT_HOST=participant-agent
patches:
  - patch: |-
      - op: replace
        path: "/spec/rules/0/host"
        value: "catalogue-api.<participant name>.<participant parent domain>"
      - op: replace
        path: "/spec/tls/0/hosts"
        value: ["catalogue-api.<participant name>.<participant parent domain>"]
      - op: replace
        path: "/spec/tls/0/secretName"
        value: "ingress-catalogue-api-tls"
    target:
      kind: Ingress
      name: catalogue-api
  - patch: |-
      - op: replace
        path: /metadata/name
        value: "<participant name>-redis-pvc"
    target:
      kind: PersistentVolumeClaim
      name: redis-pvc
  - patch: |-
      - op: replace
        path: "/spec/template/spec/containers/0/image"
        value: "<image tag>"
    target:
      kind: Deployment
      name: catalogue-api
```

> Note: Replace values <your namespace>, <image tag>, <participant name>, <participant parent domain> and <redis federated catalogue> with your environment values.

4. Deploy to your Kubernetes cluster
   - To deploy, run the following command in `overlay` directory:

   ```bash
   kubectl apply -k .
   ```

   - Check if provider catalogue is ready

   View the pod status to check that it is ready:

   ```bash
   kubectl get pod
   ```
   The output displays the pod created:
   ```bash
   NAME                             READY   STATUS    RESTARTS   AGE
   catalogue-api-6d5656d7cd-2p9pj   1/1     Running   0          1m26s
   ```
   View the Deployment's status:
   ```bash
     kubectl get deploy catalogue-api
   ```

   The output displays that the Deployment was created:

   ```bash
   NAME                READY   UP-TO-DATE   AVAILABLE   AGE
   catalogue-api       1/1     1            1           1m49s
   ```
   View the Service's status:

   ```bash
   kubectl get svc catalogue-api
   ```

   The output displays that the Service was created:
   ```bash
   NAME            TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
   catalogue-api   ClusterIP   10.3.22.54   <none>        80/TCP    1m54
   ```

   View the Ingress's status:

   ```bash
   kubectl get ingress catalogue-api
   ```

   The output displays that the Service was created:
   ```bash
   NAME                CLASS    HOSTS                                                         ADDRESS       PORTS     AGE
   catalogue-api       nginx   catalogue-api.<participant name>.<participant parent domain>   37.59.19.13   80, 443   2m24s
   ```

   Check if provider catalogue service is ready:
   ```bash
   curl https://catalogue-api.<participant name>.<participant parent domain>/healthcheck
   ```

   Check if provider catalogue:
   ```bash
   curl https://catalogue-api.<participant name>.<participant parent domain>/catalogue
   ```
The output displays that the did document was created:

```javascript
{
  "gx:providedBy": "did:web:<participant name>.<participant parent domain>:<hash>/data.json",
  "gx:getVerifiableCredentialsIDs": [
  ],
  "@context": {
    "gx": "https://w3id.org/gaia-x/",
    "schema": "https://schema.org/",
    "@vocab": "gx:Catalogue/"
  },
  "@type": "Catalogue"
}
```
