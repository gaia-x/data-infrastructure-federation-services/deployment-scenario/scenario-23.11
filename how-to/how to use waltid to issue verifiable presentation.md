## Getting Started

```
git clone https://github.com/walt-id/waltid-ssikit.git
docker-compose build
docker-compose up
```

### Step 1 : store vc

To store a vc in the waltid wallet, you need to send a PUT request on the route: /credentials/{id}.  
The {id} in the route represents the desired id which will refer to your vc.  
The body of your request must contain the vc to store.  
Note that you will need to store at least 2 VCs in your wallet to form a vp

```
curl -X 'PUT' \
  'http://localhost:7002/credentials/labelling' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "id": "https://labelling.abc-federation.gaia-x.community/vc/0c61ccaa-e929-4569-ac9f-de50e28766c1",
    "type": [
        "VerifiableCredential",
        "GrantedLabel"
    ],
    "credentialSubject": {
        "id": "did:web:ovhcloud.provider.gaia-x.community:participant:e936cd0b-42ff-4be8-805d-db6f3a63574d/granted-label/0c61ccaa-e929-4569-ac9f-de50e28766c1/data.json",
        "type": [
            "GrantedLabel"
        ],
        "vc": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/service-offering/061143dea0dd9a4c91f6adf71e6a0fe07fd7102e745e46c8ef7565f710816285/data.json",
        "label": {
            "hasName": "Gaia-X label Level 1",
            "id": "did:web:gaia-x.community:participant:020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json"
        }
    },
    "expirationDate": "2023-07-02T00:00:00.000Z",
    "issuer": "did:web:abc-federation.gaia-x.community",
    "issuanceDate": "2023-04-03T09:06:28.809209+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2023-04-03T09:06:28.809209+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..XNTC_IZvv7wrsR4iKjcXcny4uS4wmcPXmG6CFrT4Jad115adlAgS05rrSqVYtJhyx7vQHCwnOUDZJoFXJgERUKlh-IJ07stLfnCuWNNxIPZwU2m96n0bzVGDXkg1Lm2-XyT1yvFLnZDvlgKP_bk1sBl7t487w48L4dciTyoblE_VRh76M-djqxTwMJ_7IQuvvrrxFqJz30JEJkpShQGQ8iKhZs9ucl6RFiRTFnAzTpkLfghcp1Sh6-x1OBVhCrLR47W2P_jHJy7XhuJkwB198Gxp5RrB0EzQuSMH1a5_pfh-sFHIhLUXqJmEofj3VTiDnzvn4ZOyZmXHs1gI5Wlnvg"
    }
}'
```
### Step 2 : create vp with VCs ids

To ask your wallet to form a vp with the desired vcs, you must send a request to this route: /credentials/presentIds  
This route needs 2 parameters:  
    - vcIds: String Array
    - holderDid: String

```
curl -X 'POST' \
  'http://localhost:7002/credentials/presentIds' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
"vcIds": ["compliance", "labelling"],
"holderDid": "did:web:walt.id"
}'
```

Output :

```
{
  "type" : [ "VerifiablePresentation" ],
  "@context" : [ "https://www.w3.org/2018/credentials/v1", "https://w3id.org/security/suites/jws-2020/v1" ],
  "id" : "urn:uuid:ea38f3f4-a6ea-4bea-a675-801fe08c4268",
  "holder" : "did:web:walt.id",
  "verifiableCredential" : [ {
    "type" : [ "VerifiableCredential" ],
    "@context" : [ "https://www.w3.org/2018/credentials/v1" ],
    "complianceCredential" : {
      "@context" : [ "https://www.w3.org/2018/credentials/v1" ],
      "type" : [ "VerifiableCredential", "ParticipantCredential" ],
      "id" : "https://catalogue.gaia-x.eu/credentials/ParticipantCredential/1680512876842",
      "issuer" : "did:web:compliance.abc-federation.gaia-x.community",
      "issuanceDate" : "2023-04-03T09:07:56.842Z",
      "expirationDate" : "2023-07-02T09:07:56.842Z",
      "credentialSubject" : {
        "hash" : "603ca97f326b7c9d8eb30418edceba1cfac34de6c3ec36c2b9791321ed92eb2b"
      },
      "proof" : {
        "type" : "JsonWebSignature2020",
        "created" : "2023-04-03T09:07:56.842Z",
        "proofPurpose" : "assertionMethod",
        "jws" : "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..CZQL6fkYzIxNO13mZnXyN-SkjTIqcAfvPzKmyJc_0jLA2xRzCxxJRWkF-mxyV7igJKqpSreaHfYjB_fSYr-FHPYFr9gvNLRN44GNTWuErpfwS57voHklcG72pNnP9duhuss8uIL82CIGrKj2hur7ltc0UU905SFX-_bQkk94MM9hOYbrvidf3sUT4oYHyKz_kSA6dOQBOKR0_o5w_EhBMvRsktY-SPNlI2qDT2c5xSZN-aYHmXXqVT0lT0Xu5yD7pXKl0WX8jPd4QqEsD1WNAgKZoUl2bRYpZIPJz8l9EBMhZkr4Rc83UlFn50ydCVCWIL4r7TyavxTXNOfM--pGIQ",
        "verificationMethod" : "did:web:compliance.abc-federation.gaia-x.community"
      }
    }
  }, {
    "type" : [ "VerifiableCredential", "GrantedLabel" ],
    "@context" : [ "https://www.w3.org/2018/credentials/v1" ],
    "id" : "https://labelling.abc-federation.gaia-x.community/vc/0c61ccaa-e929-4569-ac9f-de50e28766c1",
    "issuer" : "did:web:abc-federation.gaia-x.community",
    "issuanceDate" : "2023-04-03T09:06:28.809209+00:00",
    "expirationDate" : "2023-07-02T00:00:00.000Z",
    "proof" : {
      "type" : "JsonWebSignature2020",
      "created" : "2023-04-03T09:06:28.809209+00:00",
      "proofPurpose" : "assertionMethod",
      "verificationMethod" : "did:web:abc-federation.gaia-x.community",
      "jws" : "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..XNTC_IZvv7wrsR4iKjcXcny4uS4wmcPXmG6CFrT4Jad115adlAgS05rrSqVYtJhyx7vQHCwnOUDZJoFXJgERUKlh-IJ07stLfnCuWNNxIPZwU2m96n0bzVGDXkg1Lm2-XyT1yvFLnZDvlgKP_bk1sBl7t487w48L4dciTyoblE_VRh76M-djqxTwMJ_7IQuvvrrxFqJz30JEJkpShQGQ8iKhZs9ucl6RFiRTFnAzTpkLfghcp1Sh6-x1OBVhCrLR47W2P_jHJy7XhuJkwB198Gxp5RrB0EzQuSMH1a5_pfh-sFHIhLUXqJmEofj3VTiDnzvn4ZOyZmXHs1gI5Wlnvg"
    },
    "credentialSubject" : {
      "id" : "did:web:ovhcloud.provider.gaia-x.community:participant:e936cd0b-42ff-4be8-805d-db6f3a63574d/granted-label/0c61ccaa-e929-4569-ac9f-de50e28766c1/data.json",
      "type" : [ "GrantedLabel" ],
      "vc" : "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/service-offering/061143dea0dd9a4c91f6adf71e6a0fe07fd7102e745e46c8ef7565f710816285/data.json",
      "label" : {
        "hasName" : "Gaia-X label Level 1",
        "id" : "did:web:gaia-x.community:participant:020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json"
      }
    }
  } ],
  "proof" : {
    "type" : "JsonWebSignature2020",
    "creator" : "did:web:walt.id",
    "created" : "2023-04-03T12:38:47Z",
    "proofPurpose" : "authentication",
    "verificationMethod" : "did:web:walt.id#3cb98106677546cda8bcce7ee97daddf",
    "jws" : "eyJiNjQiOmZhbHNlLCJjcml0IjpbImI2NCJdLCJhbGciOiJFZERTQSJ9..JzyFSOiPYtBLA0g16yimUdjERpsnVVyt8S8MzgSszaDfTndhfpxfJs-nUL65aNtlChQQao-TdxCv5dDoxISOAA"
  }
}
```

### Other

To have the complete list of your VCs stored in your wallet:  
```
curl -X 'GET' \
  'http://localhost:7002/credentials' \
  -H 'accept: application/json'
```

To have the complete list of ids of your VCs stored in your wallet:  
```
curl -X 'GET' \
  'http://localhost:7002/credentials/list/credentialIds' \
  -H 'accept: application/json'
```

To have the complete list of your DIDs:  
```
curl -X 'GET' \
  'http://localhost:7002/did' \
  -H 'accept: application/json'
```
