## Contexte
Implemented onboarding and authentication of participant to access wizard service with DID (Web3) technologies by integrating IdPkit and wallet components from walt-id (https://github.com/walt-id/waltid-idpkit) with an IAM Keycloak and wizard service.
## Terminology

| Term                            | Definition                                                                                                                                 | Gitlab repository                                                                                                                                                          |
|---------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Wallet                          | An entity used by the Holder to receive, store, present, and manage Verifiable Credentials and key material.                               |[Wallet Kit](https://docs.walt.id/v/web-wallet/wallet-kit/wallet/architecture)
| User Agent                      | User Agent is an implementation of Wallet for demo purpose                                                                                 | [User Agent repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent)                                                  |
| VC Issuer                       | VC Issuer is an internal service that generates VC for wallet                                                                              | [VC Issuer repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer)                                                    |
|   IDP Kit |   enables you launch an OIDC compliant identity provider that utilizes the OIDC-SIOPv2 protocol to retrieve identity data  via a suitable wallet, providing the data as user info and/or mapping it to standard OIDC claims.                                                                                 |                      [IDP doc](https://docs.walt.id/v/idpkit/idpkit/idpkit/functional-overview)                                                                                                                                                      |
|    Wizard  | Wizard Tool is the entry point for service providers to describe and create their services |  [Wizard repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/service-description-wizard-tool)|


## Use case and flow
### onboarding of participant
```mermaid
sequenceDiagram
    actor P as Participant
    participant W as Wallet
    participant VI as VC Issuer
    participant UA as User agent
    participant C as Compliance


    P->>+UA: Ask DID generation
    UA-->>-P: DID generated

    P->>+W: Store DID
    W-->>-P: OK


    P->>+VI: Request the signature of VC <br> legal person with Participant as Issuer
    VI-->>-P: VC legal person signed

    P->>+W: Store VC legal person
    W-->>-P: OK

    P->>+C: Request Compliance for VC legal person
    C-->>-W: Issue Compliance-VC
```

### authentification of participant
```mermaid
sequenceDiagram

    actor P as Participant
    participant B as Browser
    participant WZ as Wizard
    participant K as KeyCloak
    participant IDP as IDPKit
    participant W as Web Wallet


    P->>+B: Open wizard application
    B->>+WZ: Load login page
    WZ-->>-B: OK

    P->>+B: click "login"
    B->>+K: Start OIDC flow
    K-->>-B: Redirect OIDC authorization <br> endpoint with authorization request param
    P->>+B: Select Identity provider
    B->>+IDP: Load OIDC authorization endpoint

    IDP->>+IDP: Init OIDC session <br> verify authorization request
    IDP->>+IDP: Determine required credential to fulfill  <br> auth request
    IDP->>+IDP: Generate SIOPV2 request for wallet

    IDP-->>-B: Redirect to wallet with SIOPV2 request

    P->>+B: click to login with web wallet

    opt use QR code with mobile wallet
    P->>+B: scan QR code
    B-->>-P: Presentation request
    end

    B->>+W: Load wallet SIOP presentation endpoint
    W-->>-B: OK

    P->>+B: Authenticate to wallet
    B->>+W: Authentication
    W-->>-B: OK
    P->>+B: Accept SIOP presentation request

    B->>+W: SIOP presentation
    W->>+W: Generate SIOP response

    W-->>-B: Rediredt to IDP redirect_uri with SIOP response
    B->>+IDP: Load redirect_uri

    IDP->>+IDP: Apply verification on <br> verifable presentation (vp_tocken)


    IDP->>+IDP: Generate authorization code

    IDP-->>-B: Rediredt to keycloak <br> redirect_uri with authorization code

    B->>+K: Load keycloak redirect_uri
    K->>+IDP: Fetch access_token, id_token from token endpoint
    IDP->>+IDP: create id_token using credential <br> subject DID as user ID

    IDP-->>+K: access_token, id_token  
    K->>+IDP: Fetch user info from userinfo endpoint

    IDP->>+IDP: Map credential data to user info claims

    IDP-->>+K: user info
     K->>+K: Apply verification on user info
    K->>+K: Generate authorization code

    K-->>-B: Rediredt to client application <br> redirect_uri with authorization code
    B->>+WZ: Load client application redirect_uri
    WZ->>+K: Fetch access_token, id_token from token endpoint
    K->>+K: create id_token
    K-->>+WZ: access_token, id_token  
    WZ->>+K: Fetch user info from userinfo endpoint
    K->>+K: Map credential data to user info claims
    K-->>+WZ: user info
    WZ->>+WZ: Create authorized session
    WZ-->>-B: Rediredt to web UI (protected zone)
```

## **Onboard Gaia-X Participant using GXFSFR wallet-kit REST API**

Install the GXFSFR wallet-kit ([https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/waltid-projects/waltid-walletkit](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/waltid-projects/waltid-walletkit) ).

Launch the wallet-kit using:

```shell
docker build --rm -t waltid/walletkit .

docker run -p 8080:8080 -e WALTID_DATA_ROOT=/data -v $PWD:/data waltid/walletkit run
```

To use the onboarding part of the walletkit, you will need to have a way to sign your self-description such as the [VC-Issuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer).


Use the [Swagger](http://localhost:8080/api/swagger) to make request to the backend or setup the [Wallet Frontend](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/waltid-projects/waltid-web-wallet) as described bellow.


### **Onboard a participant**

**Pre Onboarding**

To onboard a participant, you first need to add your crypto material to waltid. To do so, you need to have the wallet-kit installed and an instance of Postman.

- Use the route [/api/auth/login](http://localhost:8080/api/swagger#/Authentication/login) to generate a user bearer token, which is mandatory to use the holder's methods.

-Use the route [/api/wallet/keys/import](http://localhost:8080/api/swagger#/Keys/importKey) to import your private key to waltid (past your private as body). You will get a key id which will be then used to associate your newly created did with the imported key

The curl methods is :
curl --location 'http://localhost:8080/api/wallet/keys/import' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0MSJ9.SwXCH97ouGQx2eMWF-3EMuaZxRhxIijniY5dXn9K0WE' \
--data '-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAoG7uhWylokUVcMT+uWWsAgUXXcM0kQZfY5lUTIQ7dlbcUWtM
...
-----END RSA PRIVATE KEY-----'



-Use the route [/api/wallet/did/create](http://localhost:8080/api/swagger#/Wallet%20%2F%20DIDs/createDid) to create your did.

You will need to enter a body such as :

```json
{
"method": "web",
"didWebDomain": "dufourstorage.provider.dev.gaiax.ovh",
"didWebPath":"participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649",
"keyId":"c84181b44e804c77af69f5746185292a"
}

```

didWebPath is optionnal and is only needed if your did document is located in a specific path in your web server

Now that your participant has its own did, you can proceed to its onboarding

**Using the web wallet**

Install the wallet by cloning the [repository](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/waltid-projects/waltid-web-wallet).

You can specify the address of the backend by setting a .env file containing (default is "https://wallet.abc-federation.dev.gaiax.ovh")

`WALTURL="your url"`

Build the application by running

```shell
yarn install
yarn build
```
Start the application by running

```shell
yarn start
```

To onboard a participant:

- Start by login using an ID and a password (1) (ID is used to associate your account to the VC you generated / imported)

- Click on the menu button at the top right corner and select **Settings** (2)

- Click on **ecosystem** then **Gaia-X** (3-4)

- Paste your self-description (5)

For this part, you will need to have a unsigned participant self-description. Create your self-description (or use one from [the sample document](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-22.11/-/blob/main/sample%20documents.md#s1-participant-dufour-storage-claims-from-schema)), waltid will then sign it using the crypto material you imported previously

- Now that your credential has been imported click on your credential (6)

- Scroll down and click on **Pass compliance** (7)

Your compliance credential should appear on your Credential page


**Using the API**

- Use the route [/api/auth/login](http://localhost:8080/api/swagger#/Authentication/login) to generate a user bearer token, which is mandatory to use the holder's methods.

```json
{
"id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649"
}

```
The route generate an access token that need to be used in the "authorize" part of the swagger

-Use the route [/api/wallet/onboard/selfsign](http://localhost:8080/api/swagger#/issueSelfSignedCredential/issueSelfSignedCredential) to sign your self-description using your own crypto material (pre-onboarding)

 -Body : your unsigned self-description (template), the did you created during pre-onboarding phase (did, verificationMethod), the id of the credentialSubject of your self-description(subject)
 ```json
 {
"template":"string",
"did":"string",
"subject":"string",
"verificationMethod":"string"
}

```


- Use the route [/api/wallet/onboard/import](http://localhost:8080/api/swagger#/Wallet%20%2F%20DID%20Onboarding/onboardGaiaX) to import a signed self-description.

   - Body: a signed participant self-description as described in [the sample document](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-22.11/-/blob/main/sample%20documents.md#s1-participant-dufour-storage-claims-from-schema)
   - Response: String that confirms the import



- Use the route [/api/wallet/onboard/gaiax/compliance](http://localhost:8080/api/swagger#/Wallet%20%2F%20DID%20Onboarding/onboardGaiaX) to ask for compliance for a specific VC. The function will build an unsigned VP and send it to the compliance service and will return a participant credential
   - Body: a signed participant self-description as described in [the sample document](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-22.11/-/blob/main/sample%20documents.md#s1-participant-dufour-storage-claims-from-schema)
   - Response: String that confirms the import


- Use the route [/api/wallet/credentials/list](http://localhost:8080/api/swagger#/Credentials/listCredentialIds) to verify that the compliance credential has been successfuly imported in your wallet

## **Signing a VC with the walt-id SSI kit**

Install the SSI kit ([https://github.com/walt-id/waltid-ssikit](https://github.com/walt-id/waltid-ssikit) ) and launch it with Docker ( docker-compose build / docker-compose up)

API Usage

The necessary Swaggers:

-   [The Signatory API](http://localhost:7001/v1/swagger#) which will be used for signing the VC.
-   [The Core API](http://localhost:7000/v1/swagger#) which will be used for generating keys / linking with the DID.

### **Steps to follow**

**In the Core API:**

-   Use the route [_/v1/key/gen_](http://localhost:7000/v1/swagger#/Key%20Management/genKey) to generate a key

```json
    {
		"keyAlgorithm" : "ECDSA_Secp256k1"
	}
```


The route returns the generated key ID.`

-   Use the generated key ID to link it with a resolvable DID of your choice in the route [/v1/did/import](http://localhost:7000/v1/swagger#/Decentralized%20Identifiers/importDid)
    -   keyID: the key ID.
    -   Body:
        -   The resolvable DID (example: did:key:z6MkiFniw3DEmvQ1AmF818vtFirrY1eJeYxtSoGCaGeqP5Mu), in raw format, not JSON.

**In the Signatory API:**

-   Use the route [/v1/credentials/issue](http://localhost:7001/v1/swagger#/Credentials/issue) to sign a VC.

```json
{
   "templateId":"VerifiableId",
   "config":{
      "issuerDid":"The previously created DID",
      "subjectDid":"Any DID",
      "proofType":"LD_PROOF"
   },
   "credentialData":{
      "credentialSubject":{
         "firstName":"Severin"
      }
   }
}
```

The returned value is a signed VC.
