version: "3.9"

networks:
 app_network:
   name: fastnetwork
   driver: bridge

services:
  redis:
    image: redis:7.2.4-alpine
    ports:
      - "6379:6379"
    healthcheck:
      test: ["CMD", "redis-cli","ping"]
      interval: 10s
      timeout: 10s
      retries: 3
      start_period: 30s
    networks:
      - app_network

  graphdb:
    image: khaller/graphdb-free:10.1.0
    ports:
      - "7200:7200"
    healthcheck:
      test: ["CMD-SHELL", "apt-get update -y && apt-get install -y curl && curl --fail http://localhost:7200/protocol || exit 1"]
      interval: 10s
      timeout: 10s
      retries: 3
      start_period: 30s
    networks:
      - app_network

  vc-issuer:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer:1.0.3
    ports:
      - "5003:5001"
    environment:
      TLS_CERT_PATH: "/app/creds/tls.crt"
      PRIVATE_KEY_PATH: "/app/creds/tls.key"
      DID_WEB_ISSUER: "did:web:dufourstorage.provider.127-0-0-1.nip.io"
      PARENT_DOMAIN: "provider.127-0-0-1.nip.io"
    volumes:
      - type: bind
        source: "$CERT_DIR/key.pem"
        target: /app/creds/tls.key
      - type: bind
        source: "$CERT_DIR/cert.pem"
        target: /app/creds/tls.crt
    networks:
      - app_network

  agent:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent:1.1
    ports:
      - "5001:5001"
    environment:
      API_KEY_AUTHORIZED: "this-is-a-secure-key"
      TLS_CERT_PATH: "/opt/participant-agent/creds/tls.crt"
      PARTICIPANT_NAME: "dufourstorage"
      PARENT_DOMAIN: "provider.127-0-0-1.nip.io"
      PORT: 5001
      VC_ISSUER_URL: "http://vc-issuer:5001"
      CATALOG_API_ENDPOINT: "http://catalogue:5001"
      CATALOG_API_KEY: "this-is-a-test"
      PUBLIC_FOLDER_TO_EXPOSE: "/opt/participant-agent/public"
      LOG_LEVEL: "DEBUG"
    healthcheck:
        test: ["CMD", "curl", "-f", "http://agent:5001/healthcheck"]
        interval: 10s
        timeout: 10s
        retries: 5
    volumes:
      - type: bind
        source: "$CERT_DIR/key.pem"
        target: /opt/participant-agent/creds/tls.key
      - type: bind
        source: "$CERT_DIR/cert.pem"
        target: /opt/participant-agent/creds/tls.crt
      - type: bind
        source: "$CREDENTIALS_DIR"
        target: /opt/participant-agent/public
    depends_on:
      - vc-issuer
      - provider-catalogue
    networks:
      - app_network

  provider-catalogue:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue:1.2
    ports:
      - "6001:5001"
    environment:
      API_KEY_AUTHORIZED: "this-is-a-test"
      API_PORT_EXPOSED: "5001"
      REDIS_HOST: "redis://redis:6379/2"
      REDIS_PUB_SUB_HOST: "redis://redis:6379/1"
      PARTICIPANT_NAME: "dufourstorage"
      PARENT_DOMAIN: "provider.127-0-0-1.nip.io"
      LOG_LEVEL: "INFO"
      ENVIRONMENT: "DEV"
      PARTICIPANT_AGENT_HOST: "http://agent:5001"
      PARTICIPANT_AGENT_SECRET_KEY: "this-is-a-test"
    working_dir: /app
    healthcheck:
        test: ["CMD", "curl", "-f", "http://provider-catalogue:5001/healthcheck"]
        interval: 10s
        timeout: 10s
        retries: 5
    depends_on:
      redis:
        condition: service_started
    networks:
      - app_network

  federated-catalogue:
    image: registry.gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue:1.4.0
    ports:
      - "6002:5002"
    environment:
      API_KEY_AUTHORIZED: "this-is-a-test"
      API_PORT_EXPOSED: "5002"
      CES_URL: "https://ces-main.lab.gaia-x.eu"
      GRAPHDB_URL: "http://graphdb:7200"
      PARENT_DOMAIN: "aster-x.127-0-0-1.nip.io"
      PARTICIPANT_NAME: "aster-x"
      REDIS_HOST: "redis://redis:6379/0"
      REDIS_PUB_SUB_HOST: "redis://redis:6379/1"
      LOG_LEVEL: "INFO"
      ENVIRONMENT: "DEV"
    working_dir: /app
    healthcheck:
      test: ["CMD", "bash", "-c", "exec 5<>/dev/tcp/127.0.0.1/5002"]
      interval: 10s
      timeout: 10s
      retries: 5
    depends_on:
      redis:
        condition: service_healthy
      graphdb:
        condition: service_healthy
    networks:
      - app_network
