# Goal

The purpose of this document is to describe how to instantiate a gaia-x ecosystem with the developments made within the framework of **GXFS-FR**.

This document will describe:
* Prerequisites
* creation of an ecosystem from the simpliest to the most complete:
    * a basic one with minimal requirements for a Gaia-x ecosystem
    * a complete ecosystem including a negociation process, an issuer, a schema registry ...

This document cover only an ecosystem setup.

# Prerequisites

Before to start the creation of a new ecosystem, some elements have to be prepared.

First the **goal of the ecosystem must be defined**.
Who may have access to what kind of catalog ? What will be the added value of the catalog ? The catalog is open to everyone or dedicated to a business ? ...

The ecosystem MUST sign Gaia-x Terms and Conditions.

The ecosystem MUST define a Governance of the ecosystem.
The Governance MUST define processes and rules. It's out of scope of this document.

The ecosystem MAY decline the goal into rules.
eg The goal of the ecosystem is to aggregate health providers: All providers MUST be HDS compliant.
These rules MAY be implemented with the ecosystem compliance and ecosystem labels.

The ecosystem MUST define terms and conditions that MUST be signed by users wanting to be part of the ecosystem.

The ecosystem MAY decline the goal into schemas.
eg The goal of the ecosystem is to aggregate health providers: the object Employee MAY have an attribute health accreditations.

# Basic ecosystem

## Overview

The goal of this chaper is to describe the minimal viable ecosystem reusing GXFS-FR components.
It's a good starting point to have a minimal viable ecosystem.

Few components MUST be instantiated:
* User agent
* VC issuer
* Federated Catalog
* Registry

The user agent service (also known as participant agent) provides an identity to the ecosystem (a DID Document with a key pair to sign Credentials) and exposes verifiable credentials.

The VC Issuer service is required to sign a Credential for the service offering catalog with private key associated to public key exposed by Participant Agent.

The Federated Catalog service is the catalog provided by the ecosystem. It ingests participant catalogues to build a federated catalogue. The web catalog provides a friendly way to browse it.

>**Note:** The web catalog MUST be customized with the ecosystem branding and user interface, legals informations ...

## Install

### Prerequisites

To establish an ecosystem, it's essential to obtain a certificate from a reliable anchor acknowledged by the Gaia-X
Digital Clearing House compliance service.
For illustrative purposes, the GCDCH non-production compliance service is employed, allowing you to acquire an SSL
certificate from Let's Encrypt.

> **Note:** In a production environment, it's imperative to have an eiDAS or EV SSL certificate; otherwise, your
> verifiable credentials will face rejection from the GXDCH compliance service.

### User agent

The user agent  is a web server to expose and persist:
* the DID Document of the ecosystem
* Participant Verifiable Credentials for the ecosystem

The installation is described by the component in the repository.

### VC Issuer

The goal of the VC issuer is to sign Verifiable Credential v1 or Verifiable Presentation v1 with the certificate of the ecosystem.

The VC Issuer MUST be used to sign
* the Gaia-x terms and conditions
* A legal participoant Verifiable Credential for the Federation as required by the Trust Framework.

```javascript
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://w3id.org/security/suites/jws-2020/v1",
    "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
  ],
  "type": [
    "VerifiableCredential"
  ],
  "id": "",
  "issuer": "",
  "issuanceDate": "",
  "credentialSubject": {
    "type": "gx:LegalParticipant",
    "gx:legalName": "Gaia-X European Association for Data and Cloud AISBL",
    "gx:legalRegistrationNumber": {
      "id": "https://gaia-x.eu/legalRegistrationNumberVC.json"
    },
    "gx:headquarterAddress": {
      "gx:countrySubdivisionCode": "BE-BRU"
    },
    "gx:legalAddress": {
      "gx:countrySubdivisionCode": "BE-BRU"
    },
    "id": ""
  }
}
```

The installation and usage of the VC Issuer is described by the component in the repository.

### Federated Catalog

The Federated Catalog is a graph database storing and indexing all services description or dataset description of the ecosystem.
The installation is described by the component in the repository.

### Registry

The registry is used to store ecosystem terms and conditions.
The installation is described by the component in the repository and API are described to set some Terms and Conditions.

# Complete ecosystem

The goal of this chapter is to describe the ecosystem  developed by GXFS-FR.
With the GXFS development, the ecosystem will have:
- a federated catalog
- a compliance and labelling for this ecosystem and a rule checker.
- a schema registry to add metadatas dedicated to the ecosystem for service or data products
- an Issuer able to provide Credentials to wallets
- a negociation tool and a Digital Signature Gateway

 [The architecture is described there](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-22.11/-/blob/main/components.md#functional-blocks-schema)

The basic ecosystem MUST be instantiated.

For the complete ecosystem, these components MUST be instantiated:
- Ecosystem Compliance
- Registry
- Identity provider
- Issuer (ICP)
- Schema provider
- Web Catalog
- Revocation Registry
- Ecosystem Checker
- Ecosystem labelling
- Contract management (components Negociation Tool and Digital Signauture Gateway)

## Install

### Identity Provider

The Identity Prvider is a keycloak with a specific configuration described in the [document](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/main/OIDC%20Keycloak%20Integration.md?ref_type=heads) and this [document](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/main/idp-authorization-management.md?ref_type=heads)

The Identity Provider allow authentication with a Wallet (SSI) and check required documents.

### Ecosystem labelling

a Labelling is specific for each ecosystem with specific rules and may required specific datas from a specific schema.
A specific label is not required. It's a way to simplify the filtering of services or datas. eg a label can determine a level of 'secret defense' accreditation in a defense ecosystem.

### Registry

The registry allow an ecosystem to add new schema for any object of the ecosystem.
For exemple an ecosystem may add some fields for the CO2 consumtion.

The Gaia-x Registry deployed for the federation [branch GXFS-FR](https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/tree/gxfsfr-dev?ref_type=heads)

To add Schemas or Shapes, files have to be added in your fork of the component in this redirectory https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/tree/gxfsfr-dev/src/static/schemas?ref_type=heads

### Web Catalog

This component provide a user interface for the Federated Catalog.
The installation is described by the component in the repository.

### ecosystem checker

This component is specific for each ecosystem.
A sample is provided for Aster-X with ecosystem-checker with samples rules (eg check the schema of a participant gaia-x credential again the extended aster-x participant).
The ecosystem MUST define specific rules for the ecosystem.

### Ecosystem Compliance

This component is specific for each ecosystem. Each Ecosystem SHOULD have its own rules.
A sample is provided for Aster-X with demo rules.
It's not a mandatory component.

### Ecosystem labelling

This component is specific for each ecosystem. Each Ecosystem SHOULD have its own rules.
A sample is provided for Aster-X with demo rules.
It's not a mandatory component.

### Schema Provider

The ecosystem MAY define specifics schemas to extend Gaia-x schemas in order to add specifics fields required for a specific ecosytem.
Set-up is described in the repository of the Schema Provider.

TO UPDATE WITH GEROME Registry vs schema registry


### Issuer (ICP)

ICP is instantiated for an ecosystem to:
- allow users to add in their wallets an Ecosytem Participant Credential
- allow users to get a legal participant
- allow users to get a registration number

It's an experimental component to demonstrate Wallets advantages to simplify registration or authentication process and to improve trust.

Set-up is described in the repository of ICP

### Revocation Registry

The Revokation is required for ICP and Compliance to allow the ecosystem to revoke Credentials already Issued.
The installation is described by the component in the repository.

### Contract Management

The solution allow a user to request a contract to have access to a dataset, to negociate the contract in a collaborative tool and to sign it.

The contract management is done with these components:
- Digital Signature Gateway
- Negociation Tool

The negociation tool allow users to have access to a directory containing several documents and to edit these documents.
The negociation tool synchronize some elements of the contract (price ...) with an ODRL file.
At the end of negociation, the provider MUST validate to push all documents to the Digital Signature Solution.

The Digital Signature Solution is a gateway between the ecosystem and Digital Signature Solution to sign documents like contracts compliant with existing regulations.
For now the Digital Signature Gateway is compliant with Contralia by Docaposte and IDnomic by Atos. The desin of the solution allow any integrator to add other solutions.
The choice of a Digital Signature solution is configured in the negociation tool.

These components requires ICP to let users get a Signature Credential in their Wallets.

Set-up and configuration are described in the repository of the negociation tool and Digital Signature Solution.

## How to add services in an ecosystem

How to get services from providers when a federation is started ?

# Component repositories

| Component | Git |
| ------ | ------ |
| User Agent | https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent   |
| VC Issuer |   https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer     |
| Web Catalog |  https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2      |
| Federated Catalog |  https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue      |
| ICP| https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer      |
| Ecosystem Compliance sample  | https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-compliance   |
| Ecosystem Labelling | https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/labelling    |
| eco system-checker |   https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/abc-checker     |
| negociation tool  |  https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/negociation-tool  |
| revokation registry  |  https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/revocation-registry  |
| negociation tool  |  https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/negociation-tool  |
| schema provider  | https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/schema-provider |
| Digital Signature Gateway |https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/digital-signature-gateway |
| Registry | https://gitlab.com/gaia-x/lab/compliance/gx-registry|
