# The Ecosystem Aster-X!

On this page, you will discover everything there is to know about Aster-X and how to interact with its components


# What is Aster-X

Aster-X is a an ecosystem created by GXFS-FR as a toolbox to allow dataspace, or any entities interested in the Gaia-X ecosystem, to bootstrap their own ecosystem.

The ecosystem is composed of a wide range of 	components that can interact with each others to enact various use cases such as identity and access management, contract negotiation, federated catalogue and many others.

The ecosystem can also be used with your own components as it revolves arround bricks which respects current standards such as the OpenID Connect for Verifiable Credential Issuance (OID4VCi) or OpenID Connect for Verifiable Presentation (OID4VP), Presentation Exchange V2 or multiple signature scheme such as the one used in Gaia-X.

If you wish to try to join the ecosystem, here are some usefull links:
***
- ICP : 
	- gitlab repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer
	- deployed version: https://icp-admin-portal.aster-x.demo23.gxfs.fr/login
***
- User Agent: 
	- gitlab repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent
	- deployed version: https://aster-x.demo23.gxfs.fr/
***
- Federated Catalogue: 
	- gitlab repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue
	- deployed version: https://federated-catalogue-api.aster-x.demo23.gxfs.fr/
***
- Web Catalog: 
	- gitlab repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2
	- deloyed version: https://webcatalog.aster-x.demo23.gxfs.fr/catalog
***
- Compliance : 
	- gitlab repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-compliance
	- deployed version: https://compliance.aster-x.demo23.gxfs.fr/docs
***
- Registry: 
	- gitlab repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/fork-gxdch/registry
	- deployed version: https://registry.aster-x.demo23.gxfs.fr/docs
***
- Verifier: 
	- gitlab repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-verifier
	- deployed version: https://verifier.aster-x.demo23.gxfs.fr/docs

***
- Self-Description Wizzard : 
	- gitlab repository: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/service-description-wizard-tool
	- deployed version: https://service-description-wizard-tool.aster-x.provider.demo23.gxfs.fr/
***
You can also create you own Aster-X credential by following the tutorial on this page.

## Get you Aster-X Credential

To get your own Aster-X Credential, you must follow 4 steps:

### Create your Participant Gaia-X Credential  

One of the necessity to pass Aster-X compliance is to create an extended Participant using the extended Participant Schema located in the Aster-X registry: 

```json
https://registry.aster-x.demo23.gxfs.fr/api/trusted-schemas-registry/v2/schemas/LegalParticipantSchemaFederation
```
This schema extends the current Participant by adding a new field: 

"aster-conformity:blockchainAddress"

This field act as a placeholder of how an ecosystem could extend a current Gaia-X credential to serve the purpose of the ecosystem. The same action could be applied to a Service-Offering, allowing to display ecosystem specific attributes within its catalogue.
It could also serve other purposes such as in Identity and Access Management

To generate this credential, you could use deploy your own VC-Issuer or use the GXDCH Wizzard and manualy adding the property and the context : 
 ``` json
https://schema-registry.aster-x.demo23.gxfs.fr/contexts/aster-conformity
```


Congratulation, you can now create your other Gaia-X Credentials and get pass the compliance (which is mandatory for the Aster-X Compliance)


### Sign Aster-X Terms and Conditions

To participate in an ecosystem, it is important to abide by its rules. Therefore the Aster-X ecosystem posess its own set of Terms And Conditions as should all future ecosystem. They are available in the Aster-X Registry, a fork of the GXCDH Registry and are available at this url : 

```json 
https://registry.aster-x.demo23.gxfs.fr/api/termsAndConditions
```

To be put into your credential, you will need to convert the terms and condition text to a string, using a tool such as  

```json
https://onlinetexttools.com/json-stringify-text
```
After signing it, you should obtain a credential such as 

```json
{

"type": [
	"VerifiableCredential"
],
"@context": [
	"https://www.w3.org/2018/credentials/v1",
	"https://w3id.org/security/suites/jws-2020/v1",
	"https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
],
"id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/8b866561-2354-4ca4-b6d2-3fe5cd46a2b9/data.json",
"issuer": "did:web:dufourstorage.provider.dev.gaiax.ovh",
"credentialSubject": {
	"id": "did:web:dufourstorage.provider.dev.gaiax.ovh:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/provider/data.json",
	"aster-conformity:termsAndConditions": "The PARTICIPANT signing the Self-Description agrees as follows:\n- to update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions.\n- to be and remain GaiaX compliant while being a member of this Federation\nThe Federation will revoke any Verifiable Credential emmitted by the Federation if the Federation becomes aware of any inaccurate statement in regards to the two rules listed",
	"type": "aster-conformity:TermsAndConditions"
},
"expirationDate": "2024-08-27T15:18:17.626769+00:00",
"issuanceDate": "2024-02-29T15:18:17.626769+00:00",
"credentialStatus": {
	"type": "StatusList2021Entry",
	"id": "https://revocation-registry.abc-federation.dev.gaiax.ovh/api/v1/revocations/credentials/dufourstorage-revocation#39",
	"statusPurpose": "revocation",
	"statusListIndex": "39",
	"statusListCredential": "https://revocation-registry.abc-federation.dev.gaiax.ovh/api/v1/revocations/credentials/dufourstorage-revocation"
},
"proof": {
	"type": "JsonWebSignature2020",
	"proofPurpose": "assertionMethod",
	"verificationMethod": "did:web:dufourstorage.provider.dev.gaiax.ovh",
	"created": "2024-02-29T15:18:17.626769+00:00",
	"jws": 	"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..ylxRz5AMYl7K7O0pvR_7f4mCGOvdkCHgHodEoiJDXaUhvaDmAttEErQKdeyFEL_8LGvyBplcJ1nUv-U3ibJQQwY53XOOS9mwjSZYByWop1wfdcauhf97sAM46LgCtH62Ulz6oO58qYrXmQk0HmYZc_SfQDAwo1GcNwIVaHwF-cfzq6_fLe34azQLdx4WB3kxJSPNwgb8gvEny5fSmyX8bUXsltpKeRS81tjOYdr-D7ERcGm4GZm0gxE7Ps6GQSHJNUnSi6QlVpj_R7Ye7yVXBfN9geXUuq2PRK5mhqshZf8gfSzzvzTlTBuSJCODonnmFP80YhqcOdF6URso_--2Cg"
}
}
```

### Prepare your Verifiable Presentation

To pass Aster-X compliance, you will need to present a payload in the form of a Verifiable Presentation containing 3 Verifiable Credential.

- Your extended Participant Credential (to pass the schema verification)
- Your Compliance credential (to prove you are compliant with Gaia-X specification)
- Your signed Aster-X Terms and conditions

You can use your personnal signature solution or the VC issuer to create needed VP and send it to the compliance using the url

```json
https://compliance.aster-x.demo23.gxfs.fr/api/credential-offer?type=Participant
```

If any test fails, you will get the details on which part you need to adjust in order to finalize your onboarding to the ecosystem.

Congratulation, your have obtained your Aster-X Credential !





