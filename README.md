# Scenario 23.11


## Name
Implementation Scenario for the Gaia-X Summit 2023

## Scenario 23.11

Detailed Description of [Scenario 23.11](scenarii.md)


## Implementation Part I, Onboard a Participant Provider

Bob is the administrator of Company B and want to identify Company B as a provider to the GXDCH and onboard the federation

[Full Description Part I](scenarii.md#part-i-onboard)

* **Initial Status**: No Credentials stored anywhere
* **Expected Status at the end**: Did + Participant Credentials + Compliance Credentials + Federation ABC Membership

Test DATA to reproduce the demo: [Step01](test data/Step01 Test Data.md)

* **Possible path**:

| Implementation | Description | Input | Output |
| ------ | ------ | ------ | ------ |
| Gaia-X Wizard | https://wizard.lab.gaia-x.eu/  | None       | gx:LegalParticipant // gx:GaiaXTermsAndConditions // gx:compliance + optionally: Did Hosting & Private Key  |
| Script + GXDCH + Federation ABC | Based on CSV file, DID and Credentials are generated | CSV file        |  gx:LegalParticipant // gx:GaiaXTermsAndConditions // gx:compliance + Did Hosting & Private Key      |
| Wallet + GXDCH + Federation ABC | Wallet is used to generated VCs, request compliance and onboarding | Participant Description + DID Hosting  | gx:LegalParticipant // gx:GaiaXTermsAndConditions // gx:compliance |
| OCM + PCM + SD Wizard |   |   |   |

* Technical workflow:
    * [Onboarding a Participant](./how-to/how%20to%20onboard%20a%20participant.md)
* Tutorials
    * [How to create an ecosystem](./how-to/how%20to%20create%20an%20ecosystem.md)
    * [How to install participant components](./how-to/how%20to%20install%20participant%20components.md)
    * [How to configure Keycloak to integrate with a Wallet](./how-to/how%20to%20configure%20Keycloak%20to%20integrate%20with%20a%20Wallet.md)
    * [How to use WaltID to sign and issue Verifiable Credentials](./how-to/how%20to%20use%20waltid%20to%20sign%20and%20issue%20verifiable%20credentials.md)
    * [How to use WaltID to issue a Verifiable Presentation](./how-to/how%20to%20use%20waltid%20to%20issue%20verifiable%20presentation.md)

* Components
    * Create Participant VC:
        * Script: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent
        * Gaia-X Wizard:
            * prod - https://wizard.lab.gaia-x.eu/main
            * dev - https://wizard.lab.gaia-x.eu/development
    * Store Credentials:
        * ParticipantAgent:
        * Walt.id wallet + SSI Kit: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/waltid-projects
    * Request Registration Number
        * Gaia-X Lab + GXDCH
            * https://registration.lab.gaia-x.eu/main/docs/
            * https://registration.lab.gaia-x.eu/development/docs/
    * Request Compliance:
        * Gaia-X Lab + GXDCH
            * https://compliance.lab.gaia-x.eu/main/docs/
            * https://compliance.lab.gaia-x.eu/development/docs/
    * Exchange of credentials through VPs
        * VP Generator: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vp-generator
    * Signature:
        * VC Issuer: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer
        * Gaia-X Wizard:
            * prod - https://wizard.lab.gaia-x.eu/main
            * dev - https://wizard.lab.gaia-x.eu/development
        * Walt.id wallet + SSI Kit: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/waltid-projects


### Part II, Add a Data Product Description to the federated Data Catalog
Bob is the administrator of Company B declared as provider inside the GXDCH
Bob want to expose a Data Product according to its terms of use

[Full Description Part II](scenarii.md#part-ii-add-data-product-offering-to-the-federated-data-catalog)

* **Initial Status**: Did + Participant Credentials + Compliance Credentials + Federation ABC Membership
* **Expected Status at the end**: Service Offering Credentials + Compliance Credentials + Label Credential + Catalogue

Test DATA to reproduce the demo: [Step02](test data/Step02 Test Data.md)

* **Possible path**:

| Implementation | Description | Input | Output |
| ------ | ------ | ------ | ------ |
| Gaia-X Wizard | https://wizard.lab.gaia-x.eu/  | gx:LegalParticipant // gx:GaiaXTermsAndConditions // gx:compliance + optionally: Did Hosting & Private Key       | gx:ServiceOffering // gx:compliance  |
| Script + GXDCH + Federation ABC | Based on CSV file, DID and Credentials are generated | CSV file        |  gx:ServiceOffering // gx:compliance // gx:label // gx:catalogue |
| Service Description Wizard + GXDCH + Federation ABC | Wallet is used to identify in the Wizard, certification can be added to a service description in the wizard | Participant Description + DID Hosting | gx:ServiceOffering // gx:compliance // gx:label // gx:catalogue |
| OCM + PCM + SD Wizard |   |   |   |


Technical Workflow:
* [Catalogue Verification Process DRAFT](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/Federated-catalogue-verification-draft/Catalogue%20Verifier.md)
* [Authorization Management DRAFT](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/299-authorization-management/idp-authorization-management.md)

* Components
    * Create Service VC:
        * Script:
        * Gaia-X Wizard:
            * prod - https://wizard.lab.gaia-x.eu/main
            * dev - https://wizard.lab.gaia-x.eu/development
        * Service Description Wizard Tool:
            * https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/service-description-wizard-tool
        * Notarization of a signed certificate:
            * https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/notarization-service
        * Schema Provider (LinkML)
            * https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/schema-provider-from-linkml
        * Registry
            * https://gitlab.com/gaia-x/lab/compliance/gx-registry
        * Request Compliance:
            * Gaia-X Lab + GXDCH
                * https://compliance.lab.gaia-x.eu/main/docs/
                * https://compliance.lab.gaia-x.eu/development/docs/
        * Request Label:
            * Labelling tool : https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/labelling
            * Rules Checker: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/rules-checker
            * Schema Validator: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/schema-validator
    * Push to Catalogue https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue
        * Provider Catalogue: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/provider-catalogue
        * Federated Catalogue: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue


### Part III, Onboard a Company as a Consumer (LegalPerson), and create a VC for Alice (Employee)

Anna is the administrator of Company A and wants to on-board Company A as a federated service consumer, then create VC for Alice, a BI Analyst of Company A

[Full Description Part III](scenarii.md#part-iii-onboard-company-a-as-a-participant-data-consumer-then-create-vc-for-employee-alice)

* **Initial Status**: No Credentials stored anywhere
* **Expected Status at the end**: Did + Participant Credentials + Compliance Credentials + Federation ABC Membership + Employee VC

Test DATA to reproduce the demo: [Step03](test data/Step03 Test Data.md)

* **Possible path**:

| Implementation | Description | Input | Output |
| ------ | ------ | ------ | ------ |
| Gaia-X Wizard | https://wizard.lab.gaia-x.eu/  | None       | gx:LegalParticipant // gx:GaiaXTermsAndConditions // gx:compliance + optionally: Did Hosting & Private Key  |
| Script + GXDCH + Federation ABC | Based on CSV file, DID and Credentials are generated | CSV file        |  gx:LegalParticipant // gx:GaiaXTermsAndConditions // gx:compliance + Did Hosting & Private Key      |
| Wallet + GXDCH + Federation ABC | Wallet is used to generated VCs, request compliance and onboarding | Participant Description + DID Hosting  | gx:LegalParticipant // gx:GaiaXTermsAndConditions // gx:compliance |
| Identity Credential Issuer (Not 23.xx) | OIDC4VC to identify + generation of Employee VC | employee VC + Credential Status in Revocation Registry | gx:LegalParticipant // gx:GaiaXTermsAndConditions // gx:compliance // gx:Employee? |
| OCM + PCM + TSA |   |   |   |


* Components
    * Same component as Part 1 to onboard an organization
    * In addition, to create an EmployeeVC for Alice:
        * Identity Credential Issuer, implementing OID4VC (Sample Portal + EmployeeBO + CredentialIssuer + Revocation Registry).
            * https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer

### Part IV, Search in the Data Catalog based on criteria

* Alice is a BI analyst from Company A and wants to search a Data Product

[Full Description Part IV](scenarii.md#part-v-alice-and-bob-negotiate-and-agree-on-data-contract)

* **Initial Status**: No Credentials stored anywhere
* **Expected Status at the end**: Did + Participant Credentials + Compliance Credentials + Federation ABC Membership + Employee VC

Test DATA to reproduce the demo: [Step04](test data/Step04 Test Data.md)

* **Possible path**:

| Implementation | Description | Input | Output |
| ------ | ------ | ------ | ------ |
| Gaia-X Kafka Topic |  central Publication / Subscription service (pubsub service) deployed via the GXDCH instances.  | gx credential IDs | Federated Catalogue Front? |
| Provider Catalogue + Federated Catalogue + Web Catalogue Front | Provider Catalogue is synchronized with the federated catalogue | Credential IDs |  Web interface to search |
| CAT |   |   |   |

* Components
    * Federated Catalogue: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/catalogue/federated-catalogue
    * Web Catalog Front: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog


### Part V, Negociate and agree on Data Usage Contract

* Alice of company A negotiates, agrees and signs a Data Contract with Bob of company B

[Full Description Part V](scenarii.md#part-ii-add-data-product-offering-to-the-federated-data-catalog)

Test DATA to reproduce the demo: [Step05](test data/Step05 Test Data.md)

Technical Workflow:
    * https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/digital-signature-gateway#signature-workflow
Components:
    * Negociate a contract:
        * EDC: https://github.com/eclipse-edc/Connector
        * Gaia-X TF Implementation: https://github.com/eclipse-edc/TrustFrameworkAdoption
    * Minimum Viable Data Space: https://github.com/eclipse-edc/MinimumViableDataspace
    * Sign a contract using any solution from the market
        * Digital Signature Gateway: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/digital-signature-gateway

### Part VI, Get access to the Data Product

* Alice is a BI analyst from Company A and wants to access the Data Product

[Full Description Part VI](scenarii.md#part-v-alice-and-bob-negotiate-and-agree-on-data-contract)

Test DATA to reproduce the demo: [Step06](test data/Step06 Test Data.md)

Technical Workflow
* [Authorization Management DRAFT](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/299-authorization-management/idp-authorization-management.md)
Component:
    * Policy Decision Point: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/abc-checker

### Part VII, Chain of Trust
* Alice Demonstration of a Trusted Data Transaction

[Full Description Part VII](scenarii.md#part-vii-chain-of-trust)

Test DATA to reproduce the demo: [Step07](test data/Step07 Test Data.md)

# Architecture

[Aster-X architecture document](architecture.md)

<!--* Description of the [components](components.md)
    * [Main Schema](components.md#functional-blocks-schema)
    * [Functional Blocks Description](components.md#functional-blocks-explanations)
    * [Usage and Workflow](components.md#components-usage)
-->
# Input/Output description + sample demo example

<!--Description of the input and output for each component with sample documents used during the demo [sample documents](sample documents.md))-->

All sample data to test the components are copied in the [test data](test data) directory

# Deployment Architecture

Description of the namespace and container deployed to play the demo: [deployment](deployment.md)
