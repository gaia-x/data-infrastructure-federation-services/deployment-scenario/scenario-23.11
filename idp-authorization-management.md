# Context
The document describes the authorization management based on User Verifiable Credentials as well as the steps to add authorization policies in the context of the demo.  

In fact, the authentication & authorization (AA) portal is made of two distinct components: Keycloak and IdpKit. Actually, the verification of VCs and VPs submitted by User to the AA Portal is verified by Idpkit with some built-in policies: such as JSONFormat, Signature Verification. As we want to enforce more fine-graine policies, we need to add new verification policies which can analyze the attributes inside each VCs and VPs. Fortunately, this can be done by two different approches: vid IDPKit and via Keycloak.

## Authorization Management
### Authorization Management via Keycloak
Keycloak is a modular and mature IDP product, which plays the role of IDP Broker in our context. To add more fine-grained verification in Keycloak, we can implement an extension which performs Post Login Flow. That is, once user's VCs have passed the built-in verifications of IdpKit, they will need to pass the verifications encoded inside the Keycloak extension. 

### Authorization Management via IdpKit
IdpKit also provides a way to add additional fine-grained VC/VP verification policies. All the VC are verified against these policies by Open Policy Agent, a new open-source project. In this project, we chose this approach to perform authorization management.

## The steps to configure new dynamic verification policies in IDPKit

### Configure IDPKit

The very first step to configure a new dynamic verification policy is to register it with the IDPKIT by sending a policy registration request as following: 

```

POST /api/siop/default/config/policies/create/CCMCPolicy?update=true&downloadPolicy=true

{
"name":"CCMCPolicy",
"description": "CCMC Gaiax Policy",
"input":{},
"policy": "package system\r\n\r\nimport future.keywords.in \r\n\r\ndefault main = false\r\n\r\n\r\nformat_vc_url(x) := res {\r\n    tmp := replace(x,\":\",\"\/\")\r\n    res := replace(tmp, \"did\/web\/\",\"https:\/\/\")\r\n    print(res)\r\n}\r\n\r\nget_vc(url) := res {\r\n\thttp_response := http.send({\r\n\t\t\"url\": url,\r\n\t\t\"method\": \"get\"\r\n\t})\r\n    res := http_response.body\r\n    print(res)\r\n}\r\n\r\n\r\nmain {\r\n    data.type[count(data.type) -1] != \"ParticipantCredential\"\r\n} else {\r\n    data.type[count(data.type)-1] == \"ParticipantCredential\"\r\n    x := data[\"credentialSubject\"][\"type\"]\r\n    url := format_vc_url(x)\r\n    vc := get_vc(url)\r\n    vc[\"gax-participant:legalAddress\"][\"vcard:country-name\"][\"@value\"] == \"IT\"\r\n    print(count(vc[\"@context\"]))\r\n}\r\n",
"dataPath":"$",
"policyQuery": "data.system.main",
"policyEngine": "OPA",
"applyToVC": true,
"applyToVP": false
}
```
The Table below describes more in details each attribute in the request:

| Attribute      | Description |
| :---           |    :----:   |
| name           | Name of the verification policy       |
| description    | Description of the policy        |
| input         | The input data used by OPA to verify the policy       | 
| policy        |The policy to be added|
| dataPath    | The starting point to access the VC document        |
| policyEngine           | The actually supported engine is OPA policy       |
| applyToVC    | true/false        |
| applyToVP    | true/false        |


Once you have succesfully registered the Dynamic Policy, you can fetch the updated list of supported policies: 

```
GET /api/siop/default/config/policies/list
```

You can also delete the registered policy by sending the following request:

```
DELETE /api/siop/default/config/policies/delete/{policyName}
```

### Configure WalletKit
After successfully adding this policy to the IDPKIT, we need to configure the Verifier so that VCs and VPs submmited by User via IDPKit will be verified againts the registered policy:

```
verifier-config.json
{
  "verifierUiUrl": "https://verifier.abc-federation.dev.gaiax.ovh/sharecredential?state=",
  "verifierApiUrl": "https://idp.abc-federation.dev.gaiax.ovh/api/siop/default",
  "additionalPolicies": [
    {
        "policy":"CCMCPolicy"
    }
  ],
  "wallets": {
    "walt.id": {
      "id": "walt.id",
      "url": "https://wallet.abc-federation.dev.gaiax.ovh",
      "presentPath": "api/siop/initiatePresentation/",
      "receivePath" : "api/siop/initiateIssuance/",
      "description": "walt.id web wallet"
    }
  }
}

```
### Policy as Code by Rego Programming Languague
Actually the verification policy is described by a new programming languague, named Rego. Once you finish implementing a new verification policy in Rego, you can pass it to the policy field in the new policy registration request described above. One example of such policy written in Rego can be found in [Here](rego-samples), which checks the location of the Participant in the Participant Credential.
## Adding custom VC templates
WalletKit actually supports a very few VC templates. However, you can add new ones or overwrite the existing ones by sending request to Issuer Portal. Below is an example of overwriting the existing Participant Credential template:

```
POST /issuer-api/{tenantId}/config/templates/{id}

{
   "type":[
      "VerifiableCredential",
      "ParticipantCredential"
   ],
   "@context":[
      "https://www.w3.org/2018/credentials/v1",
      "https://w3id.org/security/suites/ed25519-2020/v1",
      "https://w3id.org/security/suites/jws-2020/v1"
   ],
   "id":"vc.gaia-x.eu/participant-credential#392ac7f6-399a-437b-a268-4691ead8f176",
   "issuer":"did:web:vc.gaia-x.eu:issuer",
   "issued":"2022-01-03T20:38:38Z",
   "expirationDate":"2022-01-06T20:38:38Z",
   "credentialSchema":{
      "id":"https://raw.githubusercontent.com/walt-id/waltid-ssikit-vclib/master/src/test/resources/schemas/ParticipantCredential.json",
      "type":"JsonSchemaValidator2018"
   },
   "credentialSubject":{
      "id":"did:web:ccmc-fr.com",
      "hasCountry":"FR",
      "hasJurisdiction":"FR",
      "hasLegallyBindingName":"CCMC FR",
   },
   "credentialStatus":{
      "id":"https://gaiax.europa.eu/status/participant-credential#392ac7f6-399a-437b-a268-4691ead8f176",
      "type":"CredentialStatusList2020"
   }
}


```