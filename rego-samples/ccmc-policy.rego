package system

import future.keywords.in 

default main = false


format_vc_url(x) := res {
    tmp := replace(x,":","/")
    res := replace(tmp, "did/web/","https://")
    print(res)
}

get_vc(url) := res {
	http_response := http.send({
		"url": url,
		"method": "get"
	})
    res := http_response.body
    print(res)
}


main {
    data.type[count(data.type) -1] != "ParticipantCredential"
} else {
    data.type[count(data.type)-1] == "ParticipantCredential"
    x := data["credentialSubject"]["type"]
    url := format_vc_url(x)
    vc := get_vc(url)
    vc["gax-participant:legalAddress"]["vcard:country-name"]["@value"] == "IT"
    print(count(vc["@context"]))
}
